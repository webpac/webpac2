#!/bin/sh

db=$1

if [ -z "$db" ] ; then
	echo "usage: $0 database_name"
	exit
fi


ls -d var/*/$db | xargs -i sh -c "echo cleanup {} ; rm -Rf {}"
