#!/usr/bin/perl -w

# kinosearch.pl
#
# 11/04/07 15:41:02 CET Dobrica Pavlinusic <dpavlin@rot13.org>

use strict;

use KinoSearch::Simple;
use Data::Dump qw/dump/;

my ( $path, $query_string ) = @ARGV;

die "usage: $0 var/kinosearch/index 'query string'\n" unless $path && $query_string && -e $path;

my $index = KinoSearch::Simple->new(
	path     => $path,
	language => 'en',
);

my $total_hits = $index->search(
	query      => $query_string,
	offset     => 0,
	num_wanted => 100,
);

print "Total hits: $total_hits\n";

while ( my $hit = $index->fetch_hit_hashref ) {
	print dump( $hit );
}

