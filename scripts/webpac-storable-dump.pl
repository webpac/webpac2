#!/usr/bin/perl -w

# dump WebPAC storable files

use strict;
use Storable qw/retrieve/;
use Data::Dump qw/dump/;

my $path = shift @ARGV || die "usage: $0 /path/to/lookup\n" unless @ARGV;

print "# $_: ",dump( retrieve( $_ )),$/ foreach @ARGV;

