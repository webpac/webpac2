package WebPAC::Config;

use warnings;
use strict;

use base qw/WebPAC::Common/;

use Data::Dump qw/dump/;
use YAML qw/LoadFile/;

=head1 NAME

WebPAC::Config - handle WebPAC configuration file

=head1 VERSION

Version 0.02

=cut

our $VERSION = '0.02';

=head1 SYNOPSIS

FIXME

=head1 FUNCTIONS

=head2 new

Create new configuration object.

  my $config = new WebPAC::Config(
  	path => '/optional/path/to/config.yml'
  );

=cut

sub new {
	my $class = shift;
	my $self = {@_};
	bless($self, $class);

	my $log = $self->_get_logger();

	if (! $self->{path}) {
		my $hostname = `hostname`;
		chomp($hostname);
		$hostname =~ s/\..+$//;
		if (-e "conf/$hostname.yml") {
			$self->{path} = "conf/$hostname.yml";
			$log->info("using host configuration file: ", $self->{path});
		}
	}

	$self->{path} ||= 'conf/config.yml';

	$log->logdie("can't open ", $self->{path}, ": $!") if ! -r $self->{path};

	$self->{config} = LoadFile($self->{path}) ||
		$log->logdie("can't open ",$self->{path}, ": $!");

	$log->debug("config: ", dump( $self->{config} ));

	$self ? return $self : return undef;
}

=head2 databases

Return all databases in config

  my $config_databases_hash = $config->databases;
  my @config_databases_names = $config->databases;

=cut

sub databases {
	my $self = shift;
	return unless ($self->{config});
	if (wantarray) {
		return keys %{ $self->{config}->{databases} };
	} else {
		return $self->{config}->{databases};
	}
}

=head2 use_indexer

Which indexer are we using?

  $config->use_indexer;

=cut

sub use_indexer {
	my $self = shift;
	return unless ($self->{config});
	return $self->{config}->{use_indexer} || undef;
}

=head2 get

  $config->get('top-level_key');

=cut

sub get {
	my $self = shift;
	my $what = shift || return;
	return $self->{config}->{$what};
}

=head2 webpac

Return C<< config -> webpac >> parts

  my @supported_inputs = $config->webpac('inputs');
  my $inputs_hash = $config->webpac('inputs');

=cut

sub webpac {
	my $self = shift;

	$self->_get_logger()->logdie("can't find config->webpac") unless defined( $self->{config}->{webpac} );

	my $what = shift || return $self->{config}->{webpac};

	if (wantarray && ref( $self->{config}->{webpac}->{$what} ) eq 'HASH') {
		return keys %{ $self->{config}->{webpac}->{$what} };
	} else {
		return $self->{config}->{webpac}->{$what};
	}

}

=head2 iterate_inputs

  $config->iterate_inputs( sub {
  	my ($input, $database, $database_config_hash) = @_;
	# ... do something with input config hash
  } );

This function will also modify C<< $input->{normalize} >> to
be C<ARRAY>, even with just one element.

=cut

sub iterate_inputs {
	my $self = shift;

	my $log = $self->_get_logger();

	my $code_ref = shift;
	$log->logdie("called with CODE") unless ( ref($code_ref) eq 'CODE' );

	while (my ($database, $db_config) = each %{ $self->{config}->{databases} }) {
		my @inputs;
		if (ref($db_config->{input}) eq 'ARRAY') {
			@inputs = @{ $db_config->{input} };
		} elsif ($db_config->{input}) {
			push @inputs, $db_config->{input};
		} else {
			$log->info("database $database doesn't have inputs defined");
		}

		foreach my $input (@inputs) {
			$log->debug("iterating over input ", dump($input));
			if ( defined( $input->{normalize} ) && ref($input->{normalize}) ne 'ARRAY' ) {
				$input->{normalize} = [ $input->{normalize} ];
			}
			$code_ref->($input, $database, $db_config);
		}
	}

}


=head1 AUTHOR

Dobrica Pavlinusic, C<< <dpavlin@rot13.org> >>

=head1 COPYRIGHT & LICENSE

Copyright 2006 Dobrica Pavlinusic, All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

1; # End of WebPAC::Config
