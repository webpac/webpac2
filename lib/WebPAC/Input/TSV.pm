package WebPAC::Input::TSV;

use warnings;
use strict;

use WebPAC::Input;
use base qw/WebPAC::Common/;

use Encode;
use Data::Dump qw/dump/;

=head1 NAME

WebPAC::Input::TSV - tab separated values

=cut

=head1 FUNCTIONS

=head2 new

  my $input = new WebPAC::Input::TSV(
  	path => '/path/to/records.tsv',
	header_first => 1,
  );

C<header_first> will use first line as header names.

=back

Default encoding of input file is C<utf-8>

=cut

sub new {
	my $class = shift;
	my $self = {@_};
	bless($self, $class);

	my $arg = {@_};

	my $log = $self->_get_logger();

	open( my $fh, '<:raw', $arg->{path} ) || $log->logconfess("can't open $arg->{path}: $!");

	$self->{size} = 0;

	if ( $self->{header_first} ) {
		my $header = <$fh>;
		chomp $header;
		$self->{header_names} = [ split(/\t/,$header) ];
		$self->debug( "header_names = ",dump( $self->{header_names} ) );
	}

	while ( my $line = <$fh> ) {
		chomp $line;

		my $rec;
		$rec->{'000'} = [ ++$self->{size} ];

		my $col = 'A';
		my $header_pos = 0;
		foreach my $v ( split(/\t/,$line) ) {
			$rec->{ $col } = Encode::decode_utf8( $v ) if $v ne '\N';
			$col++;

			if ( $self->{header_names} ) {
				$rec->{ $self->{header_names}->[$header_pos] } =
					Encode::decode_utf8( $v ) if $v ne '\N';
				$header_pos++;
			}
		}

		push @{ $self->{_rec} }, $rec;

	};

	$log->debug("loaded ", $self->size, " records");

	$self ? return $self : return undef;
}

=head2 fetch_rec

Return record with ID C<$mfn> from database

  my $rec = $input->fetch_rec( $mfn, $filter_coderef );

=cut

sub fetch_rec {
	my ( $self, $mfn, $filter_coderef ) = @_;

	return $self->{_rec}->[$mfn-1];
}


=head2 size

Return number of records in database

  my $size = $input->size;

=cut

sub size {
	my $self = shift;
	return $self->{size};
}

=head1 AUTHOR

Dobrica Pavlinusic, C<< <dpavlin@rot13.org> >>

=head1 COPYRIGHT & LICENSE

Copyright 2010 Dobrica Pavlinusic, All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

1; # End of WebPAC::Input::TSV
