package WebPAC::Input::ISI;

use warnings;
use strict;

use WebPAC::Input;
use base qw/WebPAC::Common/;

use Data::Dump qw/dump/;
use Carp qw/confess/;

=head1 NAME

WebPAC::Input::ISI - support for ISI Export Format

=cut

our $VERSION = '0.04';

our $debug = 0;


=head1 SYNOPSIS

Open file in ISI export fromat

 my $input = new WebPAC::Input::ISI(
	path => '/path/to/ISI/records.txt',
 );

=head1 FUNCTIONS

=head2 new

Returns new low-level input API object

  my $input = new WebPAC::Input::ISI(
  	path => '/path/to/ISI/records.txt'
	filter => sub {
		my ($l,$field_nr) = @_;
		# do something with $l which is line of input file
		return $l;
	},
  }

Options:

=over 4

=item path

path to ISI export file

=back

=cut

our $subfields = {
	'CR' => sub {
		my $full_cr = shift;
		my @v = split(/, /, $full_cr);
		my $f = { full => $full_cr };
		foreach ( qw/author year reference volume page doi/ ) {
			if ( my $tmp = shift @v ) {
				$f->{$_} = $tmp;
			}
		}
		if ( $f->{author} =~ /^\*(.+)/ ) {
			delete $f->{author};
			$f->{institution} = $1;
		}
		$f->{doi} =~ s{DOI\s+}{} if $f->{doi}; # strip DOI prefix
		return $f;
	},
};

sub new {
	my $class = shift;
	my $self = {@_};
	bless($self, $class);

	my $arg = {@_};

	open( my $fh, '<', $arg->{path} ) || confess "can't open $arg->{path}: $!";

	my ( $format, $version );

	my $line = <$fh>;
	chomp($line);
	if ( $line =~ /^(\xEF\xBB\xBF)FN\s(.+)$/) {
		$format = $2;
		if ( defined $1 && $self->{encoding} !~ m/utf-8/ ) {
			warn "E: file ", $self->{path}, " is in utf-8 encoding, but config is ", $self->{encoding}, " forcing utf-8\n";
			$self->{encoding} = 'utf-8';
		}
	} else {
		die "first line of $arg->{path} has to be FN, but is: $line";
	}

	$line = <$fh>;
	chomp($line);
	if ( $line =~ /^VR\s(.+)$/) {
		$version = $1;
	} else {
		die "second line of $arg->{path} has to be VN, but is: $line";
	}

	warn "I: $arg->{path} $format $version - generating record offsets\n";

	$self->{fh} = $fh;
	$self->{record_offset} = [];
	$self->{offset} ||= 0;

	while( $line = <$fh> ) {
		chomp($line);
		next unless $line eq 'ER';
		push @{ $self->{record_offset} }, tell($fh);
		last if $self->{limit} && $#{ $self->{record_offset} } >= $self->{limit} - 1 + $self->{offset};
	}
	push @{ $self->{record_offset} }, tell($fh); # end of file

	warn "I $arg->{path} read ", tell($fh), " bytes $#{ $self->{record_offset} } records\n";

	return $self;
}



=head2 fetch_rec

Return record with ID C<$mfn> from database

  my $rec = $input->fetch_rec( $mfn, $filter_coderef );

=cut

sub fetch_rec {
	my ( $self, $mfn, $filter_coderef ) = @_;

	seek $self->{fh}, $self->{record_offset}->[ $mfn - 1 ], 0;

	my $tag;
	my $rec;

	my $fh = $self->{fh};

	while( my $line = <$fh> ) {
		chomp($line);
		my $v;

		if ( $line eq 'EF' ) {
			return;
		} elsif ( $line eq 'ER' ) {

			$line = <$fh>;
			chomp $line;
			die "expected blank like in ",$self->{path}, " +$.: $line" unless $line eq '';

			# join tags
			foreach ( qw/AB DE ID TI SO RP SC FU FX PA JI/ ) {
				$rec->{$_} = join(' ', @{ $rec->{$_} }) if defined $rec->{$_};
			}

			# split on ;
			foreach ( qw/ID SC DE/ ) {
				$rec->{$_} = [ split(/;\s/, $rec->{$_}) ] if defined $rec->{$_};
			}

			$rec->{'000'} = [ $mfn ];
			warn "## mfn $mfn" if $debug;
			return $rec;

		} elsif ( $line =~ /^(\S\S)\s(.+)$/ ) {
			$tag = $1;
			$v = $2;
		} elsif ( $line =~ /^\s{3}(.+)$/ ) {
			$v = $1;
			if ( $tag eq 'CR' && $v =~ m{DOI$} ) {
				my $doi = <$fh>;
				chomp($doi);
				$doi =~ s{^\s{3}}{ } || die "can't find DOI in: $doi";
				$v .= $doi;
			}
		} elsif ( $line =~ m{^(\S\S)\s*$} ) {
			warn "# $self->{path} +$. empty |$line|\n";
		} elsif ( $line ne '' ) {
			warn "E: $self->{path} +$ | can't parse |$line|";
		}

		if ( defined $v ) {
			$v = $subfields->{$tag}->($v) if defined $subfields->{$tag};

			warn "## $tag: ", sub { dump( $v ) } if $debug;
			push @{ $rec->{$tag} }, $v;
		}
	}

	warn "can't get full record $mfn got ", dump $rec;
	return $rec;
}


=head2 size

Return number of records in database

  my $size = $input->size;

=cut

sub size {
	my $self = shift;
	my $size = $#{ $self->{record_offset} };
	return 0 if $size < 0;
	# no need for +1 since we record end of file as last record
	return $size - $self->{offset};
}


=head1 AUTHOR

Dobrica Pavlinusic, C<< <dpavlin@rot13.org> >>

=head1 COPYRIGHT & LICENSE

Copyright 2009 Dobrica Pavlinusic, All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

1; # End of WebPAC::Input::ISI
