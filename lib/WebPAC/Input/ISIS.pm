package WebPAC::Input::ISIS;

use warnings;
use strict;

use WebPAC::Input;
use Biblio::Isis;
use base qw/WebPAC::Common/;

=head1 NAME

WebPAC::Input::ISIS - support for CDS/ISIS database files

=head1 VERSION

Version 0.09

=cut

our $VERSION = '0.09';


=head1 SYNOPSIS

Open CDS/ISIS, WinISIS or IsisMarc database using C<Biblio::Isis>
and read all records to memory.

 my $isis = new WebPAC::Input::ISIS(
	path => '/path/to/ISIS/ISIS',
 );

=head1 FUNCTIONS

=head2 new

Returns new low-level input API object

  my $isis = new WebPAC::Input::ISIS(
  	path => '/path/to/LIBRI'
	filter => sub {
		my ($l,$field_nr) = @_;
		# do something with $l which is line of input file
		return $l;
	},
  }

Options:

=over 4

=item path

path to CDS/ISIS database

=back

=cut

sub new {
	my $class = shift;
	my $self = {@_};
	bless($self, $class);

	my $arg = {@_};

	my $log = $self->_get_logger();

	$log->info("opening ISIS database '$arg->{path}'");

	$log->debug("using Biblio::Isis");
	my $isis_db = new Biblio::Isis(
		isisdb => $arg->{path},
		include_deleted => 1,
		hash_filter => $arg->{filter} ? sub { return $arg->{filter}->(@_); } : undef,
		ignore_empty_subfields => 1,
	) or $log->logdie("can't find database $arg->{path}");

	$self->{_isis_db} = $isis_db;

	$self ? return $self : return undef;
}

=head2 fetch_rec

Return record with ID C<$mfn> from database

  my $rec = $isis->fetch_rec( $mfn, $filter_coderef);

=cut

sub fetch_rec {
	my $self = shift;

	my ($mfn, $filter_coderef) = @_;

	my $rec = $self->{_isis_db}->to_hash({
		mfn => $mfn,
		include_subfields => 1,
		hash_filter => $filter_coderef,
#		hash_filter => sub {
#			my ($l,$f) = @_;
#			warn "## in hash_filter ($l,$f)\n";
#			my $o = $filter_coderef->($l,$f) if ($filter_coderef);
#			warn "## out hash_filter = $o\n";
#			return $o;
#		},
	});

	return $rec;
}

=head2 dump_ascii

Return dump of record ID C<$mfn> from database

  my $rec = $isis->dump_ascii( $mfn );

=cut

sub dump_ascii {
	my $self = shift;

	my $mfn = shift;

	return $self->{_isis_db}->to_ascii( $mfn );
}

=head2 size

Return number of records in database

  my $size = $isis->size;

=cut

sub size {
	my $self = shift;
	return $self->{_isis_db}->count;
}

=head1 AUTHOR

Dobrica Pavlinusic, C<< <dpavlin@rot13.org> >>

=head1 COPYRIGHT & LICENSE

Copyright 2005 Dobrica Pavlinusic, All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

1; # End of WebPAC::Input::ISIS
