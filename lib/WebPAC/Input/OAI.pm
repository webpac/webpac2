package WebPAC::Input::OAI;

use warnings;
use strict;

use HTTP::OAI;
use HTTP::OAI::Metadata::OAI_DC;
use base qw/WebPAC::Common/;
use Carp qw/confess/;
use Data::Dump qw/dump/;

=head1 NAME

WebPAC::Input::OAI - read MARC records from OAI

=cut

our $VERSION = '0.00';

=head1 FUNCTIONS

=head2 new

  my $input = new WebPAC::Input::OAI(
	url   => 'http://arXiv.org/oai2',
	from  => '2001-02-03',
	until => '2001-04-10',
	path  => 'var/oai/arXiv',
  }

=cut

sub new {
	my $class = shift;
	my $self = {@_};
	bless($self, $class);

	my $arg = {@_};

	my $log = $self->_get_logger();
	$log->debug( 'arg = ', dump($arg) );

	open(my $fh, '<', $arg->{path});
	if ( ! $fh ) {
		$log->error("can't open $arg->{path}: $!");
		return;
	}

	my $h = HTTP::OAI::Harvester->new( baseURL => $self->{url} );

	my $list;
	$list->{$_} = $self->{$_} foreach ( qw( from until ) );

	$log->info("ListRecords ", dump($list));

	my $response = $h->ListRecords(
		metadataPrefix=>'oai_dc',
		handlers=>{metadata=>'HTTP::OAI::Metadata::OAI_DC'},
		%$list,
	);

	warn "## ",dump($response);

	if ( $response->is_error ) {
		$log->logdie("Error harvesting $self->{url}: $response->message");
	}

	$self->{oai_response} = $response;

	$self ? return $self : return undef;
}

=head2 fetch_rec

Return record with ID C<$mfn> from database

  my $rec = $input->fetch_rec( $mfn );

=cut

sub fetch_rec {
	my $self = shift;

	my $mfn = shift;

	my $rec = $self->{oai_response}->next;

	my $row = $rec->metadata->dc;
	warn "# row ",dump($row);

	push @{$row->{'000'}}, $mfn;
	return $row;
}

=head2 size

Return number of records in database

  my $size = $isis->size;

=cut

sub size {
	my $self = shift;
	return $self->{oai_response}->resumptionToken->completeListSize;
}


=head1 AUTHOR

Dobrica Pavlinusic, C<< <dpavlin@rot13.org> >>

=head1 COPYRIGHT & LICENSE

Copyright 2011 Dobrica Pavlinusic, All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

1;
