package WebPAC::Input::AK;

use warnings;
use strict;

use WebPAC::Input;
use base qw/WebPAC::Common/;

use Data::Dump qw/dump/;
use Carp qw/confess/;
use Text::CSV;

=head1 NAME

WebPAC::Input::AK - support for AK Export Format

=cut

our $VERSION = '0.02';

our $debug = 0;


=head1 SYNOPSIS

Open file in AK export fromat

 my $input = new WebPAC::Input::AK(
	path => '/path/to/AK/records.csv',
 );

=head1 FUNCTIONS

=head2 new

Returns new low-level input API object

  my $input = new WebPAC::Input::AK(
  	path => '/path/to/AK/records.csv'
	filter => sub {
		my ($l,$field_nr) = @_;
		# do something with $l which is line of input file
		return $l;
	},
  }

Options:

=over 4

=item path

path to AK export file

=back

=cut

sub new {
	my $class = shift;
	my $self = {@_};
	bless($self, $class);

	my $arg = {@_};

	$self->{csv} = Text::CSV->new ( { binary => 1 } ) or confess Text::CSV->error_diag ();
	open( my $fh, '<:encoding(utf-8)', $arg->{path} ) || confess "can't open $arg->{path}: $!";

	$self->{fh} = $fh;
	$self->{record_offset} = [];
	$self->{offset} ||= 0;

	my $tell = tell($fh);

	while( my $row = $self->{csv}->getline( $fh ) ) {
		push @{ $self->{record_offset} }, $tell;
		last if $self->{limit} && $#{ $self->{record_offset} } >= $self->{limit} - 1 + $self->{offset};
		$tell = tell($fh);
	}

	warn "I $arg->{path} read ", tell($fh), " bytes $#{ $self->{record_offset} } records\n";

	return $self;
}



=head2 fetch_rec

Return record with ID C<$mfn> from database

  my $rec = $input->fetch_rec( $mfn, $filter_coderef );

=cut

sub fetch_rec {
	my ( $self, $mfn, $filter_coderef ) = @_;

	my $fh = $self->{fh};

	seek $fh, $self->{record_offset}->[ $mfn - 1 ], 0;

	my $rec;

	my ( $tag, $v );

#	while( my $line = <$fh> ) {
	my $row = $self->{csv}->getline( $fh );
	my $parse = pop @$row;
warn "## row = ", dump($row);

	foreach ( 0 .. $#$row ) {
		$rec->{ "_COL_" . chr(ord('A') + $_) } = $row->[$_];
	}

	foreach my $line ( split(/[\r\n]/,$parse) ) {

		next if $line eq '';

		if ( $line =~ m/^\s{19}(.*)/ ) {
			$v .= "\n" . $1;
		} elsif ( $line =~ m/^\s*([A-Z].{4,20}?):\s(.+)/ ) {

			if ( $tag && $v ) {
				$rec->{$tag} = $v;
			}

			$tag = $1;
			$v   = $2;
		} elsif ( $tag =~ m/(Autorentext|Einbandart|Titelerg.|Zusammenfassung)/ ) {
			$v .= "\n" . $line;
		} else {
			$v .= "\n" . $line;
			#confess "can't parse ",dump($line), " last tag: $tag";
			warn "XXX tag: $tag using [$line] as countinuation\n";
		}
	}

	if ( $tag && $v ) {
		$rec->{$tag} = $v;
	}

	$rec->{'000'} = [ $mfn ];
	warn "## mfn $mfn" if $debug;

	return $rec;
}


=head2 size

Return number of records in database

  my $size = $input->size;

=cut

sub size {
	my $self = shift;
	my $size = $#{ $self->{record_offset} };
	return 0 if $size < 0;
	# no need for +1 since we record end of file as last record
	return $size - $self->{offset};
}


=head1 AUTHOR

Dobrica Pavlinusic, C<< <dpavlin@rot13.org> >>

=head1 COPYRIGHT & LICENSE

Copyright 2015 Dobrica Pavlinusic, All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

1; # End of WebPAC::Input::AK
