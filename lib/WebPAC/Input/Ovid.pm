package WebPAC::Input::Ovid;

use warnings;
use strict;

use lib 'lib';
use WebPAC::Input;
use base qw/WebPAC::Common/;

use Data::Dump qw/dump/;

=head1 NAME

WebPAC::Input::Ovid - support for Ovid citation export (BRS/Tagged and Direct Export)

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';
our $debug = 0;

=head1 SYNOPSIS

Open file in Ovid citation export fromat

 my $input = new WebPAC::Input::Ovid(
	path => '/path/to/ovid-cites.txt',
 );

You can also specify file glob:

  my $input = WebPAC::Input::Ovid->new( path => '/path/to/ovid.*.txt' );

=head1 FUNCTIONS

=head2 new

Returns new low-level input API object

  my $input = new WebPAC::Input::Ovid(
  	path => '/path/to/ebsco.txt',
	filter => sub {
		my ($l,$field_nr) = @_;
		# do something with $l which is line of input file
		return $l;
	},
  }

Options:

=over 4

=item path

path to Ovid export file

=back

=cut

sub new {
	my $class = shift;
	my $self = {@_};
	bless($self, $class);

	my $arg = {@_};

	my $log = $self->_get_logger();

	my @paths;

	if ( $arg->{path} =~ m/\*/ ) {
		@paths = glob $arg->{path};
	} else {
		@paths = ( $arg->{path} );
	}

	my $size = 0;
	$self->{_rec} = [];

	my $max_size;
	$max_size = ( $self->{offset} || 0 ) + $self->{limit} if $self->{limit};

	foreach my $path ( @paths ) {

		open( my $fh, '<', $path ) || $log->logconfess("can't open $path: $!");
		$log->info("reading '$path'");

		my $tag;
		my $rec;

		while( my $line = <$fh> ) {
			$line =~ s{[\r\n]+$}{};
			next if $line eq '';

			warn "<< $line\n" if $debug;

			if ( $line =~ m/^<(\d+)>$/ ) {
				last if $max_size && $size > $max_size;

				push @{ $self->{_rec} }, $rec if $rec;
				warn "## rec = ",dump( $rec ),$/ if $debug;
				my $expect_rec = $#{ $self->{_rec} } + 2;
				warn "wrong Ovid record number: $1 != $expect_rec" if $debug && $1 != $expect_rec;
				$rec = { '000' => [ ++$size ] };
			} elsif ( $line =~ /^(\w+)\s+(-\s)?(.*)/ ) {
				$tag = $1;
				warn "++ $tag\n" if $debug;
				$rec->{$tag} = [ $3 ] if $3;
			} elsif ( $line =~ /^\s+(-\s)?(.+)/ ) {
				push @{ $rec->{$tag} }, $2;
			} else {
				warn "### skip: '$line'\n" if $debug;
			}

		}

		# save last rec
		push @{ $self->{_rec} }, $rec if $rec;
		warn "### rec ",dump $rec if $debug;

		$log->debug("loaded ", $self->size, " records from $path");

	}

	$self ? return $self : return undef;
}

=head2 fetch_rec

Return record with ID C<$mfn> from database

  my $rec = $input->fetch_rec( $mfn, $filter_coderef );

=cut

sub fetch_rec {
	my $self = shift;

	my ( $mfn, $filter_coderef ) = @_;

	return $self->{_rec}->[$mfn-1];
}


=head2 size

Return number of records in database

  my $size = $input->size;

=cut

sub size {
	my $self = shift;
	return $#{ $self->{_rec} } + 1;
}

=head1 AUTHOR

Dobrica Pavlinusic, C<< <dpavlin@rot13.org> >>

=head1 COPYRIGHT & LICENSE

Copyright 2008 Dobrica Pavlinusic, All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

1; # End of WebPAC::Input::Ovid
