package WebPAC::Input::Test;

use warnings;
use strict;

use WebPAC::Input;
use WebPAC::Common;
use base qw/WebPAC::Common/;
use Data::Dump qw/dump/;

=head1 NAME

WebPAC::Input::Test - Mock test records for WebPAC

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';

=head1 SYNOPSIS

Setup test record

 $WebPAC::Input::Test::rec = {
 	'200' => [ {
		'a' => 'foo',
		'b' => 'bar',
		}, {
		'a' => 'baz',
		} ],
	'900' => [
		'foobar',
		],
  };

Setup optional size

  $WebPAC::Input::Test::size = 42;

=head1 FUNCTIONS

=head2 new

  my $isis = new WebPAC::Input::Test(
  	path => '/path/to/LIBRI'
	filter => sub {
		my ($l,$field_nr) = @_;
		# do something with $l which is line of input file
		return $l;
	},
  }

C<filter> will be assigned to C<$WebPAC::Input::Test::filter>

=cut

our $filter;

sub new {
	my $class = shift;
	my $self = {@_};
	bless($self, $class);

	my $arg = {@_};

	$filter = $arg->{filter};

	$self->_get_logger()->info("mocking Test database with args = ", dump($arg));

	$self ? return $self : return undef;
}

=head2 fetch_rec

Return record with ID C<$mfn> from database

  my $rec = $isis->fetch_rec( $mfn );

Second argument, C<filter_coderef> will be assigned to
C<$WebPAC::Input::Test::filer_coderef>

=cut

our $rec;
our $filter_coderef;

sub fetch_rec {
	my $self = shift;

	my $mfn = shift;
	$filter_coderef = shift;

	$self->_get_logger()->debug("mfn = $mfn rec = ", dump($rec));

	return $rec;
}

=head2 dump_ascii

Return dump of record ID C<$mfn> from database

  my $rec = $isis->dump_ascii( $mfn );

=cut

sub dump_ascii {
	my $self = shift;

	return dump( $rec );
}

=head2 size

Return number of records in database

  my $size = $isis->size;

=cut

our $size = 1;

sub size {
	my $self = shift;
	$self->_get_logger()->debug("size = $size");
	return $size;
}

=head1 AUTHOR

Dobrica Pavlinusic, C<< <dpavlin@rot13.org> >>

=head1 COPYRIGHT & LICENSE

Copyright 2007 Dobrica Pavlinusic, All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

1; # End of WebPAC::Input::Test
