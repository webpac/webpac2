package WebPAC::Input::EBSCO;

use warnings;
use strict;

use WebPAC::Input;
use base qw/WebPAC::Common/;

use Data::Dump qw/dump/;

=head1 NAME

WebPAC::Input::EBSCO - support for EBSCO text export

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';

=head1 SYNOPSIS

Open file in EBSCO text export fromat

 my $input = new WebPAC::Input::EBSCO(
	path => '/path/to/ebsco.txt',
 );

=head1 FUNCTIONS

=head2 new

Returns new low-level input API object

  my $input = new WebPAC::Input::EBSCO(
  	path => '/path/to/ebsco.txt',
	filter => sub {
		my ($l,$field_nr) = @_;
		# do something with $l which is line of input file
		return $l;
	},
  }

Options:

=over 4

=item path

path to EBSCO export file

=back

=cut

my $subfields = {
	'CR' => sub {
		my @v = split(/, /, shift);
		my $f;
		foreach ( qw/author year reference volume page/ ) {
			if ( my $tmp = shift @v ) {
				$f->{$_} = $tmp;
			}
		}
		if ( $f->{author} =~ /^\*(.+)/ ) {
			delete $f->{author};
			$f->{institution} = $1;
		}
		return $f;
	},
};

sub new {
	my $class = shift;
	my $self = {@_};
	bless($self, $class);

	my $arg = {@_};

	my $log = $self->_get_logger();

	open( my $fh, '<', $arg->{path} ) || $log->logconfess("can't open $arg->{path}: $!");

	$log->info("reading '$arg->{path}'");

	my $tag;
	my $rec;

	$self->{size} = 0;

	while( my $line = <$fh> ) {
		chomp($line);
		# remove extra spaces at end (?!)
		$line =~ s/\s+$//;

		my $v;

		if ( $line =~ m/^-+$/ ) {
			# join tags
#			foreach ( qw/AB DE ID TI/ ) {
#				$rec->{$_} = join(' ', @{ $rec->{$_} }) if defined $rec->{$_};
#			}
			$rec->{'000'} = [ ++$self->{size} ];
			push @{ $self->{_rec} }, $rec;
			$rec = {};
		} elsif ( $line =~ /^(\S\S)-\s(.+)$/ ) {
				$tag = $1;
				$v = $2;
		} else {
			warn "### skip: $line\n";
		}

		if ( defined $v ) {
			$v = $subfields->{$tag}->($v) if defined $subfields->{$tag};

			$log->debug("$tag: ", sub { dump( $v ) });
			push @{ $rec->{$tag} }, $v;
		}

	}

	# save last rec
	$rec->{'000'} = [ ++$self->{size} ];
	push @{ $self->{_rec} }, $rec;

	$log->debug("loaded ", $self->size, " records");

	$self ? return $self : return undef;
}

=head2 fetch_rec

Return record with ID C<$mfn> from database

  my $rec = $input->fetch_rec( $mfn, $filter_coderef );

=cut

sub fetch_rec {
	my $self = shift;

	my ( $mfn, $filter_coderef ) = @_;

	return $self->{_rec}->[$mfn-1];
}


=head2 size

Return number of records in database

  my $size = $input->size;

=cut

sub size {
	my $self = shift;
	return $self->{size};
}

=head1 AUTHOR

Dobrica Pavlinusic, C<< <dpavlin@rot13.org> >>

=head1 COPYRIGHT & LICENSE

Copyright 2008 Dobrica Pavlinusic, All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

1; # End of WebPAC::Input::EBSCO
