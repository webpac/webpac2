package WebPAC::Output;

use warnings;
use strict;

use Carp qw/confess/;
use Data::Dump qw/dump/;

=head1 NAME

WebPAC::Output - The great new WebPAC::Output!

=cut

=head1 SYNOPSIS

Common routines for output formats

=head1 FUNCTIONS

=head2 ds_to_hash

  my $hash = $self->ds_to_hash( $ds, 'display'
  	disable_key_mungle => 0,
	singe_values = 0,
  );

=cut

sub ds_to_hash {
	my $self = shift;

	my $ds = shift || confess "need ds";
	my $type = shift || confess "need type";

	my $opt = {@_};

	my $hash;

	foreach my $t ( keys %$ds ) {
		my $name = $t;
		if ( ! $opt->{disable_key_mungle} ) {
			$name = lc($name);
			$name =~ s/\W+/_/g;
		}

		my $v = $ds->{$t} || die "bug";

		# FIXME get rid of non hash values in data_structure for consistency?
		next unless ref($v) eq 'HASH';

		if ( defined( $v->{$type} ) ) {
			if ( $opt->{single_values} && ref($v->{$type}) eq 'ARRAY' ) {
				$hash->{$name} = join(' ', map {
					if(ref($_)) {
						dump($_);
					} else {
						$_;
					}
				} @{ $v->{$type} });
			} else {
				$hash->{$name} = $v->{$type};
			}
		}
	}

	return $hash;
}


=head1 AUTHOR

Dobrica Pavlinusic, C<< <dpavlin@rot13.org> >>

=head1 COPYRIGHT & LICENSE

Copyright 2005-2007 Dobrica Pavlinusic, All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

1; # End of WebPAC::Output
