package WebPAC::Test;
use Exporter 'import';
@EXPORT = qw/
	$debug
	$abs_path
	%LOG

	dump
	abs_path
	read_file write_file

	dies_ok throws_ok
/;

sub BEGIN {

	use Cwd qw/abs_path/;
	use File::Slurp;
	use Getopt::Long;
	use Data::Dump qw/dump/;
	use Test::Exception;

	use lib 'lib';

	our $debug = 0;

	GetOptions(
		"debug+", \$debug
	);

	warn '# BEGIN' if $debug;

	our $abs_path = abs_path($0);
	$abs_path =~ s#/[^/]*$#/#;	#vim
	warn "# abs_path: $abs_path" if $debug;

	our %LOG = (
		debug => $debug,
		no_log => $debug ? 0 : 1,
	);
}

1;
