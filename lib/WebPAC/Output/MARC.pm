package WebPAC::Output::MARC;

use warnings;
use strict;

use base qw/WebPAC::Common/;

use MARC::Record;
use MARC::File::XML;
use MARC::Lint;
use Data::Dump qw/dump/;

=head1 NAME

WebPAC::Output::MARC - Create MARC records from C<marc_*> normalisation rules

=head1 VERSION

Version 0.04

=cut

our $VERSION = '0.04';

=head1 SYNOPSIS

Create MARC records from C<marc_*> normalisation rules described in
L<WebPAC::Normalize>.


=head1 FUNCTIONS

=head2 new

  my $marc = new WebPAC::Output::MARC(
  	path => '/path/to/output.marc',
	marc_encoding => 'utf-8',
	lint => 1,
	dump => 0,
	marcxml => 0,
  )

=cut

sub new {
	my $class = shift;
	my $self = {@_};
	bless($self, $class);

	my $log = $self->_get_logger;

	if ($self->{lint}) {
		$self->{lint}= new MARC::Lint or
			$log->warn("Can't create MARC::Lint object, linting is disabled");
	}

	$self->{marc_encoding} ||= 'utf-8';

	if (my $path = $self->{path}) {
		open($self->{fh}, '>', $path . '.marc') ||
			$log->logdie("can't open MARC output $path: $!");
		binmode($self->{fh}, ':utf8');

		$log->info("Creating MARC export file $path.marc", $self->{lint} ? ' (with lint)' : '', " encoding ", $self->{marc_encoding}, "\n");
		if ( $self->{marcxml} || $ENV{MARCXML} ) {
			open($self->{fh_marcxml}, '>:utf8', "$path.marcxml") ||
				$log->logdie("can't open MARCXML output $path.marcxml: $!");
			$log->info("Creating MARCXML export file $path.marcxml");
			print {$self->{fh_marcxml}} qq{<?xml version="1.0" encoding="UTF-8"?>\n<collection>\n};
		}
	} else {
		$log->logconfess("new called without path");
	}

	$self ? return $self : return undef;
}

=head2 add

  $marc->add(
	id => $mfn,
	fields => WebPAC::Normalize::_get_marc_fields(),
	leader => WebPAC::Normalize::_get_marc_leader(),
	row => $row,
  );

C<row> is optional parametar which is used when dumping original row to
error log.

=cut

sub add {
	my $self = shift;

	my $arg = {@_};

	my $log = $self->_get_logger;

	$log->logconfess("add needs fields and id arguments")
		unless ($arg->{fields} && defined $arg->{id});

	my $marc = new MARC::Record;
	$marc->encoding( $self->{marc_encoding} );

	my $id = $arg->{id};

	$log->logconfess("fields isn't array") unless (ref($arg->{fields}) eq 'ARRAY');

	my $fields = $arg->{fields};

	$log->debug("original fields = ", sub { dump( $fields ) });

	# recode fields to marc_encoding
	foreach my $j ( 0 .. $#$fields ) {
		foreach my $i ( 0 .. ( ( $#{$fields->[$j]} - 3 ) / 2 ) ) {
			my $f = $fields->[$j]->[ ($i * 2) + 4 ];
			$fields->[$j]->[ ($i * 2) + 4 ] = $f;
		}
	}

	# sort fields
	@$fields = sort { $a->[0] <=> $b->[0] } @$fields;

	$log->debug("recode fields = ", sub { dump( $fields ) });

	$marc->add_fields( @$fields );

	# tweak leader
	if (my $new_l = $arg->{leader}) {

		my $leader = $marc->leader;

		foreach my $o ( sort { $a <=> $b } keys %$new_l ) {
			my $insert = $new_l->{$o};
			$leader = substr($leader, 0, $o) .
				$insert . substr($leader, $o+length($insert));
		}
		$marc->leader( $leader );
	}

	if ($self->{lint}) {
		$self->{lint}->check_record( $marc );
		my @w = $self->{lint}->warnings;
		if (@w) {
			$log->error("MARC lint detected warning on record $id\n",
				"<<<<< Original input row:\n",dump($arg->{row}), "\n",
				">>>>> Normalized MARC row: leader: [", $marc->leader(), "]\n", dump( $fields ), "\n",
				"!!!!! MARC lint warnings:\n",join("\n",@w),"\n"
			);
			map { $self->{_marc_lint_warnings}->{$_}++ } @w;
		}
	}

	if ($self->{dump}) {
		$log->info("MARC record on record $id\n",
			"<<<<< Original imput row:\n",dump($arg->{row}), "\n",
			">>>>> Normalized MARC row: leader: [", $marc->leader(), "]\n", dump( $fields ), "\n",
		);
	}

	print {$self->{fh}} $marc->as_usmarc;

	if ( $self->{fh_marcxml} ) {
		my $xml = $marc->as_xml_record;
		$xml =~ s/\Q<?xml version="1.0" encoding="UTF-8"?>\E//;
		print {$self->{fh_marcxml}} $xml;
	}
}

=head2 finish

Close MARC output file

  $marc->finish;

It will also dump MARC lint warnings summary if called with C<lint>.

=cut

sub finish {
	my $self = shift;

	my $log = $self->get_logger;

	close( $self->{fh} ) or $log->logdie("can't close ", $self->{path}, ".marc: $!");

	if ( $self->{fh_marcxml} ) {
		print {$self->{fh_marcxml}} qq{</collection>\n};
		$log->info("MARCXML file ", $self->{path}, ".marcxml ", -s $self->{fh_marcxml}, " bytes");
		close( $self->{fh_marcxml} );
	}
	if (my $w = $self->{_marc_lint_warnings}) {
		$log->error("MARC lint warnings summary:\n",
			join ("\n",
				map { $w->{$_} . "\t" . $_ }
				sort { $w->{$b} <=> $w->{$a} } keys %$w
			)
		);
	}
}

=head1 AUTHOR

Dobrica Pavlinusic, C<< <dpavlin@rot13.org> >>

=head1 COPYRIGHT & LICENSE

Copyright 2006 Dobrica Pavlinusic, All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

1; # End of WebPAC::Output::MARC
