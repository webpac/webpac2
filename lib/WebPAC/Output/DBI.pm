package WebPAC::Output::DBI;

use warnings;
use strict;

use base qw/WebPAC::Common WebPAC::Output Class::Accessor/;
__PACKAGE__->mk_accessors(qw(
	input

	dsn
	user
	passwd

	schema

	table

	no_transaction
));

use Data::Dump qw/dump/;
use DBI;
use File::Slurp;

=head1 NAME

WebPAC::Output::DBI - feed data into RDBMS via DBI

=head1 FUNCTIONS

=head2 init

  $out->init;

=cut

sub init {
	my $self = shift;
	my $log = $self->_get_logger;

	$log->info($self->dsn);

	$self->{_sth} = {};

	$self->{_dbh} = DBI->connect( $self->dsn, $self->user, $self->passwd, { RaiseError => 1 } );

	$self->{_dbh}->begin_work unless $self->no_transaction;

	if ( -e $self->schema ) {
		foreach my $sql ( split(/;/, scalar read_file( $self->schema )) ) {
			$log->debug( $sql );
			eval { $self->{_dbh}->do( $sql ); };
		}
	}

	return 1;
}


=head2 add

Adds one entry to database.

  $out->add( 42, $ds );

=cut

sub add {
	my $self = shift;

	my ( $id, $ds ) = @_;

	return unless defined $ds->{_rows};

	my $log = $self->_get_logger;

	$id = $self->input . '-' . $id if $self->input;

	foreach my $table ( keys %{ $ds->{_rows} } ) {

		my @rows = @{ $ds->{_rows}->{$table} };
		foreach my $row ( @rows ) {

			my @cols = sort keys %$row;

			my $sth_id = $table . ':' . join(',',@cols);

			my $sth
				= $self->{_sth}->{$sth_id}
				;
			
			if ( ! $sth ) {

				my $sql = join( ''
					, 'insert into '
					, $table
					. ' (' . join(',', @cols), ')'
					, ' values ('
					, join(',', map { '?' } 0 .. $#cols )
					, ')'
				);

				$log->debug( "SQL $sth_id: $sql" );

				$sth
					= $self->{_sth}->{$sth_id}
					= $self->{_dbh}->prepare( $sql )
					;
			};

			$log->debug( "row $table ", sub { dump( $row ) } );
			$sth->execute( map { $row->{$_} } @cols );
	
		}
	}

	return 1;
}

=head2 finish

 $out->finish;

=cut

sub finish {
	my $self = shift;

	my $log = $self->_get_logger();

	$log->info('finish');

	unless ( $self->no_transaction ) {
		$self->{_dbh}->commit;
		$log->info('commit done');
	}

	return 1;
}

=head1 AUTHOR

Dobrica Pavlinusic, C<< <dpavlin@rot13.org> >>

=head1 COPYRIGHT & LICENSE

Copyright 2009 Dobrica Pavlinusic, All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

1; # End of WebPAC::Output::CouchDB
