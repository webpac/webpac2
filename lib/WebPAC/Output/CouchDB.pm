package WebPAC::Output::CouchDB;

use warnings;
use strict;

use base qw/WebPAC::Common WebPAC::Output Class::Accessor/;
__PACKAGE__->mk_accessors(qw(
	input
	url
	database
));

use Data::Dump qw/dump/;
use Net::CouchDb;
use URI;

=head1 NAME

WebPAC::Output::CouchDB - feed data into CouchDB

=head1 FUNCTIONS

=head2 init

  $out->init;

=cut

sub init {
	my $self = shift;
	my $log = $self->_get_logger;

	$log->debug('init');

	my $u = URI->new( $self->url );

	my $cdb = Net::CouchDb->new(
		host => $u->host || "localhost",
		port => $u->port || 5984,
	);

	my $database = $self->database || 'webpac2';

	$log->info("CouchDB database $database info ", dump( $cdb->server_info ) );

	$cdb->debug( $self->debug );
	eval { $cdb->delete_db( $self->database ) };
	$cdb->create_db( $self->database );
	$self->{_cdb} = $cdb->db( $self->database );

	return 1;
}


=head2 add

Adds one entry to database.

  $out->add( 42, $ds );

=cut

sub add_row {
	my $self = shift;

	my ( $id, $ds ) = @_;

	my $log = $self->_get_logger;

	$id = $self->input . '-' . $id if $self->input;

	my $doc = Net::CouchDb::Document->new( $id, $ds );
	$self->{_cdb}->put( $doc );

	return 1;
}

=head2 finish

 $out->finish;

=cut

sub finish {
	my $self = shift;

	my $log = $self->_get_logger();

	$log->info('finish');
	1;

}

=head1 AUTHOR

Dobrica Pavlinusic, C<< <dpavlin@rot13.org> >>

=head1 COPYRIGHT & LICENSE

Copyright 2009 Dobrica Pavlinusic, All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

1; # End of WebPAC::Output::CouchDB
