package WebPAC::Output::Jifty;

use warnings;
use strict;

use base qw/WebPAC::Common WebPAC::Output Class::Accessor/;
__PACKAGE__->mk_accessors(qw(
	path
	model
));

use File::Path;
use Data::Dump qw/dump/;
use WebPAC::Common qw/force_array/;
use Carp qw/confess/;
use Cwd;
use File::Slurp;

use Jifty;

=head1 NAME

WebPAC::Output::Jifty - fill Jifty model from WebPAC data

=cut

our $VERSION = '0.01';

=head1 SYNOPSIS

This is simple output which will fill one Jifty model with data

=head1 FUNCTIONS

=head2 new

 my $output = new WebPAC::Output::Jifty({
 	path => '/path/to/Jifty',
	model => 'Webpacus::Model::UFO',
 });

=head2 init

 $output->init;

=cut

sub init {
	my $self = shift;

	my $log = $self->_get_logger;

	foreach my $p (qw/path model/) {
		$log->logdie("need $p") unless ($self->$p);
	}

	my $path = $self->path;

	$log->logdie("Jifty path $path not found: $!") unless -d $path;

	my $config_path = "$path/etc/config.yml";

	$log->logdie("expected Jifty config at $config_path: $!") unless -e $config_path;

	my $model = $self->model || confess "no model?";

	$log->info("init Jifty at $path using model $model");
	my $webpac_dir = getcwd();
	chdir $path || $log->logdie("can't chdir($path): $!");
	Jifty->new();
	chdir $webpac_dir || $log->logdie("can't chdir($webpac_dir): $!");

	my $system_user = Webpacus::CurrentUser->superuser;
	$self->{_model} = $model->new(current_user => $system_user);

}


=head2 add

Adds one entry

  $output->add( 42, $ds );

=cut

my $stat;

sub add {
	my $self = shift;

	my ( $id, $ds ) = @_;

	my $log = $self->_get_logger;
	$log->logdie("need id") unless defined $id;
	$log->logdie("need ds") unless $ds;

	$log->debug("id: $id ds = ",sub { dump($ds) });

	my $stat;

	my $hash = $self->ds_to_hash( $ds, 'display', single_values => 1 ) || next;

	$log->debug("data: ", sub { dump( $hash ) });

	my ( $m_id, $msg ) = $self->{_model}->load_or_create( %$hash );
	$log->debug("ID: $m_id $msg");

	push @{ $stat->{$msg} }, $m_id;

	return 1;
}

=head2 finish

  $output->finish;

=cut

sub finish {
	my $self = shift;

	my $log = $self->_get_logger;
	$log->debug("stats: ", dump( $stat ));

	return 1;
}

=head1 AUTHOR

Dobrica Pavlinusic, C<< <dpavlin@rot13.org> >>

=head1 COPYRIGHT & LICENSE

Copyright 2007 Dobrica Pavlinusic, All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

1;
