package WebPAC::Output::Webpacus;

use warnings;
use strict;

use base qw/WebPAC::Common WebPAC::Output Class::Accessor/;
__PACKAGE__->mk_accessors(qw(
	path
	database
	input
));

use File::Path;
use Data::Dump qw/dump/;
use WebPAC::Common qw/force_array/;
use Carp qw/confess/;
use Cwd;
use File::Slurp;

use Jifty;

=head1 NAME

WebPAC::Output::Webpacus - integrate WebPAC front-end with Jifty back-end

=cut

our $VERSION = '0.03';

=head1 SYNOPSIS

Does black magic to sync data between WebPAC and Webpacus, web front-end
implement in Jifty

=head1 FUNCTIONS

=head2 new

 my $output = new WebPAC::Output::Webpacus({
 	path => '/path/to/Webpacus',
	database => 'demo',
 });

=head2 init

 $output->init;

=cut

sub init {
	my $self = shift;

	my $log = $self->_get_logger;

	foreach my $p (qw/path database/) {
		$log->logdie("need $p") unless ($self->$p);
	}

	my $path = $self->path;

	$log->logdie("Webpacus path $path not found: $!") unless -d $path;

	my $config_path = "$path/etc/config.yml";

	$log->logdie("expected Webpacus config at $config_path: $!") unless -e $config_path;

	$self->{fields} = {};

}


=head2 add

Adds one entry

  $est->add( 42, $ds );

=cut

sub add {
	my $self = shift;

	my ( $id, $ds ) = @_;

	my $log = $self->_get_logger;
	$log->logdie("need id") unless defined $id;
	$log->logdie("need ds") unless $ds;

	$log->debug("id: $id ds = ",sub { dump($ds) });

	my $stat;

	foreach my $type ( $self->consume_outputs ) {

		my $hash = $self->ds_to_hash( $ds, $type ) || next;

		$log->debug("$type has following data: ", sub { dump( $hash ) });

		foreach my $f ( keys %$hash ) {
			$self->{fields}->{$type}->{$f}++;
			$stat->{$type}->{$f}++;
		}
	}

	$log->debug("this record added following fields: ", sub { dump( $stat ) });

	return 1;
}

=head2 finish

Close index

 my $affected = $index->finish;

Returns of records saved in total

=cut

sub finish {
	my $self = shift;

	my $log = $self->_get_logger();

	my $fields = $self->{fields} || confess "no fields?";

	$log->debug("fields = ", sub { dump $fields });

	$log->info("init Jifty");
	my $path = $self->path || confess "no path?";
	my $webpac_dir = getcwd();
	chdir $path || $log->logdie("can't chdir($path) $!");
	Jifty->new();

	my $affected = 0;

	foreach my $type ( $self->consume_outputs ) {
		next unless defined $fields->{$type};
		$affected += $self->_sync_field(
			$self->database, $type, $fields->{$type}
		);
	}



	my $glue_path = "$path/lib/Webpacus/Webpac.pm";

	$log->debug("creating clue class Webpacus::Webpac at $glue_path");

	my $glue = <<"_END_OF_GLUE_";
package Webpacus::Webpac;

=head1 NAME

Webpacus::Webpac - configuration exported from WebPAC

=cut

use strict;
use warnings;

sub index_path { '/data/webpac2/var/kinosearch/' };

1;
_END_OF_GLUE_

	$log->debug("glue source:\n$glue");

	write_file( $glue_path, $glue ) || $log->logdie("can't create $glue_path: $!");

	return $affected;
};

sub _sync_field {
	my $self = shift;

	my ( $database, $type, $field_hash ) = @_;

	my $path = $self->path || confess "no path?";

	my $log = $self->_get_logger();

	my $model = 'Webpacus::Model::' . ucfirst($type);
	$log->info("sync $model");

	$log->debug("field_hash = ",sub { dump($field_hash) });

	my @field_names = keys %$field_hash;

	if ( ! @field_names ) {
		$log->warn("normalization rules don't produce any data for search!");
		return;
	}

	$log->info("syncing $database $type fields: ", join(", ", @field_names));

	my $system_user = Webpacus::CurrentUser->superuser;
	my $o = $model->new(current_user => $system_user);

	my ( $count, $new, $updated ) = ( 0, 0, 0 );

	foreach my $field ( @field_names ) {
		my $items = $field_hash->{$field} || confess "no field?";

		my ( $id, $msg ) = $o->load_by_cols(
			name => $field,
			from_database => $database,
		);

		if ( $id ) {
			$o->set_items( $items );
			$log->debug("updated $database $type field: $field [$items] ID: $id $msg");
			$updated++;
		} else {
			$log->debug("adding $database $type field: $field [$items] $msg");
			$o->create(
				name => $field,
				from_database => $database,
				items => $items,
			);
			$new++;
		}

		$count++;
	}

	$log->info("synced $count fields (",join(", ", @field_names),") from $database with Webpacus ($new new/$updated updated) at $path");

	return $count;

}

=head2 consume_outputs

Returns array with names of supported output types for this module

=cut

sub consume_outputs {
	return qw/search sorted/;
}

=head1 AUTHOR

Dobrica Pavlinusic, C<< <dpavlin@rot13.org> >>

=head1 COPYRIGHT & LICENSE

Copyright 2007 Dobrica Pavlinusic, All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

1;
