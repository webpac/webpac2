package WebPAC::Output::KinoSearch;

use warnings;
use strict;

use base qw/WebPAC::Common WebPAC::Output Class::Accessor/;
__PACKAGE__->mk_accessors(qw(
	path
	database
	input
	encoding
	clean

	index
));

use KinoSearch::Simple;
use File::Path;
use Encode qw/decode/;
use Data::Dump qw/dump/;
use Storable;

=head1 NAME

WebPAC::Output::KinoSearch - Create KinoSearch full text index

=head1 VERSION

Version 0.05

=cut

our $VERSION = '0.05';

=head1 SYNOPSIS

Create full text index using KinoSearch index from data with
type C<search>.

=head1 FUNCTIONS

=head2 new

Open KinoSearch index

 my $out = new WebPAC::Output::KinoSearch({
 	path => '/path/to/invindex',
	database => 'demo',
	encoding => 'iso-8859-2',
	clean => 1,
 });

Options are:

=over 4

=item path

path to KinoSearch index to use

=item database

name of database from which data comes

=item encoding

character encoding of C<data_structure> if it's differenet than C<ISO-8859-2>
(and it probably is). This encoding will be converted to C<UTF-8> for
index.

=back

=head2 init

  $out->init;

=cut

sub init {
	my $self = shift;

	my $log = $self->_get_logger;

	#$log->debug("self: ", sub { dump($self) });

	foreach my $p (qw/path database/) {
		$log->logdie("need $p") unless ($self->$p);
	}

#	$log->logdie("fields is not ARRAY") unless (ref($self->{fields}) eq 'ARRAY');

	$self->encoding( 'ISO-8859-2' ) unless $self->encoding;

	## FIXME we shouldn't re-create whole KinoSearch index every time!
#	$self->clean( 1 );

	if ( ! -e $self->path ) {
		mkpath $self->path || $log->logdie("can't create ", $self->path,": $!");
		$log->info("created ", $self->path);
	} elsif ( $self->clean ) {
		$log->info("removing existing ", $self->path);
		rmtree $self->path || $log->logdie("can't remove ", $self->path,": $!");
		mkpath $self->path || $log->logdie("can't create ", $self->path,": $!");
	}

	my $path = $self->path . '/' . $self->database;

	$log->info("using index $path with encoding ", $self->encoding);

	my $index = KinoSearch::Simple->new(
		path => $path,
		language => 'en',
	);

	$log->logdie("can't open $path: $!") unless $index;

	$self->index( $index );

}


=head2 add

Adds one entry

  $out->add( 42, $ds );

=cut

sub add {
	my $self = shift;

	my ( $id, $ds ) = @_;

	my $log = $self->_get_logger;
	$log->logdie("need id") unless defined $id;
	$log->logdie("need ds") unless $ds;

	my $hash = $self->ds_to_hash( $ds, 'search' ) || return;

	$hash->{id}       ||= $id;
	$hash->{database} ||= $self->database;
	$hash->{input}    ||= $self->input;

	foreach my $f ( keys %$hash ) {
		if ( ref($hash->{$f}) eq 'ARRAY' ) {
			$hash->{$f} = join(' <*> ', @{ $hash->{$f} });
		}
#		$hash->{$f} = decode( $self->encoding, $hash->{$f} );
		$self->{field_count}->{$f}++;
	}

	$log->debug("add( $id, ", sub { dump($ds) }," ) => ", sub { dump( $hash ) });

	$self->index->add_doc( $hash );

	$self->{count}++;

	return 1;
}

=head2 finish

Close index

 $out->finish;

=cut

sub finish {
	my $self = shift;

	my $log = $self->_get_logger();

	$log->info("indexed ", $self->{count}, " records");

	$log->debug("field usage: ", dump( $self->{field_count} ));

}

=head1 AUTHOR

Dobrica Pavlinusic, C<< <dpavlin@rot13.org> >>

=head1 COPYRIGHT & LICENSE

Copyright 2005-2007 Dobrica Pavlinusic, All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

1; # End of WebPAC::Output::Estraier
