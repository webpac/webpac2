package WebPAC::Normalize::MARC;
use Exporter 'import';
our @EXPORT = qw/
	marc marc_indicators marc_repeatable_subfield
	marc_compose marc_leader marc_fixed
	marc_duplicate marc_remove marc_count
	marc_original_order
	marc_template
	marc_clone
/;

use strict;
use warnings;

use Storable qw/dclone/;
use Data::Dump qw/dump/;
use Carp qw/confess/;

use WebPAC::Normalize;

our $debug = 0;

my ($marc_record, $marc_encoding, $marc_repeatable_subfield, $marc_indicators, $marc_leader);
my ($marc_record_offset, $marc_fetch_offset) = (0, 0);

our $rec;

=head1 NAME

WebPAC::Normalize::MARC - create MARC/ISO2709 records

=cut

=head1 FUNCTIONS

=head2 marc_template

	marc_template(
		from => 225, to => 440,
		subfields_rename => [
			'a' => 'a',
			'x' => 'x',
			'v' => 'v',
			'h' => 'n',
			'i' => 'p',
			'w' => 'v',
		],
		isis_template => [
			'a ; |v. |i',
			'a. |i ; |w',
		],
		marc_template => [
			'a, |x ; |v. |n, |p ; |v',
			'a ; |v. |p ; |v',
		],
	);

Returns number of records produced.

=cut

my $created_with_marc_template;

sub marc_template {
	my $args = {@_};
	warn "## marc_template(",dump($args),")",$/ if $debug;

	foreach ( qw/subfields_rename isis_template marc_template/ ) {
#		warn "ref($_) = ",ref($args->{$_})  if $debug;
		die "$_ not ARRAY" if defined($args->{$_}) && ref($args->{$_}) ne 'ARRAY';
	}

	die "marc_template needs isis_template or marc_template"
		if ! defined $args->{isis_template} && ! defined $args->{marc_template};

	my $rec = WebPAC::Normalize::_get_rec() || die "_get_rec?";
	my $r = $rec->{ $args->{from} } || return;
	die "record field ", $args->{from}, " isn't array ",dump( $rec ) unless (ref($r) eq 'ARRAY');

	my @subfields_rename = @{ $args->{subfields_rename} };
#	warn "### subfields_rename [$#subfields_rename] = ",dump( @subfields_rename )  if $debug;

	confess "need mapping in pairs for subfields_rename"
		if $#subfields_rename % 2 != 1;
	
	my ( $subfields_rename, $from_subfields );
	our $to_subfields = {};
	while ( my ( $from, $to ) = splice(@subfields_rename, 0, 2) ) {
		my ( $f, $t ) = (
			$from_subfields->{ $from }++,
			$to_subfields->{ $to }++
		);
		$subfields_rename->{ $from }->[ $f ] = [ $to => $t ];
	}
	warn "### subfields_rename = ",dump( $subfields_rename ),$/ if $debug;
	warn "### from_subfields = ", dump( $from_subfields ),$/ if $debug;
	warn "### to_subfields = ", dump( $to_subfields ),$/ if $debug;

	our $_template;

	$_template->{isis}->{fields_re} = join('|', keys %$from_subfields );
	$_template->{marc}->{fields_re} = join('|', keys %$to_subfields );

	my @marc_out;

	sub _parse_template {
		my ( $name, $templates ) = @_;

		my $fields_re = $_template->{$name}->{fields_re} || die "can't find $name in ",dump( $_template->{$name}->{fields_re} );

		foreach my $template ( @{ $templates } ) {
			our $count = {};
			our @order = ();
			sub my_count {
				my $sf = shift;
				my $nr = $count->{$sf}++;
				push @order, [ $sf, $nr ];
				return $sf . $nr;
			}
			my $pos_template = $template;
			$pos_template =~ s/($fields_re)/my_count($1)/ge;
			my $count_key = dump( $count );
			warn "### template: |$template| -> |$pos_template| count = $count_key order = ",dump( @order ),$/ if $debug;
			$_template->{$name}->{pos}->{ $count_key } = $pos_template;
			$_template->{$name}->{order}->{ $pos_template } = [ @order ];
		}
		warn "### from ",dump( $templates ), " using $fields_re created ", dump( $_template ),$/ if $debug;
	}

	_parse_template( 'marc', $args->{marc_template} );
	_parse_template( 'isis', $args->{isis_template} );
	warn "### _template = ",dump( $_template ),$/ if $debug;

	my $m;

	our $from_rec = $rec->{ $args->{from} };

	foreach my $r ( @$from_rec ) {

		my $to = $args->{to};
		my ($i1,$i2) = _get_marc_indicators( $to );
		$m = [ $to, $i1, $i2 ];

		$created_with_marc_template->{ $to }++;

		warn "### r = ",dump( $r ),$/ if $debug;

		my ( $from_mapping, $from_count, $to_count );
		our $to_mapping;
		foreach my $from_sf ( keys %{$r} ) {
			# skip everything which isn't one char subfield (e.g. 'subfields')
			next unless $from_sf =~ m/^\w$/;
			my $from_nr = $from_count->{$from_sf}++;
			my $rename_to = $subfields_rename->{ $from_sf } ||
				die "can't find subfield rename for $from_sf/$from_nr in ", dump( $subfields_rename );
			my ( $to_sf, $to_nr ) = @{ $rename_to->[$from_nr] };
			$to_mapping->{ $to_sf }->[ $to_nr ] = [ $from_sf => $from_nr ];

			my $to_nr2 = $to_count->{ $to_sf }++;
			$from_mapping->{ $from_sf }->[ $from_nr ] = [ $to_sf => $to_nr2 ];

			warn "### from $from_sf/$from_nr -> $to_sf/$to_nr\tto $from_sf/$from_nr -> $to_sf/$to_nr2\n" if $debug;
		}

		warn "### from_mapping = ",dump( $from_mapping ), "\n### to_mapping = ",dump( $to_mapping ),$/ if $debug;

		my $count_key = {
			from => dump( $from_count ),
			to   => dump( $to_count),
		};

		warn "### count_key = ",dump( $count_key ),$/ if $debug;

		my $processed_templates = 0;

		# this defines order of traversal
		foreach ( qw/isis:from marc:to/ ) {
			my ($name,$count_name) = split(/:/);

			my $ckey = $count_key->{$count_name} || die "can't find count_key $count_name in ",dump( $count_key );

			my $template = $_template->{$name}->{pos}->{ $ckey } || next;
			$processed_templates++;

			warn "### traverse $name $count_name selected template: |$template|\n" if $debug;

			our $fill_in = {};

			my @templates = split(/\|/, $template );
			@templates = ( $template ) unless @templates;

			warn "### templates = ",dump( @templates ),$/ if $debug;

			foreach my $sf ( @templates ) {
				sub fill_in {
					my ( $name, $r, $pre, $sf, $nr, $post ) = @_;
					warn "#### fill_in( $name, r, '$pre', $sf, $nr, '$post' )\n" if $debug;
					my ( $from_sf, $from_nr );
					if ( $name eq 'marc' ) {
						die "no $sf/$nr in to_mapping: ",dump( $to_mapping ), "\n>>>> from record ",dump( $r ), "\n>>>> full record = ",dump( $from_rec ) unless defined $to_mapping->{$sf}->[$nr];
						( $from_sf, $from_nr ) = @{ $to_mapping->{$sf}->[$nr] };
					} else {
						( $from_sf, $from_nr ) = ( $sf, $nr );
					}
					my $v = $r->{ $from_sf }; # || die "no $from_sf/$from_nr";
					if ( ref( $v ) eq 'ARRAY' ) {
						$v = $pre . $v->[$from_nr] . $post;
					} elsif ( $from_nr == 0 ) {
						$v = $pre . $v . $post;
					} else {
						die "requested subfield $from_sf/$from_nr but it's ",dump( $v );
					}
					warn "#### fill_in( $sf, $nr ) = $from_sf/$from_nr >>>> ",dump( $v ),$/ if $debug;
					$fill_in->{$sf}->[$nr] = $v;
					return $v;
				}
				my $fields_re = $_template->{$name}->{fields_re} || die "can't find $name in ",dump( $_template->{$name}->{fields_re} );
				warn "#### $sf <<<< $fields_re\n" if $debug;
				$sf =~ s/^(.*?)($fields_re)(\d+)(.*?)$/fill_in($name,$r,$1,$2,$3,$4)/ge;
				warn "#### >>>> $sf with fill_in = ",dump( $fill_in ),$/ if $debug;
			}

			warn "## template: |$template|\n## _template->$name = ",dump( $_template->{$name} ),$/ if $debug;

			foreach my $sf ( @{ $_template->{$name}->{order}->{$template} } ) {
				my ( $sf, $nr ) = @$sf;
				my $v = $fill_in->{$sf}->[$nr];
				die "can't find fill_in $sf/$nr" unless defined $v;
				if ( $name eq 'isis') {
					( $sf, $nr ) = @{ $from_mapping->{$sf}->[$nr] };
				}
				warn "++ $sf/$nr |$v|\n" if $debug;
				push @$m, ( $sf, $v );
			}

			warn "#### >>>> created MARC record: ", dump( $m ),$/ if $debug;

			push @marc_out, $m;

			last;
		}
	
		die "I don't have template for fields ",dump( $count_key ), "\n## available templates\n", dump( $_template ) unless $processed_templates;
		warn ">>> $processed_templates templates applied to data\n",$/ if $debug;
	}


	my $recs = 0;

	foreach my $marc ( @marc_out ) {
		warn "+++ ",dump( $marc ),$/ if $debug;
		_marc_push( $marc );
		$recs++;
	}

	warn "### marc_template produced $recs MARC records: ",dump( @marc_out ),$/ if $debug;

	return $recs;
}

=head2 marc_leader

Setup fields within MARC leader or get leader

  marc_leader('05','c');
  my $leader = marc_leader();

=cut

sub marc_leader {
	my ($offset,$value) = @_;

	if ( defined $offset && defined $value ) {
		$marc_leader->[ $marc_record_offset ]->{ $offset } = $value;
	} else {
		
		if (defined($marc_leader)) {
			die "marc_leader not array = ", dump( $marc_leader ) unless (ref($marc_leader) eq 'ARRAY');
			return $marc_leader->[ $marc_record_offset ];
		} else {
			return;
		}
	}
}

=head2 marc_fixed

Create control/indentifier fields with values in fixed positions

  marc_fixed('008', 00, '070402');
  marc_fixed('008', 39, '|');

Positions not specified will be filled with spaces (C<0x20>).

There will be no effort to extend last specified value to full length of
field in standard.

=cut

sub marc_fixed {
	my ($f, $pos, $val) = @_;
	die "need marc(field, position, value)" unless defined($f) && defined($pos);

	confess "need val" unless defined $val;

	my $update = 0;

	map {
		if ($_->[0] eq $f) {
			my $old = $_->[1];
			if (length($old) <= $pos) {
				$_->[1] .= ' ' x ( $pos - length($old) ) . $val;
				warn "## marc_fixed($f,$pos,'$val') append '$old' -> '$_->[1]'\n" if ($debug > 1);
			} elsif ( defined $old ) {
				$_->[1] = substr($old, 0, $pos) . $val . substr($old, $pos + length($val));
				warn "## marc_fixed($f,$pos,'$val') update '$old' -> '$_->[1]'\n" if ($debug > 1);
			}
			$update++;
		}
	} @{ $marc_record->[ $marc_record_offset ] };

	if (! $update) {
		my $v = ' ' x $pos . $val;
		push @{ $marc_record->[ $marc_record_offset ] }, [ $f, $v ];
		warn "## marc_fixed($f,$pos,'val') created '$v'\n" if ($debug > 1);
	}
}

=head2 marc

Save value for MARC field

  marc('900','a', rec('200','a') );
  marc('001', rec('000') );

=cut

sub marc {
	my $f = shift or die "marc needs field";
	die "marc field must be numer" unless ($f =~ /^\d+$/);

	my $sf;
	if ($f >= 10) {
		$sf = shift or die "marc needs subfield";
	}

	foreach (@_) {
		my $v = $_;		# make var read-write for Encode
		#Encode::_utf8_on($v); # FIXME we probably need this
		next unless (defined($v) && $v !~ /^\s*$/);
		my ($i1,$i2) = _get_marc_indicators( $f );
		if (defined $sf) {
			push @{ $marc_record->[ $marc_record_offset ] }, [ $f, $i1, $i2, $sf => $v ];
		} else {
			push @{ $marc_record->[ $marc_record_offset ] }, [ $f, $v ];
		}
	}
}

=head2 marc_repeatable_subfield

Save values for MARC repetable subfield

  marc_repeatable_subfield('910', 'z', rec('909') );

=cut

sub marc_repeatable_subfield {
	my ($f,$sf) = @_;
	die "marc_repeatable_subfield need field and subfield!\n" unless ($f && $sf);
	$marc_repeatable_subfield->{ $f . $sf }++;
	marc(@_);
}

=head2 marc_indicators

Set both indicators for MARC field

  marc_indicators('900', ' ', 1);

Any indicator value other than C<0-9> will be treated as undefined.

=cut

sub marc_indicators {
	my $f = shift || die "marc_indicators need field!\n";
	my ($i1,$i2) = @_;
	die "marc_indicators($f, ...) need i1!\n" unless(defined($i1));
	die "marc_indicators($f, $i1, ...) need i2!\n" unless(defined($i2));

	$i1 = ' ' if ($i1 !~ /^\d$/);
	$i2 = ' ' if ($i2 !~ /^\d$/);
	@{ $marc_indicators->{$f} } = ($i1,$i2);

	if ( $created_with_marc_template->{$f} ) {
		my $m = $marc_record->[ $marc_record_offset ];
		foreach my $field_arr ( @$m ) {
			if ( $field_arr->[0] == $f ) {
				$field_arr->[1] = $i1;
				$field_arr->[2] = $i2;
			}
		}
		$marc_record->[ $marc_record_offset ] = $m;
	}
}

sub _get_marc_indicators {
	my $f = shift || confess "need field!\n";
	return defined($marc_indicators->{$f}) ? @{ $marc_indicators->{$f} } : (' ',' ');
}

=head2 marc_compose

Save values for each MARC subfield explicitly

  marc_compose('900',
  	'a', rec('200','a')
  	'b', rec('201','a')
  	'a', rec('200','b')
  	'c', rec('200','c')
  );

If you specify C<+> for subfield, value will be appended
to previous defined subfield.

=cut

sub marc_compose {
	my $f = shift or die "marc_compose needs field";
	die "marc_compose field must be numer" unless ($f =~ /^\d+$/);

	my ($i1,$i2) = _get_marc_indicators( $f );
	my $m = [ $f, $i1, $i2 ];

	warn "### marc_compose input subfields = ", dump(@_),$/ if ($debug > 2);

	if ($#_ % 2 != 1) {
		die "ERROR: marc_compose",dump($f,@_)," not valid (must be even).\nDo you need to add first() or join() around some argument?\n";
	}

	while (@_) {
		my $sf = shift;
		my $v = shift;

		next unless (defined($v) && $v !~ /^\s*$/);
		warn "## ++ marc_compose($f,$sf,$v) ", dump( $m ),$/ if ($debug > 1);
		if ($sf ne '+') {
			push @$m, ( $sf, $v );
		} else {
			$m->[ $#$m ] .= $v;
		}
	}

	warn "## marc_compose current marc = ", dump( $m ),$/ if ($debug > 1);

	push @{ $marc_record->[ $marc_record_offset ] }, $m if ($#{$m} > 2);
}

=head2 marc_duplicate

Generate copy of current MARC record and continue working on copy

  marc_duplicate();

Copies can be accessed using C<< _get_marc_fields( fetch_next => 1 ) >> or
C<< _get_marc_fields( offset => 42 ) >>.

=cut

sub marc_duplicate {
	 my $m = $marc_record->[ -1 ];
	 die "can't duplicate record which isn't defined" unless ($m);
	 push @{ $marc_record }, dclone( $m );
	 push @{ $marc_leader }, dclone( marc_leader() );
	 warn "## marc_duplicate = ", dump(@$marc_leader, @$marc_record), $/ if ($debug > 1);
	 $marc_record_offset = $#{ $marc_record };
	 warn "## marc_record_offset = $marc_record_offset", $/ if ($debug > 1);

}

=head2 marc_remove

Remove some field or subfield from MARC record.

  marc_remove('200');
  marc_remove('200','a');

This will erase field C<200> or C<200^a> from current MARC record.

  marc_remove('*');

Will remove all fields in current MARC record.

This is useful after calling C<marc_duplicate> or on it's own (but, you
should probably just remove that subfield definition if you are not
using C<marc_duplicate>).

FIXME: support fields < 10.

=cut

sub marc_remove {
	my ($f, $sf) = @_;

	die "marc_remove needs record number" unless defined($f);

	delete $created_with_marc_template->{$f}; # remote magic marker

	my $marc = $marc_record->[ $marc_record_offset ];

	warn "### marc_remove before = ", dump( $marc ), $/ if ($debug > 2);

	if ($f eq '*') {

		delete( $marc_record->[ $marc_record_offset ] );
		warn "## full marc_record = ", dump( @{ $marc_record }), $/ if ($debug > 1);

	} else {

		my $i = 0;
		foreach ( 0 .. $#{ $marc } ) {
			last unless (defined $marc->[$i]);
			warn "#### working on ",dump( @{ $marc->[$i] }), $/ if ($debug > 3);
			if ($marc->[$i]->[0] eq $f) {
				if (! defined $sf) {
					# remove whole field
					splice @$marc, $i, 1;
					warn "#### slice \@\$marc, $i, 1 = ",dump( @{ $marc }), $/ if ($debug > 3);
					$i--;
				} else {
					foreach my $j ( 0 .. (( $#{ $marc->[$i] } - 3 ) / 2) ) {
						my $o = ($j * 2) + 3;
						if ( ! defined $marc->[$i]->[$o] ) {
							warn "marc_remove missing $i $o in ",dump($marc->[$i]);
						} elsif ($marc->[$i]->[$o] eq $sf) {
							# remove subfield
							splice @{$marc->[$i]}, $o, 2;
							warn "#### slice \@{\$marc->[$i]}, $o, 2 = ", dump( @{ $marc }), $/ if ($debug > 3);
							# is record now empty?
							if ($#{ $marc->[$i] } == 2) {
								splice @$marc, $i, 1;
								warn "#### slice \@\$marc, $i, 1 = ", dump( @{ $marc }), $/ if ($debug > 3);
								$i--;
							};
						}
					}
				}
			}
			$i++;
		}

		warn "### marc_remove($f", $sf ? ",$sf" : "", ") after = ", dump( $marc ), $/ if ($debug > 2);

		$marc_record->[ $marc_record_offset ] = $marc;
	}

	warn "## full marc_record = ", dump( @{ $marc_record }), $/ if ($debug > 1);
}

=head2 marc_original_order

Copy all subfields preserving original order to marc field.

  marc_original_order( marc_field_number, original_input_field_number );

Please note that field numbers are consistent with other commands (marc
field number first), but somewhat counter-intuitive (destination and then
source).

You might want to use this command if you are just renaming subfields or
using pre-processing modify_record in C<config.yml> and don't need any
post-processing or want to preserve order of original subfields.


=cut

sub marc_original_order {

	my ($to, $from) = @_;
	die "marc_original_order needs from and to fields\n" unless ($from && $to);

	return unless defined($rec->{$from});

	my $r = $rec->{$from};
	die "record field $from isn't array ",dump( $rec ) unless (ref($r) eq 'ARRAY');

	warn "## marc_original_order($to,$from) source = ", dump( $r ),$/ if ($debug > 1);

	$created_with_marc_template->{$to}++; # don't use magic

	foreach my $d (@$r) {

		if ( ! ref($d) ) {
			# scalar
			if ( $to eq 'leader' ) {
				marc_leader( 0, $d );
			} elsif ( $to < 100 ) {
				marc_fixed( $to, 0, $d );
			} else {
				warn "## marc_original_order($to,$from) skipped: ",dump( $d );
			}
			next;
		}

		if (! defined($d->{subfields}) && ref($d->{subfields}) ne 'ARRAY') {
			warn "# marc_original_order($to,$from): field $from doesn't have subfields specification\n";
			next;
		}
	
		my @sfs = @{ $d->{subfields} };

		die "field $from doesn't have even number of subfields specifications\n" unless($#sfs % 2 == 1);

		warn "#--> d: ",dump($d), "\n#--> sfs: ",dump(@sfs),$/ if ($debug > 2);

		my ($i1,$i2) = _get_marc_indicators( $from );
		$i1 = $d->{i1} if exists $d->{i1};
		$i2 = $d->{i2} if exists $d->{i2};
	
		my $m = [ $to, $i1, $i2 ];

		while (my $sf = shift @sfs) {

			warn "#--> sf: ",dump($sf), $/ if ($debug > 2);
			my $offset = shift @sfs;
			die "corrupted sufields specification for field $from\n" unless defined($offset);

			my $v;
			if (ref($d->{$sf}) eq 'ARRAY') {
				$v = $d->{$sf}->[$offset] if (defined($d->{$sf}->[$offset]));
			} elsif ($offset == 0) {
				$v = $d->{$sf};
			} else {
				die "field $from subfield '$sf' need occurence $offset which doesn't exist in ", dump($d);
			}
			push @$m, ( $sf, $v ) if (defined($v));
		}

		if ($#{$m} > 2) {
			push @{ $marc_record->[ $marc_record_offset ] }, $m;
		}
	}

	warn "## marc_record = ", dump( $marc_record ),$/ if ($debug > 1);
}


=head2 marc_count

Return number of MARC records created using L</marc_duplicate>.

  print "created ", marc_count(), " records";

=cut

sub marc_count {
	return $#{ $marc_record };
}

=head2 marc_clone

Clone marc records from input file, whole or just some fields/indicators

  marc_clone;	# whole record

=cut

sub marc_clone {
	warn "### marc_clone rec: ",dump( $rec ) if $debug > 2;
	foreach my $f ( sort keys %$rec ) {
		warn "## marc_clone $f\n" if $debug;
		marc_original_order( $f, $f );
	}
}

=head1 PRIVATE FUNCTIONS

=head2 _marc_push

 _marc_push( $marc );

=cut

sub _marc_push {
	my $marc = shift || die "no marc?";
	push @{ $marc_record->[ $marc_record_offset ] }, $marc;
}

=head2 _clean

Clean internal structures

=cut

sub _clean {
	($marc_record, $marc_encoding, $marc_repeatable_subfield, $marc_indicators, $marc_leader, $created_with_marc_template) = ();
	($marc_record_offset, $marc_fetch_offset) = (0,0);
}


=head2 _get_marc_fields

Get all fields defined by calls to C<marc>

	$marc->add_fields( WebPAC::Normalize:_get_marc_fields() );

We are using I<magic> which detect repeatable fields only from
sequence of field/subfield data generated by normalization.

This magic is disabled for all records created with C<marc_template>.

Repeatable field is created when there is second occurence of same subfield or
if any of indicators are different.

This is sane for most cases. Something like:

  900a-1 900b-1 900c-1
  900a-2 900b-2
  900a-3

will be created from any combination of:

  900a-1 900a-2 900a-3 900b-1 900b-2 900c-1

and following rules:

  marc('900','a', rec('200','a') );
  marc('900','b', rec('200','b') );
  marc('900','c', rec('200','c') );

which might not be what you have in mind. If you need repeatable subfield,
define it using C<marc_repeatable_subfield> like this:

  marc_repeatable_subfield('900','a');
  marc('900','a', rec('200','a') );
  marc('900','b', rec('200','b') );
  marc('900','c', rec('200','c') );

will create:

  900a-1 900a-2 900a-3 900b-1 900c-1
  900b-2

There is also support for returning next or specific using:

  while (my $mf = WebPAC::Normalize:_get_marc_fields( fetch_next => 1 ) ) {
  	# do something with $mf
  }

will always return fields from next MARC record or

  my $mf = WebPAC::Normalize::_get_marc_fields( offset => 42 );

will return 42th copy record (if it exists).

=cut

my $fetch_pos;

sub _get_marc_fields {

	my $arg = {@_};
	warn "### _get_marc_fields arg: ", dump($arg), $/ if ($debug > 2);
	$fetch_pos = $marc_fetch_offset;
	if ($arg->{offset}) {
		$fetch_pos = $arg->{offset};
	} elsif($arg->{fetch_next}) {
		$marc_fetch_offset++;
	}

	return if (! $marc_record || ref($marc_record) ne 'ARRAY');

	warn "### full marc_record = ", dump( @{ $marc_record }), $/ if ($debug > 2);

	my $marc_rec = $marc_record->[ $fetch_pos ];

	warn "## _get_marc_fields (at offset: $fetch_pos) -- marc_record = ", dump( @$marc_rec ), $/ if ($debug > 1);

	return if (! $marc_rec || ref($marc_rec) ne 'ARRAY' || $#{ $marc_rec } < 0);

	# first, sort all existing fields 
	# XXX might not be needed, but modern perl might randomize elements in hash
#	my @sorted_marc_record = sort {
#		$a->[0] . ( $a->[3] || '' ) cmp $b->[0] . ( $b->[3] || '')
#	} @{ $marc_rec };

	my @sorted_marc_record = @{ $marc_rec };	### FIXME disable sorting
	
	# output marc fields
	my @m;

	# count unique field-subfields (used for offset when walking to next subfield)
	my $u;
	map { $u->{ $_->[0] . ( $_->[3] || '')  }++ } @sorted_marc_record;

	if ($debug) {
		warn "## marc_repeatable_subfield = ", dump( $marc_repeatable_subfield ), $/ if ( $marc_repeatable_subfield );
		warn "## marc_record[$fetch_pos] = ", dump( $marc_rec ), $/;
		warn "## sorted_marc_record = ", dump( \@sorted_marc_record ), $/;
		warn "## subfield count = ", dump( $u ), $/;
	}

	my $len = $#sorted_marc_record;
	my $visited;
	my $i = 0;
	my $field;

	warn "## created_with_marc_template = ",dump( $created_with_marc_template ) if $debug;

	foreach ( 0 .. $len ) {

		# find next element which isn't visited
		while ($visited->{$i}) {
			$i = ($i + 1) % ($len + 1);
		}

		# mark it visited
		$visited->{$i}++;

		my $row = dclone( $sorted_marc_record[$i] );

		if ( $created_with_marc_template->{ $row->[0] } ) {
			push @m, $row;
			warn "## copied marc_template created ", dump( $row ),$/ if $debug;
			next;
		}

		# field and subfield which is key for
		# marc_repeatable_subfield and u
		my $fsf = $row->[0] . ( $row->[3] || '' );

		if ($debug > 1) {

			print "### field so far [", $#$field, "] : ", dump( $field ), " ", $field ? 'T' : 'F', $/;
			print "### this [$i]: ", dump( $row ),$/;
			print "### sf: ", $row->[3], " vs ", $field->[3],
				$marc_repeatable_subfield->{ $row->[0] . $row->[3] } ? ' (repeatable)' : '', $/,
				if ($#$field >= 0);

		}

		# if field exists
		if ( $#$field >= 0 ) {
			if (
				$row->[0] ne $field->[0] ||		# field
				$row->[1] ne $field->[1] ||		# i1
				$row->[2] ne $field->[2]		# i2
			) {
				push @m, $field;
				warn "## saved/1 ", dump( $field ),$/ if ($debug);
				$field = $row;

			} elsif (
				( $row->[3] lt $field->[-2] )		# subfield which is not next (e.g. a after c)
				||
				( $row->[3] eq $field->[-2] &&		# same subfield, but not repeatable
					! $marc_repeatable_subfield->{ $fsf }
				)
			) {
				push @m, $field;
				warn "## saved/2 ", dump( $field ),$/ if ($debug);
				$field = $row;

			} else {
				# append new subfields to existing field
				push @$field, ( $row->[3], $row->[4] );
			}
		} else {
			# insert first field
			$field = $row;
		}

		if (! $marc_repeatable_subfield->{ $fsf }) {
			# make step to next subfield
			$i = ($i + $u->{ $fsf } ) % ($len + 1);
		}
	}

	if ($#$field >= 0) {
		push @m, $field;
		warn "## saved/3 ", dump( $field ),$/ if ($debug);
	}

	return \@m;
}

=head2 _get_marc_leader

Return leader from currently fetched record by L</_get_marc_fields>

  print WebPAC::Normalize::MARC::_get_marc_leader();

=cut

sub _get_marc_leader {
	die "no fetch_pos, did you called _get_marc_fields first?" unless ( defined( $fetch_pos ) );
	return $marc_leader->[ $fetch_pos ];
}

=head2 _created_marc_records

  my $nr_records = _created_marc_records;

=cut

sub _created_marc_records {
	return $#{ $marc_record } + 1 if $marc_record;
}

1;
