package WebPAC::Normalize::ISBN;
use Exporter 'import';
@EXPORT = qw/
	isbn_13
	isbn_10
/;

use warnings;
use strict;

use Data::Dump qw/dump/;
use Carp qw/confess/;

use Business::ISBN;

my $debug = 0;

=head1 NAME

WebPAC::Normalize::ISBN - work with ISBN numbers

=cut

=head1 FUNCTIONS

=head2 isbn_13

  my @isbns = isbn_13( rec('10') );

=cut

sub isbn_13 {
	my @out;

	foreach my $isbn ( @_ ) {

		next unless $isbn =~ m/\d/;

		my $i = Business::ISBN->new( $isbn );
		if ( ! $i ) {
			warn "ERROR: ISBN not valid: $isbn\n";
		} else {
			push @out, $i->as_isbn13->as_string;
		}
	}

	warn "### isbn13",dump( @_ )," => ",dump( @out ) if $debug;

	return @out;
}

=head2 isbn_10

  my @isbns = isbn_10( rec('10') );

=cut

sub isbn_10 {
	my @out;

	foreach my $isbn ( @_ ) {

		next unless $isbn =~ m/\d/;

		my $i = Business::ISBN->new( $isbn );
		if ( ! $i ) {
			warn "ERROR: ISBN not valid: $isbn\n";
		} else {
			push @out, $i->as_isbn10->as_string;
		}
	}

	warn "### isbn10",dump( @_ )," => ",dump( @out ) if $debug;

	return @out;
}

1;
