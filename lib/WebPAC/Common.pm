package WebPAC::Common;
use Exporter 'import';
@EXPORT = qw/
	force_array
	dump
/;

use warnings;
use strict;

use Log::Log4perl qw/get_logger :levels/;
use Time::HiRes qw/time/;
use Data::Dump qw/dump/;
use File::Spec;
use Cwd qw/abs_path/;

use base qw/Class::Accessor/;
__PACKAGE__->mk_accessors( qw/log_debug no_log debug/ );

=head1 NAME

WebPAC::Common - internal methods called from other WebPAC modules

=head1 VERSION

Version 0.05

=cut

our $VERSION = '0.05';

=head1 SYNOPSYS

This module defines common functions, and is used as base for other, more
specific modules.

my $o = WebPAC::Common->new({
	log_debug => 1,
	no_log => 1,
	debug => 1,
});

Options:

=over 20

=item log_debug

Generate additional debugging log on C<STDERR>

=item no_log

Disable all logging (useful for tests)

=item debug

Use debugging logger which dumps output only yo C<STDERR>

=back


=head1 FUNCTIONS

=head2 progress_bar

Draw progress bar on STDERR.

 $webpac->progress_bar($current, $max);

=cut

sub progress_bar {
	my $self = shift;

	my ($curr,$max) = @_;

	my $log = $self->_get_logger();

	$self->{last_pcnt_t} ||= time();

	$log->logconfess("no current value!") if (! $curr);
	$log->logconfess("no maximum value!") if (! $max);

	if ($curr > $max) {
		$max = $curr;
		$log->debug("overflow to $curr");
	}

	$self->{'last_pcnt'} ||= 1;
	$self->{'start_t'} ||= time();

	my $p = int($curr * 100 / $max) || 1;

	# reset on re-run
	if ($p < $self->{'last_pcnt'}) {
		$self->{'last_pcnt'} = $p;
		$self->{'start_t'} = time();
	}

	my $t = time();

	if ($p != $self->{'last_pcnt'} || ( $t - $self->{last_pcnt_t} ) > 2 ) {

		my $rate = ($curr / ($t - $self->{'start_t'} || 1));
		my $eta = ($max-$curr) / ($rate || 1);
		printf STDERR ("%5d [%-38s] %-5d %0.1f/s %s\r",$curr,"=" x ($p/3)."$p%>", $max, $rate, $self->fmt_time($eta));
		$self->{'last_pcnt'} = $p;
		$self->{'last_curr'} = $curr;
		$self->{last_pcnt_t} = $t;
	}
	print STDERR "\n" if ($p == 100);
}

=head2 fmt_time

Format time (in seconds) for display.

 print $webpac->fmt_time(time());

This method is called by L<progress_bar> to display remaining time.

=cut

sub fmt_time {
	my $self = shift;

	my $t = shift || 0;
	my $out = "";

	my ($ss,$mm,$hh) = gmtime($t);
	$out .= "${hh}h" if ($hh);
	$out .= sprintf("%02d:%02d", $mm,$ss);
	$out .= "  " if ($hh == 0);
	return $out;
}

=head2 fill_in

Fill in variable names by values

  print $webpac->fill_in( 'foo = $foo bar = $bar',
  	foo => 42, bar => 11,
  );

=cut

sub fill_in {
	my $self = shift;

	my $format = shift || die "no format?";
	my $d = {@_};

	foreach my $n ( keys %$d ) {
		$format =~ s/\$\Q$n\E/$d->{$n}/gs;
	}

	die "unknown variables in '$format' input data = ", dump( $d ) if ( $format =~ m/\$\w+/ );

	return $format;
}

#
#
#

=head2 var_path

  my $path = $self->var_path('data_dir', 'data_file', ... );

=cut

my $abs_path;

sub var_path {
	my $self = shift;

	if ( ! $abs_path ) {
#		$abs_path = abs_path( $0 );
#		$abs_path =~ s!/WebPAC/Common\.pm!!;
		$abs_path = '/data/webpac2';
	}

	return File::Spec->catfile($abs_path, 'var', @_);
}

=head1 EXPORTED NETHODS

=head2 force_array

  my @array = force_array( $ref, sub {
  	warn "reference is undefined!";
  });

=cut

sub force_array {
	my ( $what, $error ) = @_;
	my @result;
	if ( ref( $what ) eq 'ARRAY' ) {
		@result = @{ $what };
	} elsif ( defined $what ) {
		@result =  ( $what );
	} else {
		$error->() if ref($error) eq 'CODE';
	}
	return @result;
}


=head1 INTERNAL METHODS

Here is a quick list of internal methods, mostly useful to turn debugging
on them (see L<LOGGING> below for explanation).

=cut

=head2 _eval

Internal function to eval code without C<strict 'subs'>.

=cut

sub _eval {
	my $self = shift;

	my $code = shift || return;

	my $log = $self->_get_logger();

	no strict 'subs';
	my $ret = eval $code;
	if ($@) {
		$log->error("problem with eval code [$code]: $@");
	}

	$log->debug("eval: ",$code," [",$ret,"]");

	return $ret || undef;
}

=head2 _init_logger

This function will init C<Log::Log4perl> using provided configuration file.

  $webpac->_init_logger('/path/to/log.conf');

If no path to configuration file is given, dummy empty configuration
will be created. If any mode which inherits from this one is called
with C<debug> flag, it will turn logging to debug level.

This function will also read C<log_conf> value from current object and try
to read that as configuration file if it exists, if it doesn't it will
fallback to default C<conf/log.conf>.

You can disable all logging by adding C<no_log> to constructor of WebPAC
object. Object in C<Test::Exception> class will disable logging
automatically.

=cut

sub _init_logger {
	my $self = shift;
	my $file = shift;
	$file ||= $self->{'log_conf'};
	$file = 'conf/log.conf';
	my $name = (caller(2))[3] || caller;

	my $conf = q( );
	if ($self->no_log) {
		warn "# $name disabled logging\n" if $self->log_debug;
		$Log::Log4perl::Config::CONFIG_INTEGRITY_CHECK = 0;
	} elsif ($self->debug) {
		$conf = << '_log4perl_';

log4perl.rootLogger=INFO, SCREEN

log4perl.logger.WebPAC.=DEBUG

log4perl.appender.SCREEN=Log::Log4perl::Appender::Screen
log4perl.appender.SCREEN.layout=PatternLayout
log4perl.appender.SCREEN.layout.ConversionPattern=%d %p> %F{1}:%L %M - %m%n

_log4perl_
		warn "# $name is using debug logger\n" if $self->log_debug;
	} elsif ($name =~ m/Test::Exception/o) {
		warn "# disabled logging for Text::Exception\n" if $self->log_debug;
	} elsif (-e $file) {
		warn "# $name is using $file logger\n" if $self->log_debug;
		Log::Log4perl->init($file);
		return 1;
	} else {
		warn "# $name is using null logger\n" if $self->log_debug;
	}
	Log::Log4perl->init( \$conf );

	return 1;
}


=head2 _get_logger

Get C<Log::Log4perl> object with a twist: domains are defined for each
method

 my $log = $webpac->_get_logger();

=cut

my $_logger_seen;

sub _get_logger {
	my $self = shift;

	my $name = (caller(1))[3] || caller;

	# make name full
	my $f = '';
	if ( $self->log_debug ) {
		foreach ( 0 .. 5 ) {
			my $s = (caller($_))[3];
			$f .= "#### $_ >> $s\n" if ($s);
		}
	}

	$self->{'_logger_'} ||= $self->_init_logger;

	my $log = get_logger( $name );
	warn "# get_logger( $name ) level ", $log->level, "\n$f" if ($self->log_debug && !defined($_logger_seen->{$name}));
	$_logger_seen->{$name}++;
	return $log;
}


=head2 _log

Quick cludge to make logging object available to scripts which
use webpac line this:

  my $log = _new WebPAC::Common()->_get_logger();

=cut

sub _new {
	my $class = shift;
	my $self = {@_};
	bless($self, $class);

	$self ? return $self : return undef;
}

=head1 LOGGING

Logging in WebPAC is performed by L<Log::Log4perl> with config file
C<log.conf>.

Methods defined above have different levels of logging, so
it's descriptions will be useful to turn (mostry B<debug> logging) on
or off to see why WabPAC isn't perforing as you expect it (it might even
be a bug!).

B<This is different from normal Log4perl behaviour>. To repeat, you can
also use method names, and not only classes (which are just few)
to filter logging.

=cut

1;
