package WebPAC::Path;

use strict;
use warnings;

use Exporter 'import';
our @EXPORT = qw/
	mk_base_path
/;

use File::Path;

sub mk_base_path {
	my ($path) = @_;

	my $base_path = $path;
	$base_path =~ s{/[^/]+$}{};

	if ( ! -e $base_path ) {
		mkpath $base_path;
		warn "# created $base_path\n";
	}

	return -e $base_path;
}

1;
