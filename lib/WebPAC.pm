package WebPAC;

our $VERSION = '2.35';

use warnings;
use strict;

=head1 NAME

WebPAC - core module

=cut

=head1 SYNOPSIS

This is quick description of what WebPAC is. This is another iteration of
WebPAC design (first was system with XML files and CGI, second one was semi-private
creatation of CD ROM with L<jsFind> module and third was older version 2
with supprot for lagacy XML and YAML).

Current version supports different input formats and normalisation using set rules.

=head1 AUTHOR

Dobrica Pavlinusic, C<< <dpavlin@rot13.org> >>

=head1 SEE ALSO

To undestand concpets behind WebPAC examine L<WebPAC::Manual>, and then
respective documentation for each component.

=head1 COPYRIGHT & LICENSE

Copyright 2005-2006 Dobrica Pavlinusic, All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

1; # End of WebPAC
