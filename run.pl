#!/usr/bin/perl -w

use strict;

use Cwd qw/abs_path/;
use File::Temp qw/tempdir/;
use lib './lib';

use WebPAC::Common 0.02;
use WebPAC::Parser 0.08;
use WebPAC::Input 0.16;
use WebPAC::Store 0.15;
use WebPAC::Normalize 0.22;
#use WebPAC::Output::TT;
use WebPAC::Validate 0.11;
use WebPAC::Output::MARC;
use WebPAC::Config;
use Getopt::Long;
use File::Path;
use Time::HiRes qw/time/;
use File::Slurp;
use Data::Dump qw/dump/;
use Storable qw/dclone/;
use Pod::Usage qw/pod2usage/;
use LWP::Simple qw//;

use POSIX ":sys_wait_h"; # imports WNOHANG

=head1 NAME

run.pl - start WebPAC indexing

B<this command will probably go away. Don't get used to it!>

=head1 OPTIONS

=over 4

=item --offset 42

start loading (all) databases at offset 42

=item --limit 100

limit loading to 100 records

=item --clean

remove database and Hyper Estraier index before indexing

=item --only=database_name/input_filter

reindex just single database (legacy name is --one)

C</input_filter> is optional part which can be C<name>
or C<type> from input

=item --config conf/config.yml

path to YAML configuration file

=item --stats

disable indexing, modify_* in configuration and dump statistics about field
and subfield usage for each input

=item --validate path/to/validation_file

turn on extra validation of imput records, see L<WebPAC::Validation>

You can use special variables C<$database> and $C<$input> in this parametar
like C<--validate 'conf/validate/$database-$input'> to construct filename

=item --validate-delimiters path/to/validate_delimiters_file

this option is used with C<--validate> to turn on extra validation of
delimiters. If file is non existant, it will be created on first run.

=item --marc-generate

Generate MARC file. This will automatically be on if file contains C<marc*> directives.
You can use this option as C<--no-marc-generate> to disable MARC generation.

=item --marc-lint

By default turned on if normalisation file has C<marc*> directives. You can disable lint
messages with C<--no-marc-lint>.

=item --marc-xml

Creeate MARCXML file (this can be quite large)

=item --marc-dump

Force dump or input and marc record for debugging.

=item --parallel 4

Run databases in parallel (aproximatly same as number of processors in
machine if you want to use full load)

=item --only-links

Create just links

=item --merge

Create merged index of databases which have links

=item --mirror http://www.example.com

Tries to download input path files from mirror URI

=back

=cut

my $offset;
my $limit;

my $clean = 0;
my $config_path;
my $debug = 0;
my $only_filter;
my $stats = 0;
my $validate_path;
my $validate_delimiters_path;
my $marc_generate = 1;
my $marc_lint = 0;
my $marc_dump = 0;
my $marc_xml = 0;
my $parallel = 0;
my $only_links = 0;
my $merge = 0;
my $mirror;
my $help;

my $log = _new WebPAC::Common()->_get_logger();

GetOptions(
	"limit=i" => \$limit,
	"offset=i" => \$offset,
	"clean" => \$clean,
	"one=s" => \$only_filter,
	"only=s" => \$only_filter,
	"config=s" => \$config_path,
	"debug+" => \$debug,
	"stats" => \$stats,
	"validate=s" => \$validate_path,
	"validate-delimiters=s" => \$validate_delimiters_path,
	"marc-generate!" => \$marc_generate,
	"marc-lint!" => \$marc_lint,
	"marc-dump!" => \$marc_dump,
	"marcxml!" => \$marc_xml,
	"parallel=i" => \$parallel,
	"only-links!" => \$only_links,
	"merge" => \$merge,
	"mirror=s" => \$mirror,
	"help" => \$help,
);

$marc_generate = 0 if ( $validate_delimiters_path );

pod2usage(-verbose => 2) if ($help);

my $config = new WebPAC::Config( path => $config_path );

WebPAC::Normalize::_debug( $debug - 1 ) if $debug > 1;

#print "config = ",dump($config) if ($debug);

die "no databases in config file!\n" unless ($config->databases);

$log->info( "-" x 79 );

my $log_file = 'log';

if (-e $log_file ) {	# && -s $log_file > 5 * 1024 * 1024) {
	$log->info("moved old log with ", -s $log_file, " bytes to '${log_file}.old'");
	rename $log_file, "${log_file}.old" || $log->logwarn("can't rename $log_file to ${log_file}.old: $!");
}

my $estcmd_fh;
my $estcmd_path = './estcmd-merge.sh';
if ($merge) {
	open($estcmd_fh, '>', $estcmd_path) || $log->logdie("can't open $estcmd_path: $!");
	print $estcmd_fh 'cd /data/estraier/_node/ || exit 1',$/;
	print $estcmd_fh 'sudo /etc/init.d/hyperestraier stop',$/;
	$log->info("created merge batch file $estcmd_path");
}

my $validate;
$validate = new WebPAC::Validate(
	delimiters => $config->webpac('delimiters'),
) if ($validate_path || $validate_delimiters_path);

my $use_indexer = $config->use_indexer;
$stats ||= $validate;
if ($stats) {
	$log->debug("disabled indexing for stats collection");
	$use_indexer = undef;
} elsif ( $use_indexer ) {
	$log->info("using $use_indexer indexing engine...");
}

# parse normalize files and create source files for lookup and normalization

my ($only_database,$only_input) = split(m#/#, $only_filter) if $only_filter;

my $parser = new WebPAC::Parser(
	config => $config,
	only_database => $only_database,
	only_input => $only_input,
);

my $total_rows = 0;
my $start_t = time();

my @links;

if ($parallel) {
	eval 'use Proc::Queue size => 1;';
	die $@ if $@;
	$log->info("Using $parallel processes for speedup");
	Proc::Queue::size($parallel);
}

sub create_ds_config {
	my ($db_config, $database, $input, $mfn) = @_;
	my $c = dclone( $db_config );
	$c->{_} = $database || $log->logconfess("need database");
	$c->{_mfn} = $mfn || $log->logconfess("need mfn");
	$c->{input} = $input || $log->logconfess("need input");
	return $c;
}

foreach my $database ( sort keys %{ $config->databases } ) {
	my $db_config = $config->databases->{$database};

	next if ($only_database && $database !~ m/$only_database/i);

	if ($parallel) {
		my $f=fork;
		if(defined ($f) and $f==0) {
			$log->info("Created processes $$ for speedup");
		} else {
			next;
		}
	}

	my $indexer;
	if ($use_indexer && $parser->have_rules( 'search', $database )) {

		my $cfg_name = $use_indexer;
		$cfg_name =~ s/\-.*$//;

		my $indexer_config = $config->get( $cfg_name ) || $log->logdie("can't find '$cfg_name' part in confguration");
		$indexer_config->{database} = $database;
		$indexer_config->{clean} = $clean;
		$indexer_config->{label} = $db_config->{name};

		# force clean if database has links
		$indexer_config->{clean} = 1 if ($db_config->{links});

		if ($use_indexer eq 'hyperestraier') {

			# open Hyper Estraier database
			require WebPAC::Output::Estraier;
			$indexer = new WebPAC::Output::Estraier( %{ $indexer_config } );
		
		} elsif ($use_indexer eq 'hyperestraier-native') {

			# open Hyper Estraier database
			require WebPAC::Output::EstraierNative;
			$indexer = new WebPAC::Output::EstraierNative( %{ $indexer_config } );

		} elsif ($use_indexer eq 'kinosearch') {

			die "no longer supported";

		} else {
			$log->logdie("unknown use_indexer: $use_indexer");
		}

		$log->logdie("can't continue without valid indexer") unless ($indexer);
	}


	#
	# store Hyper Estraier links to other databases
	#
	if (ref($db_config->{links}) eq 'ARRAY' && $use_indexer) {
		foreach my $link (@{ $db_config->{links} }) {
			if ($use_indexer eq 'hyperestraier') {
				if ($merge) {
					print $estcmd_fh 'sudo -u www-data estcmd merge ' . $database . ' ' . $link->{to},$/;
				} else {
					$log->info("saving link $database -> $link->{to} [$link->{credit}]");
					push @links, sub {
						$log->info("adding link $database -> $link->{to} [$link->{credit}]");
						$indexer->add_link(
							from => $database,
							to => $link->{to},
							credit => $link->{credit},
						);
					};
				}
			} else {
				$log->warn("NOT IMPLEMENTED WITH $use_indexer: adding link $database -> $link->{to} [$link->{credit}]");
			}
		}
	}
	next if ($only_links);


	#
	# now WebPAC::Store
	#
	my $store = new WebPAC::Store({
		debug => $debug,
	});


	#
	# prepare output
	#
	my @outputs = force_array( $db_config->{output}, sub {
		$log->error("Database $database doesn't have any outputs defined. Do you want to remove it from configuration?" );
	} );

	my @output_modules;

	foreach my $output ( @outputs ) {

#warn '## output = ',dump( $output );

		my $module = $output->{module} || $log->logdie("need module in output section of $database");
		$module = 'WebPAC::Output::' . $module unless $module =~ m/::/;
	
		$log->debug("loading output module $module");
		eval "require $module";

		# add database to arugemnts for output filter
		$output->{database} = $database;
		$output->{clean} = $clean;

		$log->debug("calling $module->new(",dump( $output ),")");
		my $out = new $module->new( $output );
		if ( $out->init ) {
			push @output_modules, $out;
		} else {
			$log->warn("SKIPPED $module");
		}
	}


	#
	# now, iterate through input formats
	#


	my @inputs = force_array( $db_config->{input}, sub {
		$log->info("database $database doesn't have inputs defined");
	} );

	if ( -e 'out/debug' ) {	# FIXME flag?
		my $out;
		foreach my $i ( @inputs ) {
			warn dump( $i );
			next unless defined $i->{normalize};
			warn dump( $i->{normalize} );
			foreach my $normalize ( @{ $i->{normalize} } ) {
				my $path = $normalize->{path};
				$out .= qq/\n##\n## $path\n##\n\n/;
				$out .= read_file( $path );
			}
		}
		my $all = "out/debug/all-normalize.pl";
		write_file( $all, $out );
		warn "### all normalize for this input saved to: $all";
	};

	foreach my $input (@inputs) {

		my $input_name = $input->{name} || $log->logdie("input without a name isn't valid: ",dump($input));

		if ( $input->{skip} ) {
			$log->info("skip $input_name");
			next;
		}

		next if defined $only_input && $input_name !~ m#$only_input#i;

		my $type = lc($input->{type});

		# FIXME check if input module exists
		my $input_module = $input->{module};

		if ( ! $input_module ) {
			if ( grep(/$type/, $config->webpac('inputs')) ) {
				$input_module = $config->webpac('inputs')->{$type};
			} else {
				$log->logdie("I know only how to handle input types ", join(",", $config->webpac('inputs') ), " not '$type'!" );
			}
		}

		my @lookups = $parser->have_lookup_create($database, $input);

		$log->info("working on $database/$input_name with $input_module on $input->{path}",
			@lookups ? " creating lookups: ".join(", ", @lookups) : ""
		);

		if ($stats) {
			# disable modification of records if --stats is in use
			delete($input->{modify_records});
			delete($input->{modify_file});
		}

		if ( $mirror ) {
			my $path = $input->{path} || die "no input path in ",dump( $input );

			my $base = $path;
			$base =~ s{/[^/]+$}{};
			mkpath $base unless -e $base;

			my $rc = LWP::Simple::mirror( "$mirror/$path", $path );
			if (LWP::Simple::is_error( $rc )) {
				die "can't mirror $mirror/$path -> $path [$rc]";
			} else {
				$log->info( "mirror ", $path, " [$rc] ", -s $path, " bytes" );
			}
			
		}

		my $input_db = new WebPAC::Input(
			module => $input_module,
			limit => $limit || $input->{limit},
			offset => $offset || $input->{offset},
			recode => $input->{recode},
			stats => $stats,
			modify_records => $input->{modify_records},
			modify_file => $input->{modify_file},
			input_config => $input,
		);
		$log->logdie("can't create input using $input_module") unless ($input);

		if (defined( $input->{lookup} )) {
			$log->warn("$database/$input_name has depriciated lookup definition, removing it...");
			delete( $input->{lookup} );
		}

		my $lookup_coderef;

		if (@lookups) {

			my $rules = $parser->lookup_create_rules($database, $input) || $log->logdie("no rules found for $database/$input");

			$lookup_coderef = sub {
				my $rec = shift || die "need rec!";
				my $mfn = $rec->{'000'}->[0] || die "need mfn in 000";

				WebPAC::Normalize::data_structure(
					row => $rec,
					rules => $rules,
					config => create_ds_config( $db_config, $database, $input, $mfn ),
				);

				#warn "current lookup: ", dump(WebPAC::Normalize::_get_lookup());
			};

			WebPAC::Normalize::_set_lookup( undef );

			$log->debug("created lookup_coderef using:\n$rules");

		};

		my $lookup_jar;

		my $maxmfn = $input_db->open(
			path => $input->{path},
			input_encoding => $input->{encoding},	# database encoding
			lookup_coderef => $lookup_coderef,
			lookup => $lookup_jar,
			%{ $input },
			load_row => sub {
				my $a = shift;
				return $store->load_row(
					database => $database,
					input => $input_name,
					id => $a->{id},
				);
			},
			save_row => sub {
				my $a = shift;
				return $store->save_row(
					database => $database,
					input => $input_name,
					id => $a->{id},
					row => $a->{row},
				);
			},

		);

		my $lookup_data = WebPAC::Normalize::_get_lookup();

		if (defined( $lookup_data->{$database}->{$input_name} )) {
			$log->debug("created following lookups: ", sub { dump( $lookup_data ) } );

			foreach my $key (keys %{ $lookup_data->{$database}->{$input_name} }) {
				$store->save_lookup(
					database => $database,
					input => $input_name,
					key => $key,
					data => $lookup_data->{$database}->{$input_name}->{$key},
				);
			}
		}

		my $report_fh;
		if ($stats || $validate) {
			my $out_report = 'out/report'; # FIXME move to config
			mkpath $out_report unless -e $out_report;
			my $path = "$out_report/${database}-${input_name}.txt";
			open($report_fh, '>', $path) || $log->logdie("can't open $path: $!");

			print $report_fh "Report for database '$database' input '$input_name' records ",
				$offset || 1, "-", $limit || $input->{limit} || $maxmfn, "\n\n";
			$log->info("Generating report file $path");

			if ( $validate ) {
				$validate->read_validate_file( $validate->fill_in( $validate_path, database => $database, input => $input_name ) ) if ( $validate_path );
				$validate->read_validate_delimiters_file( $validate->fill_in( $validate_delimiters_path, database => $database, input => $input_name ) ) if ( $validate_delimiters_path );
			}
		}

		my $marc;
		if ($marc_generate && $parser->have_rules( 'marc', $database, $input_name )) {
			
			my $out_marc = 'out/marc'; # FIXME move to config
			mkpath $out_marc unless -e $out_marc;

			$marc = new WebPAC::Output::MARC(
				path => "$out_marc/${database}-${input_name}",
				lint => $marc_lint,
				dump => $marc_dump,
				marcxml => $marc_xml,
			);
		}

		my $rules = $parser->normalize_rules($database,$input_name);
		if ( ! $rules ) {
			$log->logwarn("no normalize rules for $database/$input_name", $input_db->input_module->can('normalize') ? " using normalize from input module" : '');
			next;
		}

		$log->debug("parsed normalize rules:\n$rules");

		# reset position in database
		$input_db->seek(1);

		# generate name of config key for indexer (strip everything after -)
		my $indexer_config = $use_indexer;
		$indexer_config =~ s/^(\w+)-?.*$/$1/g if ($indexer_config);

		my $lookup_hash;
		my $depends = $parser->depends($database,$input_name);
	
		if ($depends) {
			$log->debug("$database/$input_name depends on: ", dump($depends)) if ($depends);
			$log->logdie("parser->depends didn't return HASH") unless (ref($depends) eq 'HASH');

			foreach my $db (keys %$depends) {
				foreach my $i (keys %{$depends->{$db}}) {
					foreach my $k (keys %{$depends->{$db}->{$i}}) {
						my $t = time();
						$log->debug("loading lookup $db/$i");
						$lookup_hash->{$db}->{$i}->{$k} = $store->load_lookup(
							database => $db,
							input => $i,
							key => $k,
						);
						$log->debug(sprintf("lookup $db/$i took %.2fs", time() - $t));
					}
				}
			}

			$log->debug("lookup_hash = ", sub { dump( $lookup_hash ) });
		}


		# setup input name for all output filters
		foreach my $out ( @output_modules ) {
			if ( $out->can('input') ) {
				$out->input( $input_name );
			} else {
				$log->warn("output filter ",ref($out)," doesn't support input name");
			}
		}


		foreach my $pos ( 0 ... $input_db->size ) {

			my $row = $input_db->fetch || next;

			$total_rows++;

			my $mfn = $row->{'000'}->[0];

			if (! $mfn || $mfn !~ m{^\d+$}) {
				$log->warn("record $pos doesn't have valid MFN but '$mfn', using $pos");
				$mfn = $pos;
				push @{ $row->{'000'} }, $pos;
			}

			foreach my $out ( @output_modules ) {
				$out->add_row( $mfn, $row ) if $out->can('add_row');
			}

			if ($validate) {
				if ( my $errors = $validate->validate_rec( $row, $input_db->dump_ascii ) ) {
					$log->error( "MFN $mfn validation error:\n",
						$validate->report_error( $errors )
					);
				}
				next;	# validation doesn't create any output
			}

			my $ds;

			if ($rules) {

				$ds = WebPAC::Normalize::data_structure(
					row => $row,
					rules => $rules,
					lookup => $lookup_hash,
					config => create_ds_config( $db_config, $database, $input, $mfn ),
					marc_encoding => 'utf-8',
					load_row_coderef => sub {
						my ($database,$input,$mfn) = @_;
#warn "### load_row($database,$input,$mfn) from data_structure\n";
						return $store->load_row(
							database => $database,
							input => $input,
							id => $mfn,
						);
					},
				);

			} elsif ( $input_db->input_module->can('normalize') ) {
				$ds = $input_db->input_module->normalize( $mfn );
			}

			if ( $ds ) {
				$log->debug("ds = ", sub { dump($ds) });

				$store->save_ds(
					database => $database,
					input => $input_name,
					id => $mfn,
					ds => $ds,
				) if !$stats;

				$indexer->add(
					id => "${input_name}/${mfn}",
					ds => $ds,
					type => $config->get($indexer_config)->{type},
				) if $indexer;

				foreach my $out ( @output_modules ) {
					$out->add( $mfn, $ds ) if $out->can('add');
				}

			} else {
				$log->warn("record $pos didn't produce any output after normalization rules!") unless $marc;
			}
			if ($marc) {
				my $i = 0;

				while (my $fields = WebPAC::Normalize::MARC::_get_marc_fields( fetch_next => 1 ) ) {
					$marc->add(
						id => $mfn . ( $i ? "/$i" : '' ),
						fields => $fields,
						leader => WebPAC::Normalize::MARC::_get_marc_leader(),
						row => $row,
					);
					$i++;
				}

				$log->info("Created $i instances of MFN $mfn\n") if ($i > 1);
			}

		}

		if ($validate) {
			my $errors = $validate->report;
			if ($errors) {
				$log->info("validation errors:\n$errors\n" );
				print $report_fh "$errors\n" if ($report_fh);
			}

			print $report_fh "\nAll possible subfields/delimiter templates:\n", $validate->delimiters_templates( report => 1, current_input => 1 ), "\n\n";

			# must be last thing that touches $validate for this input
			$validate->reset;
		}

		if ($stats) {
			my $s = $input_db->stats;
			$log->info("statistics of fields usage:\n$s");
			print $report_fh "Statistics of fields usage:\n$s" if ($report_fh);
		}

		# close MARC file
		$marc->finish if ($marc);

		# close report
		close($report_fh) if ($report_fh);
	}

	$indexer->finish if $indexer && $indexer->can('finish');

	foreach my $out ( @output_modules ) {
		$out->finish if $out->can('finish');
	}

	my $dt = time() - $start_t;
	$log->info("$total_rows records ", $indexer ? "indexed " : "",
		sprintf("in %.2f sec [%.2f rec/sec]",
			$dt, ($total_rows / $dt)
		)
	);


	# end forked process
	if ($parallel) {
		$log->info("parallel process $$ finished");
		exit(0);
	}

}

if ($parallel) {
	# wait all children to finish
	sleep(1) while wait != -1;
	$log->info("all parallel processes finished");
}

# save new delimiters if needed
$validate->save_delimiters_templates if ( $validate_delimiters_path );

#
# handle links or merge after indexing
#

if ($merge) {
	print $estcmd_fh 'sudo /etc/init.d/hyperestraier start',$/;
	close($estcmd_fh);
	chmod 0700, $estcmd_path || $log->warn("can't chmod 0700 $estcmd_path: $!");
	system $estcmd_path;
} else {
	foreach my $link (@links) {
		$log->logdie("coderef in link ", Dumper($link), " is ", ref($link), " and not CODE") unless (ref($link) eq 'CODE');
		$link->();
	}
}
