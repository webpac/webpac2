#!/usr/bin/perl -w

use strict;

use Cwd qw/abs_path/;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Simple;
use File::Slurp;
use Data::Dumper;
use Text::Iconv;

use lib '../lib';

use WebPAC::DB;
use WebPAC::Output::TT;

my $abs_path = abs_path($0);
$abs_path =~ s#/[^/]*$#/../#;

my $db_path = $abs_path . '/db/';
my $template_path = "$abs_path/conf/output/tt";
opendir(my $dir, $template_path) || die "can't open template path $template_path: $!";
my @templates = grep { /\.tt$/i } readdir($dir);
my $css_file = 'user.css';

my $iconv_utf8 = new Text::Iconv('ISO-8859-2', 'UTF-8');
my $iconv_loc = new Text::Iconv('UTF-8', 'ISO-8859-2');

my $db = new WebPAC::DB(
	path => $db_path,
	read_only => 1,
	debug => 1,
);

my $out = new WebPAC::Output::TT(
	include_path => $template_path,
	filters => { foo => sub { shift } },
);

my $q = new CGI::Simple;
my $self = $q->url( '-path_info'=>1, '-query'=>0, '-full'=>0 );

my $rec = $q->param('rec') || 1;
my $template_filename = $q->param('template') || $templates[0];

print $q->header( -charset    => 'utf-8' );

##---- some handy subs

sub update_file($$) {
	my ($path, $content) = @_;

	$content = $iconv_loc->convert( $content ) || die "no content?";

	sub _conv_js {
		my $t = shift || return;
		return $iconv_loc->convert(chr(hex($t)));
	}
	$content =~ s/%u([a-fA-F0-9]{4})/_conv_js($1)/gex;
	$content =~ s/^[\n\r]+//s;
	$content =~ s/[\n\r]+$//s;

	write_file($path . '.new', $content) || die "can't save ${path}.new $!";
	rename $path . '.new', $path || die "can't rename to $path: $!";
}

sub get_file_in_html($) {
	my ($path) = @_;

	die "no path?" unless ($path);

	my $content = read_file($path) || die "can't read $path: $!";
	$content = $q->escapeHTML($iconv_utf8->convert($content));

	return $content;
}

sub template_list_html($) {
	my $current = shift || die;

	my $html = qq{ Template\n<ul> };

	foreach my $t (sort @templates) {
		if ($t eq $current) {
			$html .= qq{ <li><b>$t</b></li> };
		} else {
			$html .= qq{ <li><a href="#" onClick="load_template('$t'); return false;">$t</a></li> };
		}
	}

	$html .= qq{ </ul> };
	return $html;
}

##----

if ($q->path_info =~ m#xml#) {

	my $ds = $db->load_ds($rec);

	if ($ds) {
		print qq{<response>
<action type='html' target='div_record' errorCode='' errorMessage='' >
		}, $iconv_utf8->convert( $out->apply(
			template => $template_filename,
			data => $ds,
		) ), qq{

</action>
<action type='javascript' errorCode='' errorMessage='' >
<!--
	var el = iwfGetById('div_record_nr');
	if (el) el.innerHTML = '# <b>$rec</b>';
	//iwfShow('div_record');
	iwfOpacity('div_record', 100);
//-->
</action>
</response>
};
		exit;
	} else {
		print qq{<response>
<action type='html' target='div_record' errorCode='' errorMessage='' >

<b>Record $rec not found!</b>
</action>
<action type='javascript' errorCode='' errorMessage='' >
<!--
	var el = iwfGetById('div_record_nr');
	if (el) el.innerHTML = '<strike>&nbsp;$rec&nbsp;</strike>';
//-->
</action>
</response>
};
		exit;
	}

} elsif ($q->path_info =~ m#template_list#) {

		print qq{<response>
<action type='html' target='div_template_list' errorCode='' errorMessage='' >
<div>} . template_list_html($template_filename) . qq{</div>
</action>
</response>
		};

		exit;

} elsif ($q->path_info =~ m#template#) {

		my $template_path = $out->{'include_path'} . '/' . $template_filename;

		if ($q->param('save_template')) {

			update_file($template_path, $q->param('tt_template'));

			print qq{<response>
<action type='html' target='div_template_status' errorCode='' errorMessage='' >
<tt>$template_filename</tt> saved
</action>
<action type='js'>
<!--
iwfShow('div_template_status', 1);
reload_rec();
iwfHideGentlyDelay('div_template_status', 2, 2000, 1);
-->
</action>
</response>
			};
			exit;

		}

		my $tmpl = get_file_in_html($template_path);

		print qq{<response>
<action type='html' target='div_template' errorCode='' errorMessage='' >

<form name="frmEditor" action="$self" method="post" iwfTarget="div_template_status" >

<textarea name="tt_template" cols="80" rows="10" style="display: block;">
$tmpl
</textarea>

<br/>
<input type="button" name="save_template" value="Save" onclick="javascript:iwfRequest(this);" />
<!--
<input type="checkbox" name="checkin_template" id="checkin_checkbox" label="checkin" /> checkin
-->
&nbsp;&nbsp;<span id="div_template_status" style="color: #808080;">idle</span>

<input type="hidden" name="template" value="$template_filename" />

</form>
</action>
<action type='js'>
<!--
iwfOpacity('div_template', 100);
iwfHideGentlyDelay('div_template_status', 2, 2000, 1);
-->
</action>
</response>
		};

		exit;

} elsif ($q->path_info =~ m#css#) {

		my $css_path = $abs_path . '/web/' . $css_file;


		if ($q->param('save_css')) {
			update_file($css_path, $q->param('user_css'));

			print qq{<response>
<action type='html' target='div_css_status' errorCode='' errorMessage='' >
<tt>$css_file</tt> saved
</action>
<action type='js'>
<!--
iwfShow('div_css_status', 1);
// switch css
css_rnd++;
iwfLog('loading user.css?'+css_rnd);
iwfGetById('user_css_link').href = 'user.css?'+css_rnd;
iwfHideGentlyDelay('div_css_status', 2, 2000, 1);
-->
</action>
</response>
			};
			exit;

		}

		my $user_css = get_file_in_html($css_path);

		print qq{<response>
<action type='html' target='div_css' errorCode='' errorMessage='' >

<form name="frmCSSEditor" action="$self" method="post" iwfTarget="div_css_status" >

<textarea name="user_css" cols="80" rows="10" style="display: block; width: 100%;">
$user_css
</textarea>

<br/>
<input type="button" name="save_css" value="Save" onclick="javascript:iwfRequest(this);" />
&nbsp;&nbsp;<span id="div_css_status" style="color: #808080;">idle</span>
</form>
</action>
<action type='js'>
<!--
iwfLog('loaded CSS template');
-->
</action>
</response>
		};

		exit;

} else {

	my $template_list_html = template_list_html($template_filename);

	print <<"_END_OF_HEAD_";
<html>
<head>
<title>WebPAC simple browse interface</title>

<link id="user_css_link" href="user.css" type="text/css" rel="stylesheet"> 

<script type='text/javascript' src='iwf/iwfcore.js'></script>
<script type='text/javascript' src='iwf/iwfgui.js'></script>
<script type='text/javascript' src='iwf/iwfxml.js'></script>
<script type='text/javascript' src='iwf/iwfajax.js'></script>
<script type='text/javascript' src='iwf/iwconfig.js'></script>
<script type='text/javascript'>

var rec = $rec ;
var url = '$self';
var template_filename = '$template_filename';

var css_rnd = 0;

function update_status(text) {
	var el = iwfGetById('div_record_nr');
	if (el) el.innerHTML = text;
}

function load_rec(nr) {
	if (nr == 1) {
		iwfHide('a_left_arr', 1);
	} else {
		iwfShow('a_left_arr', 1);
	}
	update_status(nr+'...');
	iwfRequest( url+'/xml/?template='+template_filename+'&rec='+nr, 'div_record' );
	iwfOpacity('div_record', 30);
}

function inc_rec() {
	rec++;
	load_rec(rec);
	return false;
}

function dec_rec() {
	if (rec > 1) {
		rec--;
		load_rec(rec);
	}
	return false;
}

function reload_rec() {
	load_rec(rec);
	return false;
}

function edit_template() {
	iwfHideGently('div_css', 30, 1);
	iwfShowGently('div_template', 30, 1);
	return false;
}

function edit_css() {
	iwfHideGently('div_template', 30, 1);
	iwfShowGently('div_css', 30, 1);
	return false;
}

function load_template(name) {
	iwfLog('changing template to '+name);
	iwfOpacity('div_template', 30);
	template_filename = name;
	// load template editor
	iwfRequest( url+'/template/?template='+template_filename, 'div_template' );
	load_rec(rec);

	// refresh template list
	iwfRequest( url+'/template_list/?template='+template_filename, 'div_template_list' );
}

function init_page() {
	iwfLog('div_css = ' + iwfX('div_css') + ':' + iwfY('div_css'));
	iwfLog('div_template = ' + iwfX('div_template') + ':' + iwfY('div_template'));

	iwfX('div_css', iwfX('div_template'));
	iwfY('div_css', iwfY('div_template'));

	iwfLog('div_css = ' + iwfX('div_css') + ':' + iwfY('div_css'));

	// load template editor and record
	load_template(template_filename);

	// load css editor
	iwfRequest( url+'/css/', 'div_css' );
}

</script>

</head>
<body onload="init_page();">

<div id="iwfLog">
</div>

db_path = <tt>$db_path</tt><br/>
css = <tt>$css_file</tt>

<div style="background: #e0e0e0; padding: 0.5em; display: block;">
	<a id="a_left_arr" href="$self?rec=}, $rec - 1, qq{" onClick="return dec_rec();">&#8678;</a>
	<span id="div_record_nr"> none </span>

	<a id="a_right_arr" href="$self?rec=}, $rec + 1, qq{" onClick="return inc_rec();">&#8680;</a>
	<a id="a_reload" href="$self?rec=}, $rec, qq{" onClick="return reload_rec();">&#8634;</a>
	<a href="#" onClick="iwfShowLog(); return false;">&#9636;</a>

</div>


<div id="div_template_list" style="display: inline; border: 2px solid #ffcc88; float: right; z-index: 10;">
$template_list_html
</div>

<div>

<div style="border: 2px solid #ffff80;">

Editor
<a id="a_template" href="#" onClick="return edit_template();">template</a>
<a id="a_css" href="#" onClick="return edit_css();">css</a>


<div id="div_template">
<span style="color: #808080;"> no template loaded yet. </span>
</div>

<div id="div_css" style="position: absolute; display: none;">
<span style="color: #808080;"> no CSS loaded yet. </span>
</div>

</div>

<div id="div_record" style="display: block;">
<span style="color: #808080;"> no record loaded yet. </span>
</div>


</body>
</html>
_END_OF_HEAD_

}
