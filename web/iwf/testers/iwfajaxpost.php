<?
require_once('../php/iwf.php');

$now = gmdate("D, d M Y H:i:s");

header( "Expires: Mon, 20 Dec 1998 01:00:00 GMT" );
header( "Last-Modified: $now GMT" );
header( "Cache-Control: no-cache, must-revalidate" );
header( "Pragma: no-cache" );

$iwf->popup($iwfTarget);
$iwf->startHtml($iwfTarget);
$iwf->pretty(true);

echo "Generated on $now GMT<br/>";

echo "<h1>GET</h1>";

foreach($_GET as $key => $val){
	echo "'$key' = '$val'<br />";
}

echo "<h1>POST</h1>";
foreach($_POST as $key => $val){
	echo "'$key' = '$val'<br />";
}

echo "<h1>RAW POST</h1>";
echo $GLOBALS['HTTP_RAW_POST_DATA'] . "<br />";

$iwf->endHtml(true);


?>
