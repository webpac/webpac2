#!/usr/bin/perl

use warnings;
use strict;

use WWW::Mechanize;
use Data::Dump qw(dump);
use File::Path;
use Text::Unaccent;

# Advanced search syntax:
# http://images.isiknowledge.com/WOK46/help/WOS/h_advanced_examples.html

our $q = 'AD=Croatia';
#$q = 'AU=BRATKO, D';
$q = 'CA=BRATKO, D';
#$q = 'AD=(croat* OR hrvat*)';

my $range_size = 100;
my $overlap    = 3; # between previous and this range

my $results = 0;
my $citations = 0;
my $cited_reference = 1; # html tables
my $citing_articles = 1; # as many files as cited articles

my $cites_by_year = 0;

my $max_cites = 5000; # ISI limit to get cites

$q = unac_string( 'utf-8', join(' ', @ARGV) ) if @ARGV;

warn "QUERY: $q\n";

our $mech = WWW::Mechanize->new(
	autocheck => 0, # it dies in reference download with it!
	cookie_jar => undef,
);

our $step = 0;
our @ranges;

my $dir = '/tmp/isi/';
#rmtree $dir if -e $dir;
mkdir $dir unless -d $dir;

sub save_mech {
	my $path = shift;
	$step++;
	my $base_path = sprintf('%s/%04d', $dir,$step);
	$path ||= $base_path;
	$path .= $mech->{ct} =~ m{html}i ? '.html' : '.txt';
	$mech->save_content( $path );
	warn "# [$step] $path ", -s $path, " ", $mech->ct, "\n";
	open(my $dump, '>', "$base_path.dump.txt");
	$mech->dump_all($dump);
}

warn "# get session";
$mech->get( 'http://www.webofknowledge.com/?DestApp=WOS' );
save_mech;

sub search {
	warn "# advanced serach";
	$mech->follow_link( url_regex => qr/AdvancedSearch/ );
	save_mech;

	warn "# cookie_jar ", dump $mech->cookie_jar;

	my $q_this = $q;

	if ( @ranges ) {
		$q_this .= ' AND (' . join(' OR ', map { "PY=$_" } @{ shift @ranges } ) . ')';
	}

	warn "# submit_form search: $q_this\n";
	$mech->submit_form(
		fields => {
			'value(input1)' => $q_this,
		},
	);
	save_mech;

	warn "# summary";
	$mech->follow_link( url_regex => qr/summary/ );
	save_mech;
}

sub get_results {
	my $desc = shift;
	my $from = 1;

	while ( 1 ) {

		my $to = $from + $range_size;

		warn "# submit_form results $from - $to\n";
		save_mech;

		$mech->submit_form(
			form_name => 'output_form',
			fields => {
				'value(record_select_type)' => 'range',
				markFrom => $from,
				markTo => $to,

				mark_from => $from,
				mark_to => $to,
				mark_id => 'WOS',

				fields_selection => 'ACCESSION_NUM FUNDING SUBJECT_CATEGORY JCR_CATEGORY LANG IDS PAGEC SABBR CITREFC ISSN PUBINFO KEYWORDS CITTIMES ADDRS CONFERENCE_SPONSORS DOCTYPE ABSTRACT CONFERENCE_INFO SOURCE TITLE AUTHORS',
				filters	=> 'ACCESSION_NUM FUNDING SUBJECT_CATEGORY JCR_CATEGORY LANG IDS PAGEC SABBR CITREFC ISSN PUBINFO KEYWORDS CITTIMES ADDRS CONFERENCE_SPONSORS DOCTYPE ABSTRACT CONFERENCE_INFO SOURCE TITLE AUTHORS CITREF',
				fullrec_fields_option => 'CITREF',

				save_options => 'fieldtagged',
				format => 'saveToFile',

			},
			button => 'saveToFile',
		);
		save_mech;


		if ( $mech->content =~ m{invalid API call} ) {
			$mech->back;
			last;
		}

		if ( $mech->content !~ m{Please wait while your request is processed} ) {
			warn "WARNING: expecting processing request";
		}


		my $path = "/tmp/isi.$q.$from-$to";
		$path .= '.' . $desc if $desc;

		warn "save $from - $to into $path\n";
		$mech->submit_form(
			form_name => 'etsForm',
		);
		save_mech $path;

		$from += $range_size - $overlap;

		$mech->back;
		$mech->back;
		save_mech;
	}
}


sub citations {
	warn "# citation report";
	$mech->follow_link( url_regex => qr/search_mode=CitationReport/ );
	save_mech;

	warn "view citing articles";
	$mech->follow_link( url_regex => qr/search_mode=TotalCitingArticles/ );
	save_mech;
}

sub years {
	my $years_url = $mech->find_link( url_regex => qr/ra_name=/ );
	if ( ! $years_url ) {
		warn "W: can't find ra_name link\n";
		return;
	}
	$years_url = $years_url->url_abs;
	warn "## $years_url";
	if ( $years_url !~ s{ra_name=\w+}{ra_name=PublicationYear} ) {
		warn "W: no ra_name in $years_url\n";
		return;
	}
	warn "# refine years (hidden by javascript)";
#	warn "http://apps.isiknowledge.com/RAMore.do?product=WOS&search_mode=TotalCitingArticles&SID=T1o6bChdN9PGP1LN1Nh&qid=3&ra_mode=more&ra_name=PublicationYear&db_id=WOS&viewType=raMore\n$years_url\n";
	$mech->get( $years_url );
	save_mech;

	my $html = $mech->content;
	my $years;
	while ( $html =~ s{<label.+?PublicationYear.+?>(\d{4})\s\(([\d,]+)\)</label>}{} ) {
		my ( $year, $count ) = ( $1, $2 );
		$count =~ s{,}{}g;
		$years->{$year} = $count;
	}
	warn "# years ",dump $years;
	$mech->back;

	my @y = sort keys %$years;

	@ranges = ();

	if ( $cites_by_year ) {
		push @ranges, [ $_ ] foreach @y;
		warn "# cites_by_year ranges ", dump @ranges;
		return;
	}

	my $y = shift @y;
	my $size = $years->{$y};

	my $cites_range;
	$cites_range = [$y] if $y;

	foreach my $y ( @y ) {
		if ( $size + $years->{$y} > $max_cites ) {
			push @ranges, $cites_range;
			warn "# cites_range $size years ",dump( $cites_range ),$/;

			$cites_range = [];
			$size = 0;
		}
		$size += $years->{$y};
		push @$cites_range, $y;
	}

	if ( $cites_range ) {
		push @ranges, $cites_range;
		warn "# cites_range $size years ",dump( $cites_range ), " FINAL\n"
	}

	warn '# ranges ', dump @ranges;
	@ranges = () if $#ranges == 1; # just take all

	return $years;
}


our $page = 1;
sub next_page {
	$page++;
	warn "next_page $page\n";

	$mech->submit_form(
		form_name => 'summary_navigation',
		fields => {
			'page' => $page,
		},
	);

	save_mech;

	$mech->form_name( 'summary_navigation' );
	my $is_next_page = $mech->value('page') == $page;
	warn "no next_page" unless $is_next_page;
	return $is_next_page;
}

if ( $results ) {
	search;
	years;
	get_results;
}

if ( $citations ) {

	citations;
	years unless @ranges;

	do {
		my $part;
		if ( @ranges ) {
			$part .= $ranges[0]->[0] . '.';
			search;
			citations;
		}
		$part .= 'citing';
		get_results $part;
	} while ( @ranges );

}



if ( $q =~ m{CA=(.+)} && $cited_reference ) {

	my $CA = $1;

	warn "# citated reference search";
	$mech->follow_link( url_regex => qr/CitedReferenceSearch/ );
	save_mech;


	$mech->submit_form(
		form_name => 'WOS_CitedReferenceSearch_input_form',
		fields => {
			'value(input1)' => $CA,
		},
	);

	my $page = 1;
	my $records = $1 if $mech->content =~ m/(\d+)\s+records/;
	warn "# found $records records\n";
	my $last_span = 'fake';

	while (1) {
		save_mech "/tmp/isi.$q.citedref.$page";

		last unless next_page();
		$page++;

		if ( $mech->content =~ m/(\d+\s*-\s*(\d+))/ ) {
			warn "span: $1\n";
			last if $2 == $records;
			last if $1 == $last_span;
			$last_span = $1;
		} elsif ( $page > 5 ) {
			warn "ARTIFICALLY LIMITED TO 5 PAGES WITHOUT VALID SPAN!";
			last;
		}

	}

}

if ( $q =~ m{CA=(.+)} && $citing_articles ) {

	search;

	my $orig_q = $q;
	my $nr = 0;

	do {

	foreach my $link ( $mech->find_all_links( url_regex => qr/CitingArticles.do/ ) ) {
		$nr++;
		warn "link $nr\n";
		$mech->get( $link->url );
		save_mech;
		$q = $orig_q . '.citing_article.' . $nr;
		get_results;
		$mech->back;
		$mech->back;

		#last if $nr > 3; # FIXME only for development
	}

	} while next_page;

	$q = $orig_q;
}

warn "OVER\n";
