#!/bin/sh

function fix_webpac_group() {
	find $1/ ! -group webpac -exec chgrp -v webpac {} \;
	find $1/ ! -perm -g=w -group webpac -exec chmod g+w -v {} \;
}

fix_webpac_group t
fix_webpac_group lib
fix_webpac_grouo out
fix_webpac_group conf
fix_webpac_group web
fix_webpac_group var
