#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 14;

use JSON;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Output::JSON' );
}

my $path = "$abs_path/out/test.js";

ok(my $out = new WebPAC::Output::JSON({ path => $path, %LOG }), "new");

ok( $out->init, 'init' );

my $ds = {
	'Source' => {
		'name' => 'Izvor: ',
		'display' => [ 'foo' ]
	},
	'ID' => {
		'display' => 'id',
	},
	'Array' => {
		'display' => [ qw/a1 a2 s3 a4 a5/ ],
	},
};

throws_ok { $out->add( ) } qr/need id/, 'add without params';
throws_ok { $out->add( 42 ) } qr/need ds/, 'add without ds';

ok( $out->add( 42, $ds ), 'add' );

ok( $out->add( 99, { foo => { display => 'foo' } } ), 'add another' );

ok( $out->finish );

ok( -e $out->path, "created $path" );

cmp_ok( $out->path, 'eq', $path, 'path' );

ok( my $items = read_file( $path ), 'read_file' );

ok( $items = jsonToObj( $items ), 'parse JSON' );

diag dump( $items ) if $debug;

is_deeply( $items, {
	items => [
		{ array => ["a1", "a2", "s3", "a4", "a5"], id => "id", source => ["foo"] },
		{ foo => "foo" },
	],
}, 'same' );
