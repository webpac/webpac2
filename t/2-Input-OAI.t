#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 28;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Input' );
}

my $module = 'WebPAC::Input::OAI';
diag "testing with $module";

ok(my $input = new WebPAC::Input(
	module => $module,
	no_progress_bar => 1,
	%LOG
), "new");

my $path = '/tank/oai/hrcak';

ok(my $db = $input->open(
	url  => 'http://hrcak.srce.hr/oai/',
	from => '2010-01-01',
	until => '2011-12-31',
	path => 'var/oai/hrcak',
), "open");
ok(my $size = $input->size, "size");
cmp_ok( $size, '==', 7, 'size ok' );

foreach my $mfn ( 3 + 1 ... 3 + $size ) {
	my $rec = $input->fetch;
	ok($rec, "fetch $mfn");
	cmp_ok($rec->{'000'}->[0], '==', $mfn, 'has mfn');
	cmp_ok($input->pos, '==', $mfn, "pos $mfn");
	diag "rec: ", dump($rec), "\n" if $debug;
}

ok( unlink $path, "unlink $path" );
