#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 5;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Output::TT' );
}

ok(my $tt = new WebPAC::Output::TT(
	include_path => "$abs_path../conf/output/tt",
	%LOG
), "new");

my $ds = {
	'Source' => {
		'name' => 'Izvor: ',
		'tag' => 'Source',
		'display' => [ 'foo' ]
		},
	'ID' => {
		'name' => 'ID',
		'tag' => 'IDths',
		'search' => [ 'bar' ],
		'lookup_key' => [ 'bar' ]
		},
	'filename' => [ 'out/thes/001.html' ],
	'name' => 'filename',
	'tag' => 'filename'
};

throws_ok { $tt->apply( template => 'foo', data => [] ) } qr/error.*foo/, "apply without template";

cmp_ok(my $text = $tt->apply( template => 'text.tt', data => $ds ), '=~', qr/Source.*foo/, "apply");

diag $text if $debug;

