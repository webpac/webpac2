#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 22;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Output::Excel' );
use_ok( 'WebPAC::Input' );
}

my $path = "$abs_path/out/test.xls";

ok(my $out = new WebPAC::Output::Excel({ path => $path, %LOG }), "new");

ok( $out->init, 'init' );

throws_ok { $out->add( ) } qr/need id/, 'add without params';
throws_ok { $out->add( 42 ) } qr/need ds/, 'add without ds';

my @funny_chars = ( qw/� � � � � � � � � �/ );

my @expected;

foreach my $line ( 1 .. 5 ) {
	my $ds;
	my $tmp;
	foreach my $col ( 'A' .. 'Z' ) {
		my $text = $line . $col;
		$text .= shift @funny_chars if @funny_chars;
		$ds->{ $col } = { csv => $text };
		$tmp->{$col} = $text;
	}
	ok( $out->add( $line, $ds ), "add $line" );
	push @expected, $tmp;
}

ok( $out->finish );

ok( -e $out->path, "created $path" );

cmp_ok( $out->path, 'eq', $path, 'path' );

diag dump( @expected ) if $debug;

ok(my $input = WebPAC::Input->new( module => 'WebPAC::Input::Excel', no_progress_bar => 1, encoding => 'utf-16', %LOG ), 'new input' );
ok(my $db = $input->open( path => $path ), "input->open $path");

cmp_ok( $input->size, '==', $#expected, "same size" );

foreach my $mfn ( 1 ... $input->size ) {
	my $ds = shift @expected;
	my $rec = $input->fetch;
	diag dump( $ds, $rec ) if $debug;
	is_deeply( $rec, $ds, "$mfn same?" );
}
