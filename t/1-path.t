#!/usr/bin/perl

use warnings;
use strict;

use Test::More tests => 5;

use lib 'lib';

BEGIN {
	use_ok( 'WebPAC::Path' );
}

my $path = '/tmp/webpac-test-$$/file';

ok( mk_base_path( $path ), 'mk_base_path' );
ok( open(my $fh, '>', $path), "create $path" );
ok( -e $path, "exists $path" );
ok( unlink($path), 'unlink' );
