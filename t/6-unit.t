#!/usr/bin/perl -w

use strict;

use Test::More tests => 31;
use Test::Exception;
use Cwd qw/abs_path/;
use File::Temp qw/tempdir/;
use File::Slurp;
use Data::Dump qw/dump/;
use Time::HiRes qw/time/;
use lib 'lib';

my $debug = shift @ARGV;

#
# FIXME add lookup testing!
#

BEGIN {
use_ok( 'WebPAC::Input' );
use_ok( 'WebPAC::Store' );
use_ok( 'WebPAC::Normalize' );
use_ok( 'WebPAC::Output::TT' );
}

ok(my $abs_path = abs_path($0), "abs_path");
$abs_path =~ s#/[^/]*$#/#;
diag "abs_path: $abs_path" if ($debug);

my $isis_file = "$abs_path../t/winisis/BIBL";
#$isis_file = '/data/hidra/THS/THS';
#$isis_file = '/data/isis_data/ffkk/';

diag "isis_file: $isis_file" if ($debug);

my $normalize_set_pl = "$abs_path/data/normalize.pl";
my $lookup_file = "$abs_path../conf/lookup/isis.pm";

ok(my $isis = new WebPAC::Input(
	module => 'WebPAC::Input::ISIS',
	limit => 100,
	no_progress_bar => 1,
), "new Input::ISIS");

ok(my $maxmfn = $isis->open(
	path => $isis_file,
	input_encoding => 'cp852',		# database encoding
	lookup_coderef => sub {
		my $rec = shift || return;
		ok($rec, 'lookup_coderef has rec');
		ok(defined($rec->{'000'}->[0]), 'have mfn');
	},
), "Input::ISIS->open");

ok(my $path = tempdir( CLEANUP => 1 ), "path");

ok(my $db = new WebPAC::Store({
	database => '.',
}), "new Store");

ok(my $norm_pl = read_file( $normalize_set_pl ), "set definitions: $normalize_set_pl" );

ok(my $out = new WebPAC::Output::TT(
	include_path => "$abs_path../conf/output/tt",
	filters => { foo => sub { shift } },
), "new Output::TT");

my $t_norm = 0;

foreach my $pos ( 0 ... $isis->size ) {

	my $row = $isis->fetch || next;

	diag " row $pos => ",dump($row) if ($debug);

	my $t = time();
	ok( my $ds = WebPAC::Normalize::data_structure(
		row => $row,
		rules => $norm_pl,
	), "Set data_structure");
	$t_norm += time() - $t;

	diag " ds $pos => ",dump($ds) if ($debug);

	ok(my $html = $out->apply(
		template => 'html.tt',
		data => $ds,
	), "apply");

	$html =~ s#\s*[\n\r]+\s*##gs;

	#diag $html;

};

diag sprintf("timings: %.2fs\n", $t_norm);
