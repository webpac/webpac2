#!/usr/bin/perl -w

use strict;

use Test::More tests => 12;
use lib 'lib';
use lib 'lib';

BEGIN {
use_ok( 'WebPAC' );
use_ok( 'WebPAC::Validate' );
use_ok( 'WebPAC::Input' );
use_ok( 'WebPAC::Input::ISIS' );
use_ok( 'WebPAC::Store' );
use_ok( 'WebPAC::Parser' );
use_ok( 'WebPAC::Normalize' );
use_ok( 'WebPAC::Normalize::MARC' );
use_ok( 'WebPAC::Output' );
#use_ok( 'WebPAC::Output::Estraier' );
use_ok( 'WebPAC::Output::TT' );
use_ok( 'WebPAC::Output::MARC' );
use_ok( 'WebPAC::Output::JSON' );
#use_ok( 'WebPAC::Output::KinoSearch' );	# needs optional KinoSearch
#use_ok( 'WebPAC::Output::Webpacus' );		# needs Jifty which is optional
}

diag( "Testing WebPAC $WebPAC::VERSION, Perl 5.008007, /usr/bin/perl" );
