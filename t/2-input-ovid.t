#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 48;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Input' );
}

my $module = 'WebPAC::Input::Ovid';
diag "testing with $module";

ok(my $input = new WebPAC::Input(
	module => $module,
	no_progress_bar => 1,
	%LOG
), "new");

ok(my $db = $input->open(
	path => "$abs_path/data/ovid-cites.txt"
), "open");
ok(my $size = $input->size, "size");
cmp_ok( $size, '==', 14, 'size ok' );

foreach my $mfn ( 1 ... $size ) {
	my $rec = $input->fetch;
	ok($rec, "fetch $mfn");
	cmp_ok($rec->{'000'}->[0], '==', $mfn, 'has mfn');
	cmp_ok($input->pos, '==', $mfn, "pos $mfn");
	diag "rec: ", dump($rec), "\n" if $debug;
}

