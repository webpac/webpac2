#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 102;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Input' );
use_ok( 'Encode' );
use_ok( 'Devel::Peek' );
}

my $module = 'WebPAC::Input::CSV';
diag "testing with $module";

ok(my $input = new WebPAC::Input(
	module => $module,
	no_progress_bar => 1,
	%LOG
), "new");

ok(my $db = $input->open(
	path => "$abs_path/data/records-utf8.csv"
), "open");
ok(my $size = $input->size, "size");
cmp_ok( $size, '==', 11, 'size ok' );

foreach my $mfn ( 1 ... $size ) {
	my $rec = $input->fetch;
	ok($rec, "fetch $mfn");
	cmp_ok($rec->{'000'}->[0], '==', $mfn, 'has mfn');
	cmp_ok($input->pos, '==', $mfn, "pos $mfn");

	ok( my $txt = $rec->{'E'}, 'E' );
	diag Dump( $txt ) if $debug;
	ok( Encode::is_utf8( $txt, 1 ), 'utf8' );
	diag "rec: ", dump($rec), "\n" if $debug;
}

ok(my $db = $input->open(
	path => "$abs_path/data/header_first.csv",
	header_first => 1,
), "open");
ok(my $size = $input->size, "size");
cmp_ok( $size, '==', 10 - 1, 'size header_first' );

foreach my $mfn ( 1 ... $size ) {
	my $rec = $input->fetch;
	ok($rec, "fetch $mfn");
	cmp_ok($rec->{'000'}->[0], '==', $mfn, 'has mfn');
	cmp_ok($input->pos, '==', $mfn, "pos $mfn");

	cmp_ok( $rec->{'A'}, 'eq', $rec->{'publication_title'}, 'A eq publication_title' );
	diag "rec: ", dump($rec), "\n" if $debug;
}

