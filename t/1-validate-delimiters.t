#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 15;

use File::Temp qw/ :POSIX /;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Validate' );
}

my $delimiters_path = tmpnam();

ok(my $v = new WebPAC::Validate(
	path => "$abs_path/data/validate_test",
	delimiters => [ ' : ', ' ; ', ' / ', ' \. ', ' = ' ],
	delimiters_path => $delimiters_path,
	%LOG,
), "new");

diag "rules = ", dump( $v->{rules} ) if ( $debug );

$v->{rules} = {};

sub test_v {
	my $row = shift || die "no row?";

	my $d = dump( $row );

	$row->{'000'} = [ 42 ];

	$v->reset;
	my $e = $v->validate_rec( $row );

	diag ">> validate $d\n",dump($e) if ($debug);
}

test_v({
	'900' => [
		{ 'a' => 'a : aa = aaa : a ; a', 'b' => 'b ; b ; b ; b ; a : / z . z . ' },
		{ 'a' => 'a : a : a', 'c' => [ 'a : b ; c', 'a : b ; c / d' ] },
		{ 'a' => 'a : b / c' },
		{ 'a' => 'a : b / c' },
	],
	'901' => [
		{ 'a' => 'a : b / c', 'b' => 'foo' },
		{ 'a' => 'a : b / c', 'b' => 'foo' },
	],
});

diag dump($v->{_delimiters_templates}) if ( $debug );

is_deeply( $v->{_delimiters_templates}, {
	900 => {
		"^a :  / " => 2,
		"^a :  : ^c :  ; ^c :  ;  / " => 1,
		"^a :  =  :  ; ^b ;  ;  ;  ;  :  .  . " => 1,
	},
	901 => { "^a :  / ^b" => 2 },
}, 'just subfields and delimiters');

ok(! $v->delimiters_templates, 'no accumulated delimiters_template');

ok(my $dt = $v->delimiters_templates( current_input => 1 ), 'delimiters_template');
diag $dt if ( $debug );

ok($dt = $v->delimiters_templates( report => 1, current_input => 1 ), 'delimiters_template report');
diag $dt if ( $debug );

$v->reset;
ok( $v->delimiters_templates, 'have accumulated delimiters_templates');

$v->{_validate_delimiters_templates} = {
	900 => { '^a : ^b' => 1 },
};

test_v({
	'900' => [
		{ 'a' => 'foo : bar', 'b' => 'baz' },
		{ 'a' => 'foo', 'b' => 'baz' },
		{ 'a' => 'foo' },
		{ 'a' => 'foo : bar' },
	],
});

ok (my $e = $v->report, 'report');

diag $e if ( $debug );

ok( $v->save_delimiters_templates, 'save_delimiters_templates' );

ok( -s $delimiters_path, "$delimiters_path " . ( -s $delimiters_path ) . " bytes" );

ok( $v->save_delimiters_templates, 'save_delimiters_templates to existing file' );

ok( -s $delimiters_path . '.new' , "new file created" );

ok(my $v2 = new WebPAC::Validate(
	delimiters => [ ' : ', ' ; ', ' / ', ' \. ', ' = ' ],
	delimiters_path => $delimiters_path,
	debug => $debug,
), "new");

is_deeply( $v->{_accumulated_delimiters_templates}, $v2->{_validate_delimiters_templates}, 'save/load ok');

diag dump( $v, $v2 ) if $debug;
