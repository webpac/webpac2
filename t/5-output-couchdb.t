#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 9;

BEGIN {
	use lib 'lib';
	use_ok( 'WebPAC::Test' );
	use_ok( 'WebPAC::Output::CouchDB' );
	use_ok( 'SWISH::API' );
}

my $path = "$abs_path/kino/";

ok(my $out = new WebPAC::Output::CouchDB({
	url => 'http://193.198.212.57:5984',
	database => 'webpac2',
	%LOG
}), "new");

ok( $out->init, 'init' );

my $ds = {
	'Source' => {
		'name' => 'Izvor: ',
		'search' => [ 'tko zna' ]
	},
	'ID' => {
		'search' => [ 'id' ],
	},
	'Array' => {
		'search' => [ qw/a1 a2 s3 a4 a5/ ],
	},
	'foo' => {
		'search' => [ 'foo' ],
	},
};

ok( $out->add_row( 42, $ds ), 'add 42' );

my @strange = ( qw/�aj�inica odma��ivanje �abokre�ina �uma/ );

ok( $out->add_row( 99, { foo => { search => [ @strange ] } } ), 'add 99' );

ok( $out->add_row( 100, { foo => { search => [ qw/foo bar baz/ ] } } ), 'add 100' );

ok( $out->finish, 'finish' );

