#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 60;

use YAML qw/LoadFile/;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Parser' );
use_ok( 'WebPAC::Config' );
}

my $config_path = "$abs_path/conf/test.yml";

ok(-e $config_path, "$config_path exists");

throws_ok { new WebPAC::Parser( %LOG ) } qr/WebPAC::Config/, "new without config";

ok(
	my $parser = new WebPAC::Parser(
		config => new WebPAC::Config( path => $config_path ),
		base_path => $abs_path,
		%LOG
), "new");

my $inputs = {
	foo => [ qw/foo-input1 foo-input2/ ],
	bar => [ qw/bar-input/ ],
	baz => [ qw/baz-input/ ],
};

foreach my $db (keys %$inputs) {
	ok($parser->valid_database($db), "database $db");
	ok($inputs->{$db}, "has known inputs");
	diag "valid inputs = ", dump($inputs->{$db});
	foreach my $i (@{ $inputs->{$db} }) {
		ok($parser->valid_database_input($db,$i), "input $i");
	}
}
ok(! $parser->valid_database('non-existant'), "no database");
ok(! $parser->valid_database_input('foo','non-existant'), "no database input");

ok(my $l = $parser->{_lookup_create_key}, "_lookup_create_key");
ok($parser->{_lookup_create}, "_lookup_create");
diag "_lookup_create_key = ",dump($l) if ($debug);
foreach my $db (keys %$l) {
	foreach my $i (keys %{$l->{$db}}) {
		ok(defined($parser->lookup_create_rules($db,$i)), "lookup_create_rules($db/$i)");
		my @keys = sort keys %{$l->{$db}->{$i}};
		ok(@keys, 'have keys');
		my @have_keys = sort $parser->have_lookup_create($db,$i);
		ok(@have_keys, 'have_lookup_create');
		ok(eq_array(\@have_keys, \@keys), "found all lookups");
	}
}

ok(my $n = $parser->{_normalize_source}, "_normalize_source");
diag "_normalize_source = ",dump($n) if ($debug);
foreach my $db (keys %$n) {
	foreach my $i (keys %{$n->{$db}}) {
		ok(my $r = $parser->normalize_rules($db,$i), "normalize_rules($db/$i)");
		diag "normalize_rules($db,$i) = $r" if ($debug);
		cmp_ok($n->{$db}->{$i}, 'eq', $r, "same");
	}
}

ok(my $d = $parser->{depends}, "depends");
diag "depends = ",dump($d) if ($debug);

my $expected_depend = {
   foo => {
            "foo-input1" => {
                              bar => { "bar-input" => { "210-a-210-e" => 1, "220-a-220-e" => 1, "230-a-230-e" => 1 } },
                              baz => { "baz-input" => { "200-a" => 1 } },
                              foo => { "foo-input1" => { 11 => 1 }, "foo-input2" => { 11 => 1 } },
                            },
            "foo-input2" => {
                              bar => { "bar-input" => { "900-x" => 1 } },
                              baz => { "baz-input" => { "900-x" => 1 } },
                              foo => { "foo-input1" => { "245-a" => 1 }, "foo-input2" => { "245-a" => 1 } },
                            },
          },
   bar => {
            "bar-input" => {
                  baz => { "baz-input" => { "900-x" => 1 } },
                  foo => { "foo-input1" => { "245-a" => 1 } },
                },
          },
   baz => {
            "baz-input" => {
                  bar => { "bar-input" => { "900-x" => 1 } },
                  foo => { "foo-input2" => { "245-a" => 1 } },
                },
          },
};


is_deeply($d, $expected_depend, "depends correct");

foreach my $db (keys %$d) {
	foreach my $i (keys %{$d->{$db}}) {
		is_deeply($d->{$db}->{$i}, $parser->depends($db,$i), "depend $db/$i");
	}
}

$config_path = "$abs_path/conf/marc.yml";

ok(-e $config_path, "$config_path exists");

ok(
	$parser = new WebPAC::Parser(
		config => new WebPAC::Config( path => $config_path ),
		base_path => $abs_path,
		%LOG,
), "new");

ok(my $marc = $parser->have_rules('marc', 'marc', 'marc-input'), 'have_rules(marc,...)');

diag "marc: ",dump($marc) if ($debug);

is_deeply($marc, {
	marc						=> 1,
	marc_compose				=> 1,
	marc_duplicate				=> 1,
	marc_indicators				=> 1,
	marc_leader					=> 1,
	marc_original_order			=> 1,
	marc_remove					=> 1,
	marc_repeatable_subfield	=> 1,
}, 'catched all marc_*');


$config_path = "$abs_path/conf/sub.yml";

ok(-e $config_path, "$config_path exists");

ok(
	$parser = new WebPAC::Parser(
		config => new WebPAC::Config( path => $config_path ),
		base_path => $abs_path,
		%LOG,
), "new");

ok(my $rules = $parser->normalize_rules('sub','sub-input'), "normalize_rules(sub)");

diag "rules: $rules" if $debug;

our @test;
eval $rules;
diag "test = ",dump( @test ) if $debug;

ok(! $@, "eval: $@" );

is_deeply( [ @test ], [ "foo", "foo", "bar >>2<<", "bar >>42<<", 1,2,3 ], 'source fixed' );

