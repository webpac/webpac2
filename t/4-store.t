#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 38;

use File::Temp qw/tempdir/;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Store' );
}

my $db;

diag "NULL Store";

ok(new WebPAC::Store(), 'new without database');

ok($db = new WebPAC::Store({ database => 'foobar', %LOG }), "new");

throws_ok { $db->load_ds() } qr/id/, 'load_ds without id';
ok(! $db->load_ds( id => 000 ), 'load_ds');

throws_ok { $db->save_ds() } qr/id/, "save_ds without id";
throws_ok { $db->save_ds( id => 000 ) } qr/ds/, 'save_ds without ds';

undef $db;

ok($db = new WebPAC::Store({ database => 'webpac-test', %LOG }), "new");

#
# test *_ds
#

throws_ok { $db->load_ds() } qr/without id/, 'load_ds without arguments';
ok(! $db->load_ds( id => 000 ), 'load_ds');

throws_ok { $db->save_ds() } qr/id/, "save_ds - need id";

my $ds = {
	'Source' => {
		'name' => 'Izvor: ',
		'tag' => 'Source',
		'display' => [ 'foo' ]
	},
	'ID' => {
		'name' => 'ID',
		'tag' => 'IDths',
		'swish' => [ 'bar' ],
		'lookup_key' => [ 'bar' ]
	},
};

throws_ok { $db->save_ds( id => 1 ) } qr/ds/, "save_ds - need ds";

ok($db->save_ds( id => 1, ds => $ds ), "save_ds");

ok(my $ds2 = $db->load_ds( id => 1 ), "load_ds with id");

is_deeply($ds, $ds2, "loaded data");

throws_ok { $ds2 = $db->load_ds( 1 ) } qr/HASH/, "load_ds without hash";

ok($ds2 = $db->load_ds( id => 1 ), "load_ds");

is_deeply($ds, $ds2, "loaded data");

ok(! $db->load_ds( id => 42 ), "load_ds non-existing");

ok($db = new WebPAC::Store({ database => 'webpac-test', %LOG }), "new");

ok(! $db->load_ds( id => 1, input => 'foobar' ), "load_ds with invalid input");

ok(! $db->load_ds( id => 1, database => 'non-existant', ), "load_ds with unknown database");

ok($ds2 = $db->load_ds( id => 1, database => 'webpac-test' ), "load_ds");

#
# test *_lookup
#

my $l = {
	foo => { 42 => 1 },
};

ok(! $db->load_lookup( input => 'non-existant', key => 'foo' ), 'invalid load_lookup');

ok($db->save_lookup( input => 'foo', key => 'bar', data => $l ), "save_lookup");

ok(-e $db->var_path( 'lookup', 'webpac-test', 'foo', 'bar'), "exists");

is_deeply($db->load_lookup( input => 'foo', key => 'bar' ), $l, 'load_lookup');

ok($db->save_lookup( database => 'baz', input => 'foo', key => 'bar', data => $l ), "save_lookup with database");

ok(-e $db->var_path( '/lookup','baz','foo','bar'), "exists");

is_deeply($db->load_lookup( database => 'baz', input => 'foo', key => 'bar' ), $l, 'load_lookup');

#
# test *_row
#

my $row = {
	'000' => [ 42 ],
	'900' => [ qw/a foo b bar c baz/ ],
};

ok(! $db->load_row( input => 'non-existant', id => 1234 ), 'invalid load_row');

ok($db->save_row( input => 'foo', id => 1234, row => $row ), "save_row");

ok(-e $db->var_path( 'row','webpac-test','foo',1234), "exists");

is_deeply($db->load_row( input => 'foo', id => 1234 ), $row, 'load_row');

ok($db->save_row( database => 'baz', input => 'foo', id => 1234, row => $row ), "save_row with database");

ok(-e $db->var_path( 'row','baz','foo',1234), "exists");

is_deeply($db->load_row( database => 'baz', input => 'foo', id => 1234 ), $row, 'load_row');

undef $db;

