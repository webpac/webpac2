#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 300;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Input' );
}

my $module = 'WebPAC::Input::Gutenberg';
diag "testing with $module", $debug ? ' with debug' : '';

ok(my $input = new WebPAC::Input(
	module => $module,
	no_progress_bar => 1,
	%LOG
), "new");

ok(my $db = $input->open(
	path => "$abs_path/data/gutenberg-small.rdf"
), "open");
ok(my $size = $input->size, "size");

diag "size: $size" if ($debug);

ok(defined($input->{ll_db}->{_rows}), 'have ll_db->rows');

foreach my $mfn ( 1 ... $size ) {

	ok(defined($input->{ll_db}->{_rows}->{$mfn}), "have ll_db->_rows->$mfn");

	diag "row: ", dump( $input->{ll_db}->{_rows}->{$mfn} ) if ($debug);

	my $rec = $input->fetch;

	ok($rec, "fetch $mfn");

	cmp_ok($input->pos, '==', $mfn, "pos $mfn");

	diag "rec: ", dump($rec), "\n" if ($debug);
}

