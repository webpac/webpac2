#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 35;

use KinoSearch;
use Encode qw/decode/;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Output::KinoSearch' );
}

my $path = "$abs_path/kino/";

ok(my $out = new WebPAC::Output::KinoSearch({
	path => $path,
	database => 'test',
	clean => 1,
	%LOG
}), "new");

ok( $out->init, 'init' );

my $ds = {
	'Source' => {
		'name' => 'Izvor: ',
		'search' => [ 'tko zna' ]
	},
	'ID' => {
		'search' => 'id',
	},
	'Array' => {
		'search' => [ qw/a1 a2 s3 a4 a5/ ],
	},
	'foo' => {
		'search' => [ 'foo' ],
	},
};

throws_ok { $out->add( ) } qr/need id/, 'add without params';
throws_ok { $out->add( 42 ) } qr/need ds/, 'add without ds';

ok( $out->add( 42, $ds ), 'add 42' );

my @strange = ( qw/�aj�inica odma��ivanje �abokre�ina �uma/ );

ok( $out->add( 99, { foo => { search => [ @strange ] } } ), 'add 99' );

ok( $out->add( 100, { foo => { search => [ qw/foo bar baz/ ] } } ), 'add 100' );

ok( -e $out->path, "created $path" );

ok( my $index = $out->index, 'have index' );

diag $out->path," eq ",$path;
cmp_ok( $out->path, 'eq', $path, 'path' );

sub test_search {
	my ( $query_string, $expected_hits ) = @_;

	my $total_hits = $index->search(
		query      => $query_string,
		offset     => 0,
		num_wanted => 10,
	);

	ok( $total_hits, "search '$query_string'" );

	diag "Total hits: $total_hits\n" if $debug;

	cmp_ok( $total_hits, '==', $expected_hits, 'total_hits' );

	while ( my $hit = $index->fetch_hit_hashref ) {
		ok( $hit, 'hit' );
		ok( $hit->{foo} =~ m/�aj�inica/, 'utf-8 conversion' );
		diag dump($hit) if $debug;
	}

}

test_search( 'foo', 2 );

test_search( $_, 1 ) foreach @strange;

ok( $out->finish, 'finish' );

