#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 75;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Input' );
}

my $module = 'WebPAC::Input::Excel';
diag "testing with $module";

ok(my $input = new WebPAC::Input(
	module => $module,
	no_progress_bar => 1,
	%LOG,
), "new");

our ($db,$size);

sub open_xls {
	my $path = shift || die "no path?";
	$path = "$abs_path/data/$path";
	ok(my $db = $input->open( path => $path ), "open $path");
	ok($size = $input->size, "size $size");
	return $db;
}

open_xls('excel_95.xls');

foreach my $mfn ( 1 ... $size ) {
	my $rec = $input->fetch;
	if ($mfn <= 10 || $mfn == 20) {
		ok($rec, "fetch $mfn");
	} else {
		ok(! $rec, "empty $mfn");
	}

	cmp_ok($input->pos, '==', $mfn, "pos $mfn");

	diag "rec: ", dump($rec), "\n" if $debug;
}

open_xls('stara-signatura.xls');
for ( 1 .. 14 ) {
	ok( $input->seek( $_ ), "seek $_");
	ok( my $rec = $input->fetch, "fetch $_" );
	diag dump( $rec ) if $debug;
}

