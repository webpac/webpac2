#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 124;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Input::ISIS' );
use_ok( 'WebPAC::Input::MARC' );
use_ok( 'WebPAC::Input::Test' );
}

$LOG{no_progress_bar} = 1;

warn "# LOG = ",dump( %LOG );

my $module = 'WebPAC::Input::ISIS';
diag "testing with $module";

throws_ok { my $input = new WebPAC::Input( %LOG ) } qr/module/, "need module";
ok(my $input = new WebPAC::Input( module => $module, stats => 1, \%LOG ), "new $module");
ok(my $input_lm = new WebPAC::Input( module => $module, \%LOG ), "new $module");

throws_ok { $input->open( ) } qr/path/, "need path";

throws_ok { $input->open( path => '/dev/null', ) } qr/can't find database/ , "open";

my $store;

ok($input->open( path => "$abs_path/winisis/BIBL" ), "open winisis");
ok($input_lm->open(
	path => "$abs_path/winisis/BIBL",
	save_row => sub {
		my $a = shift;
		$store->{ $a->{id} } = $a->{row};
	},
	load_row => sub {
		my $a = shift;
		return defined($store->{ $a->{id} }) &&
			$store->{ $a->{id} };
	},
), "open winisis");

cmp_ok( keys %$store, '==', 5, 'have 5 rows');

foreach my $i ( 1 .. 5 ) {
	ok(my $r = $store->{$i}, "row $i");
	ok($r->{'000'}, "have 000");
	isa_ok($r->{'000'}, 'ARRAY', "is ARRAY");
	cmp_ok($r->{'000'}->[0], '==', $i, 'sane value');
}

diag "store = ",dump( $store ) if ($debug);

sub test_after_open($) {
	my $input = shift;

	cmp_ok($input->pos, '==', -1, "mfn");
	ok(my $size = $input->size, "size");
	return $size;
}

test_after_open($input);
my $size = test_after_open($input_lm);

sub test_fetch($$) {
	my ($input, $size) = @_;

	my @db;

	foreach my $mfn ( 1 ... $size ) {
		ok(my $rec = $input->fetch, "fetch $mfn");
		cmp_ok($input->pos, '==', $mfn, "pos $mfn");
		push @db, $rec;
		ok(my $dump = $input->dump_ascii, "dump_ascii $mfn");
		# XXX test count will help us keep this test in-line :-)
		ok($rec->{leader}, "leader $mfn") if $rec->{leader};
		diag $dump if ($debug);
	}

	return @db;
}

my @db1 = test_fetch($input, $size);
my @db2 = test_fetch($input_lm, $size);

is_deeply(\@db1, \@db2, "seek working");

sub test_start_limit($$$$) {
	my ($input, $s,$l,$e) = @_;

	diag "offset $s, limit: $l, expected: $e";

	ok($s = $input->open( path => "$abs_path/winisis/BIBL", offset => $s, limit => $l, debug => $debug ), "open winisis");
	cmp_ok($s, '==', $size, "db size");
	cmp_ok($input->size, '==', $e, "input->size = $e");
}

test_start_limit($input, 1, 0, $size - 1);
test_start_limit($input, $size, 0, 0);
test_start_limit($input, 3, $size, $size - 3);
test_start_limit($input, 1, $size, $size - 1);

ok(my $s = $input->stats, "$module stats");
diag "stats:\n$s" if ($debug);

$module = 'WebPAC::Input::MARC';
diag "testing with $module";

ok($input = new WebPAC::Input( module => $module, stats => 1, %LOG ), "new $module");

ok($input->open( path => "$abs_path/data/marc.iso" ), "open marc.iso");

test_after_open($input);

test_fetch($input, $input->size);

ok(my $s = $input->stats, "$module stats");

diag "stats:\n$s" if ($debug);
# test modify_record
$module = 'WebPAC::Input::Test';
ok($input = new WebPAC::Input( module => $module, %LOG ), "new $module");

$WebPAC::Input::Test::rec = {
 	'200' => [
		{ 'a' => '[200 a]', 'b' => '[200 b]', 'c' => '[200 c]', 'f' => '[200 f] test : test' },
	],
	'900' => [
		{ 'x' => 'foobar', },
	],
};

$WebPAC::Input::Test::size = 42;

ok($input->open( path => "/fake/path", ), "open modify_isis (plain)");

cmp_ok($input->size, '==', 42, 'size');

ok(my $rec_p = $input->fetch, 'fetch');

# modify_records

ok($input->open(
	path => "/another/fake/path",
	modify_records => {
		200 => {
			'*' => { '^c' => '. ' },
			'^f' => { ' : ' => ' / ' },
		}
	},
), "open (with modify_records)");

# seek
throws_ok { $input->seek } qr/without/, 'seek without position';
cmp_ok($input->seek(0), '==', -1, 'seek');

sub test_filter {

	my $f = $WebPAC::Input::Test::filter_coderef;
	ok(ref($f) eq 'CODE', 'filter_coderef');

	my ($field, $from, $to) = @_;
	cmp_ok( $f->( $from, $field, 1 ), 'eq', $to, "filter $field |$from| -> |$to|" );
}

test_filter(200,
	'^afoo^cbar^fbing : bong',
	'^afoo. bar^fbing / bong',
);

# modify_file

my $modify_file = "$abs_path/conf/modify/test.pl";

ok($input->open(
	path => "/and/another/fake/path",
	modify_file => $modify_file,
), "open (with modify_file $modify_file)");

diag "regexps = ", dump($input->modify_file_regexps( $modify_file )) if ($debug);

test_filter(200,
	'^a foo ; bar = baz : zzz',
	'^a foo^kbar^dbaz : zzz',
);

# empty subfield removal

ok($input->open(
	path => "/another/fake/path",
	modify_records => {
		900 => {
			'^a' => { '^e' => ' : ^e' },
		},
		901 => {
			'^a' => { 'foo' => 'baz' },
		},
	},
), "open (with modify_records for empty subfields)");

test_filter(900,
	'^a^ebar',
	'^a^ebar',
);

test_filter(900,
	'^afoo^ebar',
	'^afoo : ^ebar',
);

test_filter(901,
	'^afoo^ebar',
	'^abaz^ebar',
);
