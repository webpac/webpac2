#!/usr/bin/perl -w

use strict;

use Test::More tests => 13;
use Test::Exception;
use Cwd qw/abs_path/;
use File::Temp qw/tempdir/;
use Data::Dump;
use lib 'lib';

BEGIN {
use_ok( 'WebPAC::Output::Estraier' );
use_ok( 'WebPAC::Search::Estraier' );
use_ok( 'Log::Log4perl' );
}

Log::Log4perl::init('./conf/log.conf');
ok(my $log = Log::Log4perl->get_logger('WebPAC.test'), "get_logger");

my $config = {
 	masterurl => 'http://localhost:1978/',
	user => 'admin',
	passwd => 'admin',
	database => 'webpac2test',
	encoding => 'iso-8859-2',
	log => $log,
	debug => 0,
};

ok(my $est = new WebPAC::Output::Estraier( %{ $config } ), "new WebPAC::Output::Estraier");

my $ds = {
	'Source' => {
		'name' => 'Izvor: ',
		'tag' => 'Source',
		'display' => [ 'foo' ],
		'search' => [ 'demo source '],
		},
	'ID' => {
		'name' => 'ID',
		'tag' => 'IDths',
		'search' => [ 'bar' ],
		'lookup_key' => [ 'bar' ],
		},
	'filename' => [ 'out/thes/001.html' ],
	'name' => 'filename',
	'tag' => 'filename'
};

ok($est->add( id => 42, ds => $ds, type => 'search' ), "add");

ok(my $est_s = new WebPAC::Search::Estraier( %{ $config } ), "new WebPAC::Search::Estraier");

my $query = 'demo';
my $max = 1;

ok(my @res = $est_s->search(
	phrase => $query,
	get_attr => [ qw/ID Source/ ],
	max => $max,
), "search $query, max: $max");

cmp_ok(($#res + 1), '==', $max, "$max hits");

ok($est->master( action => 'nodeadd', name => 'webpac2test_link', label => 'test' ), "nodeadd");

ok($est->add_link( from => 'webpac2test', to => 'webpac2test_link', credit => 42 ), "add_link");

ok($est->master( action => 'nodedel', name => 'webpac2test' ), "nodedel");
ok($est->master( action => 'nodedel', name => 'webpac2test_link' ), "nodedel");

