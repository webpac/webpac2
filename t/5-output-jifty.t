#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 8;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Output::Jifty' );
}

ok(my $out = new WebPAC::Output::Jifty({
	path => '/data/Webpacus2/',
	model => 'Webpacus::Model::UFO',
	clean => 1,
	%LOG
}), "new");

ok( $out->init, 'init' );

my $ds = {
	date => {
		display => '2007-11-19',
	},
	time => {
		display => '23:20:19',
	},
	town => {
		display => 'Sometown',
	},
	county => {
		display => 'Somewhere',
	},
	comment => {
		display => [ 'test data', 'and more' ],
	}
};

throws_ok { $out->add( ) } qr/need id/, 'add without params';
throws_ok { $out->add( 42 ) } qr/need ds/, 'add without ds';

ok( $out->add( 42, $ds ), 'add 42' );

ok( $out->finish, 'finish' );

