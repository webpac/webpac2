#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 35;

BEGIN {
	use lib 'lib';
	use_ok( 'WebPAC::Test' );
	use_ok( 'WebPAC::Output::SWISH' );
	use_ok( 'SWISH::API' );
}

my $path = "$abs_path/kino/";

ok(my $out = new WebPAC::Output::SWISH({
	database => 'test',
	%LOG
}), "new");

ok( $out->init, 'init' );

my $ds = {
	'Source' => {
		'name' => 'Izvor: ',
		'search' => [ 'tko zna' ]
	},
	'ID' => {
		'search' => [ 'id' ],
	},
	'Array' => {
		'search' => [ qw/a1 a2 s3 a4 a5/ ],
	},
	'foo' => {
		'search' => [ 'foo' ],
	},
};

ok( $out->add( 42, $ds ), 'add 42' );

my @strange = ( qw/�aj�inica odma��ivanje �abokre�ina �uma/ );

ok( $out->add( 99, { foo => { search => [ @strange ] } } ), 'add 99' );

ok( $out->add( 100, { foo => { search => [ qw/foo bar baz/ ] } } ), 'add 100' );

ok( $out->finish, 'finish' );

sub test_search {
	my ( $query_string, $expected_hits ) = @_;

	my $swish = SWISH::API->new( $out->index_path );
	$swish->abort_last_error if $swish->Error;

	my $results = $swish->query( $query_string );

	my $total_hits = $results->hits;

	ok( $total_hits, "search '$query_string'" );

	diag "Total hits: $total_hits\n" if $debug;

	cmp_ok( $total_hits, '==', $expected_hits, 'total_hits' );

	while ( my $hit = $results->next_result ) {
		diag dump($hit) if $debug;
	}

}

test_search( 'foo', 2 );

test_search( $_, 1 ) foreach @strange;

