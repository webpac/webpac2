#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 4;

BEGIN {
	use_ok( 'WebPAC::Test' );
	use_ok( 'WebPAC::Normalize::ISBN' );
}

is_deeply(
	[ isbn_13( '1558607013', '978-1558607019' ) ],
	[ '978-1-55860-701-9', '978-1-55860-701-9', ],
'isbn_13' );

is_deeply(
	[ isbn_10( '1558607013', '978-1558607019' ) ],
	[ '1-55860-701-3', '1-55860-701-3' ],
'isbn_10' );
