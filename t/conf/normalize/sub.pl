# check all repairs in WebPAC::Parser

sub foo {
	push @test, 'foo';
}

sub bar($) {
	push @test, "bar >>" . shift(@_) . "<<";
}

foo();
bar(foo());
bar(42);

sub test($) {
	push @test, $_[0];
}

foreach my $a ( 1,2,3 ) {
	test($a) if length($a) > 0;
}
