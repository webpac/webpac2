# example of comment

marc('777','v',
	lookup(
		sub { '1st-1' . rec('000') },
		'foo','foo-input1',
		sub { rec('11') }
	) ||
	lookup(
		sub { '1st-2' . rec('000') },
		'foo','foo-input2',
		sub { rec('11') }
	) ||
	lookup(
		sub { '2nd-1:' . rec('000') },
		'bar','bar-input',
		sub { rec(210,'a') . rec('210','e') },
	) ||
	lookup(
		sub { '2nd-2:' . rec('000') },
		'bar','bar-input',
		sub { rec(220,'a') . rec('220','e') },
	) ||
	lookup(
		sub { '2nd-3:' . rec('000') },
		'bar','bar-input',
		sub { rec(230,'a') . rec('230','e') },
	) ||
	lookup(
		sub { "3rd:" . rec('000') },
		'baz','baz-input',
		sub { rec(200,'a') },
		sub { rec(900,'x') },
	)
);

