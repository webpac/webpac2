marc('900','x',
	lookup(
		sub { rec('200','a') . ' ' . rec('200','b') },
		'no-database','baz',
		sub { rec('245','a') },
	),
	lookup(
		sub { rec('200','a') . ' ' . rec('200','b') },
		'foo','no-input',
		sub { rec('245','a') },
	),
	lookup(
		sub { rec('200','a') . ' ' . rec('200','b') },
		'foo','foo-input2',
		sub { rec('245','a') },
	),
	lookup(
		sub { rec('000') },
		'bar', 'bar-input',
		sub { rec('900','x') },
	);
);
