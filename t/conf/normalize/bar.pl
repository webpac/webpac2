marc('900','x',
	lookup(
		sub { rec('200','a') . ' ' . rec('200','b') },
		'foo','foo-input1',
		sub { rec('245','a') },
	),
	lookup(
		sub { rec('000') },
		'baz', 'baz-input',
		sub { rec('900','x') },
	)
);
