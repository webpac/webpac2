drop table if exists test;

create table test (
	id serial,
	foo int not null,
	bar int,
	baz text
);
