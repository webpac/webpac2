# special mungle file which prepare data_structure from XML

my $ds = get_ds;
warn "## ds = ",dump( $ds );

my @a = ( 'foo' );
my @s = ( 'bar' );

# check if tag is scalar or array and add it
foreach my $tag ( qw/number array/ ) {
	if ( ref($xml->{$tag}) eq 'ARRAY' ) {
		push @a, $tag;
	} else {
		push @s, $tag;
	}
}

warn '## collect data from @a = ',dump( @a ), ' @s = ',dump( @s );

# unroll content from arrays
foreach my $tag ( @a ) {
	next unless defined $ds->{xml}->{$tag};
	set_ds( $tag => [
		map { $_->{content} } @{ $ds->{xml}->{$tag} }
	] );
}

# and just copy scalars
foreach my $tag ( @s ) {
	next unless defined $ds->{xml}->{$tag};
	set_ds( $tag =>	$ds->{xml}->{$tag} );
}

