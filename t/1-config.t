#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 28;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Config' );
}

my $path = "$abs_path/conf/test.yml";

ok(my $config = new WebPAC::Config( path => $path ), "new");

cmp_ok($config->{path}, 'eq', $path, "path $path");

ok($config->{config}, "config exist");

# $config->databases

isa_ok($config->databases, 'HASH', "databases");
my @names = $config->databases;
isa_ok(\@names, 'ARRAY', "databases names");
cmp_ok(@names, '==', 3, "got 3 names");
isa_ok($config->databases, 'HASH', "databases");

diag '$config->webpac = ', dump($config->webpac) if ($debug);

isa_ok($config->webpac, 'HASH', "webpac");
my @inputs = $config->webpac('inputs');
diag "inputs = ",dump( @inputs ) if ($debug);
isa_ok(\@inputs, 'ARRAY', "inputs");
cmp_ok(@inputs, '==', 2, "got 2 webpac inputs");

$config->iterate_inputs(sub {
	my ($input,$database,$db_config) = @_;
	isa_ok($input, 'HASH', 'input');
	ok($database, 'database');
	isa_ok($db_config, 'HASH', 'database config');
	isa_ok($input->{normalize}, 'ARRAY', 'normalize');
});
