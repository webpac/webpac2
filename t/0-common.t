#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 7;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Common' );
}

my $path = "$abs_path/conf/test.yml";

my $common = 'WebPAC::Common';

cmp_ok( $common->fmt_time( 123456 ), 'eq', '10h17:36', 'fmt_time' );

throws_ok { $common->fill_in() } qr/no format/, 'fill_in without format';
throws_ok { $common->fill_in( '$foo', bar => 42 ) } qr/unknown var/, 'fill_in no variable';
cmp_ok( $common->fill_in( 'foo = $foo bar = $bar', foo => 42, bar => 1 ), 'eq', 'foo = 42 bar = 1', 'fill_in' );

like $common->var_path( qw/foo bar baz/ ), qr/var.foo.bar.baz/, 'var_path';
