#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 11;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Output::Webpacus' );
}

ok(my $out = new WebPAC::Output::Webpacus({
	path => '/data/Webpacus2/',
	database => 'test',
	clean => 1,
	%LOG
}), "new");

ok( $out->consume_outputs, 'consume_outputs' );

diag "consume_outputs = ",dump( $out->consume_outputs ) if $debug;

ok( $out->init, 'init' );

my $ds = {
	'Source' => {
		'name' => 'Izvor: ',
		'sorted' => [ 'foo' ]
	},
	'ID' => {
		'sorted' => 'id',
	},
	'Array' => {
		'sorted' => [ qw/a1 a2 s3 a4 a5/ ],
	},
};

throws_ok { $out->add( ) } qr/need id/, 'add without params';
throws_ok { $out->add( 42 ) } qr/need ds/, 'add without ds';

ok( $out->add( 42, $ds ), 'add 42' );

ok( $out->add( 99, { foo => { sorted => 'bar' } } ), 'add 99' );

ok( $out->add( 100, { foo => { sorted => [ qw/foo bar baz/ ] } } ), 'add 100' );

diag "path: ",$out->path if $debug;

cmp_ok( $out->finish, '==', 4, 'finish' );

