#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 84;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Input' );
use_ok( 'Encode' );
use_ok( 'Devel::Peek' );
}

my $module = 'WebPAC::Input::TSV';
diag "testing with $module";

ok(my $input = new WebPAC::Input(
	module => $module,
	no_progress_bar => 1,
	%LOG
), "new");

ok(my $db = $input->open(
	path => "$abs_path/data/records-utf8.tsv"
), "open");
ok(my $size = $input->size, "size");
cmp_ok( $size, '==', 11, 'size ok' );

foreach my $mfn ( 1 ... $size ) {
	my $rec = $input->fetch;
	ok($rec, "fetch $mfn");
	cmp_ok($rec->{'000'}->[0], '==', $mfn, 'has mfn');
	cmp_ok($input->pos, '==', $mfn, "pos $mfn");

	ok( my $txt = $rec->{'E'}, 'E' );
	diag Dump( $txt ) if $debug;
	ok( Encode::is_utf8( $txt, 1 ), 'utf8' );
	diag "rec: ", dump($rec), "\n" if $debug;
}

ok(my $db = $input->open(
	path => "$abs_path/data/header_first.tsv",
	header_first => 1,
), "open header_first");
ok(my $size = $input->size, "size");
cmp_ok( $size, '==', 9, 'size one leas because of header_first' );

foreach my $mfn ( 1 ... $size ) {
	my $rec = $input->fetch;
	ok($rec, "fetch $mfn");

	ok( $rec->{'publication_title'}, 'has publication_title' );
	diag "rec: ", dump($rec), "\n" if $debug;
}

