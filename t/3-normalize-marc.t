#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 331;

BEGIN {
	use_ok( 'WebPAC::Test' );
	use_ok( 'WebPAC::Normalize' );
	use_ok( 'WebPAC::Normalize::MARC' );
}

_debug( $debug - 1 ) if $debug > 1;

my $rec = {
	'225' => [{
		'a' => 'a-1-1',
		'i' => 'i-1-1',
		'v' => 'v-1-1',
		'w' => 'w-1-1',
		'h' => 'h-1-1',
		'x' => 'x-1-1',
	},{
		'a' => 'a-2-1',
		'v' => 'v-2-1',
		'i' => 'i-2-1',
	},{
		'a' => 'a-3-1',
		'i' => 'i-3-1',
		'v' => 'v-3-1',
	},{
		'a' => 'a-4-1',
		'v' => 'v-4-1',
		'i' => 'i-4-1',
		'w' => 'w-4-1',
	},{
		'a' => 'a-5-1',
		'v' => 'v-5-1',
		'i' => 'i-5-1',
	},{
		'a' => 'a-6-1',
		'i' => 'i-6-1',
		'w' => 'w-6-1',
	},{
		'a' => 'a-7-1',
	},{
		'a' => 'a-8-1',
		'v' => 'v-8-1',
	},{
		'v' => 'v-9-1',
	},{
		'v' => '0',
	}],
};

sub test_marc_template {
	my $rec = shift;

	ok( _clean_ds(), '_clean_ds' );
	ok( _set_ds( $rec ), '_set_ds' );

	ok( marc_indicators( 440, '0', '1' ), 'marc_indicators' );

	ok( marc_template(
		from => 225, to => 440,
		subfields_rename => [
			'a' => 'a',
			'x' => 'x',
			'v' => 'v',
			'h' => 'n',
			'i' => 'p',
			'w' => 'v',
		],
		isis_template => [
			'a ; |v. |i',
			'a. |i ; |w',
		],
		marc_template => [
			'a',
			'a ;|v',
			'a.|p',
			'a, |x ; |v. |n, |p ; |v',
			'a ; |v. |p ; |v',
			'v',
		],
	), 'marc_template' );

	ok(my $marc = WebPAC::Normalize::MARC::_get_marc_fields(), "_get_marc_fields");
	diag " _get_marc_fields = ",dump( $marc ) if $debug;

	ok( marc_indicators( 440, 'x', 'y' ), 'marc_indicators' );

	return $marc;
}

my $marc_out = [
	[440, "0", "1", "a", "a-1-1, ", "x", "x-1-1 ; ", "v", "v-1-1. ", "n", "h-1-1, ", "p", "i-1-1 ; ", "v", "w-1-1"],
	[440, "0", "1", "a", "a-2-1 ; ", "v", "v-2-1. ", "p", "i-2-1"],
	[440, "0", "1", "a", "a-3-1 ; ", "v", "v-3-1. ", "p", "i-3-1"],
	[440, "0", "1", "a", "a-4-1 ; ", "v", "v-4-1. ", "p", "i-4-1 ; ", "v", "w-4-1"],
	[440, "0", "1", "a", "a-5-1 ; ", "v", "v-5-1. ", "p", "i-5-1"],
	[440, "0", "1", "a", "a-6-1. ", "p", "i-6-1 ; ", "v", "w-6-1"],
	[440, "0", "1", "a", "a-7-1"],
	[440, "0", "1", "a", "a-8-1 ;", "v", "v-8-1"],
	[440, "0", "1", "v", "v-9-1"],
	[440, 0, 1, "v", 0],
];

is_deeply( test_marc_template($rec), $marc_out , 'is_deeply');

my $max_occ = $#{ $rec->{225} };

foreach my $from ( 0 .. $max_occ - 1 ) {
	foreach my $to ( $from + 1 .. $max_occ ) {
		my @orig_rec = @{ $rec->{225} };
		my $new_rec = {
			225 => [ splice( @orig_rec, $from, $to ) ],
		};
		diag "$from-$to new_rec = ",dump( $new_rec ) if $debug;

		my @expect = @$marc_out;
		my $expect_marc = [ splice( @expect, $from, $to ) ];
		diag "$from-$to expect_marc = ",dump( $expect_marc ) if $debug;

		is_deeply( test_marc_template($new_rec), $expect_marc, "$from-$to is_deeply");

	}
}

sub test_marc_clone {
	my ( $rec, $expect ) = @_;

	ok( _clean_ds(), '_clean_ds' );
	ok( _set_ds( $rec ), '_set_ds' );

	ok( ! marc_clone, 'marc_clone' );

	ok(my $marc = WebPAC::Normalize::MARC::_get_marc_fields(), "_get_marc_fields");
#	diag "rec = ",dump( $rec );
	diag "marc = ",dump( $marc );
	diag "expect = ",dump( $expect );

	is_deeply( $marc, $expect, 'marc_clone same' );
diag "marc_leader = ",dump( marc_leader );
	cmp_ok( WebPAC::Normalize::MARC::marc_leader()->{0}, 'eq', $rec->{leader}->[0], 'leader' );

}

$rec = {
  "000"  => [2],
  "001"  => ["ocm00734950"],
  "003"  => ["OCoLC"],
  "040"  => [
              {
                a => "DLC",
                c => "BOS",
                d => "TML",
                i1 => "1",
                i2 => "2",
                subfields => ["a", 0, "c", 0, "d", 0],
              },
            ],
  245    => [
              {
                1 => 2,
                a => "A treatise on insanity /",
                c => "translated from the French by D. D. Davis, with an introd. by Paul F. Cranefield.",
                subfields => [1, 0, "a", 0, "c", 0],
		i1 => '0',
		i2 => '1',
              },
            ],
  leader => ["01237cam  2200301Ii 4500"],
};

test_marc_clone( $rec, [
  ["000", 2],
  ["001", "ocm00734950"],
  ["003", "OCoLC"],
  ["040", 1,2, "a" => "DLC", "c" => "BOS", "d" => "TML"],
  [ 245,  0,1, 1 => 2, "a" => "A treatise on insanity /", "c" => "translated from the French by D. D. Davis, with an introd. by Paul F. Cranefield." ],
] );

