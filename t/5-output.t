#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 8;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Output' );
}

my $ds = {
	'000' => { foo => [ 42 ], bar => 'bug!' },
	'900' => { foo => [
		{ a => '900a1', b => '900b1' },
		{ a => '900a2', b => '900b2' },
		{ a => '900a3', b => '900b3' },
	] },
	'902' => { foo => [
		{ a => '900a1', b => '900b1' },
	] },

	'StRaNgE!Field' => { foo => 'value' },

	'array' => { foo => [ 'a' .. 'c' ] },
};

my $hash;
ok( $hash = WebPAC::Output->ds_to_hash( $ds, 'foo' ), 'ds_to_hash' );
diag dump($hash) if $debug;
is_deeply( $hash, {
   "000" => [42],
   900   => [
              { a => "900a1", b => "900b1" },
              { a => "900a2", b => "900b2" },
              { a => "900a3", b => "900b3" },
            ],
   902   => [{ a => "900a1", b => "900b1" }],
   array => ["a", "b", "c"],
   strange_field => "value",
}, 'is_deeply');

ok( $hash = WebPAC::Output->ds_to_hash( $ds, 'foo', disable_key_mungle => 1 ), 'ds_to_hash disable_key_mungle' );
diag dump($hash) if $debug;
is_deeply( $hash, {
   "000" => [42],
   900   => [
              { a => "900a1", b => "900b1" },
              { a => "900a2", b => "900b2" },
              { a => "900a3", b => "900b3" },
            ],
   902   => [{ a => "900a1", b => "900b1" }],
   array => ["a", "b", "c"],
   'StRaNgE!Field' => "value",
}, 'is_deeply');

ok( $hash = WebPAC::Output->ds_to_hash( $ds, 'foo', single_values => 1 ), 'ds_to_hash single_values' );
diag dump($hash) if $debug;
is_deeply( $hash, {
   "000" => 42,
   900 => "{ a => \"900a1\", b => \"900b1\" } { a => \"900a2\", b => \"900b2\" } { a => \"900a3\", b => \"900b3\" }",
   902 => "{ a => \"900a1\", b => \"900b1\" }",
   array => "a b c",
   strange_field => "value",
}, 'is_deeply');


