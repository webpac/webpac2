#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 24;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Input' );
}

my $module = 'WebPAC::Input::XML';
diag "testing with $module";

ok(my $input = new WebPAC::Input(
	module => $module,
	no_progress_bar => 1,
	%LOG
), "new");

throws_ok { $input->open( path => '/tmp/does_not_exit', %LOG ) } qr/can't find path/, 'open with non-existant path';

ok(my $db = $input->open(
	path => "$abs_path/data/xml/",
	mungle => "$abs_path/conf/mungle/xml.pl",
), "open");
ok(my $size = $input->size, "size");

foreach my $mfn ( 1 ... $size ) {
	my $rec = $input->fetch;
	if ( $mfn >= 7 ) {
		ok(! $rec, "error $mfn");
	} else {
		ok($rec, "fetch $mfn");
	}

	cmp_ok($input->pos, '==', $mfn, "pos $mfn");

	diag "rec: ", dump($rec), "\n" if $debug;
}

