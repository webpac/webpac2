#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More;

BEGIN {

	eval "use XBase";
	if ( $@ ) {
		plan skip_all => "XBase required for WebPAC::Input::DBF";
	} else {
		plan tests => 1179;
	}

	use_ok( 'WebPAC::Test' );
	use_ok( 'WebPAC::Input' );
}

my $module = 'WebPAC::Input::DBF';
diag "testing with $module", $debug ? ' with debug' : '';

ok(my $input = new WebPAC::Input(
	module => $module,
	no_progress_bar => 1,
	%LOG
), "new");

ok(my $db = $input->open(
	path => "$abs_path/data/cas2000.dbf",
	input_config => {
		mapping_path => "$abs_path/conf/input/dbf/cas2000.yml",
	},
), "open");
ok(my $size = $input->size, "size");

diag "size: $size" if ($debug);

ok(defined($input->{ll_db}->{_rows}), 'have ll_db->rows');

foreach my $mfn ( 1 ... $size ) {

	ok(defined($input->{ll_db}->{_rows}->{$mfn}), "have ll_db->_rows->$mfn");

	diag "row: ", dump( $input->{ll_db}->{_rows}->{$mfn} ) if ($debug);

	my $rec = $input->fetch;

	ok($rec, "fetch $mfn");

	cmp_ok($input->pos, '==', $mfn, "pos $mfn");

	diag "rec: ", dump($rec), "\n" if ($debug);
}

