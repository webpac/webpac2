#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 12;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Input' );
}

my $module = 'WebPAC::Input::ISI';
diag "testing with $module";

ok(my $input = new WebPAC::Input(
	module => $module,
	no_progress_bar => 1,
	%LOG
), "new");

ok(my $db = $input->open(
#	path => "$abs_path/data/recs.txt"
	path => "$abs_path/data/isi.txt"
), "open");
ok(my $size = $input->size, "size");
cmp_ok( $size, '==', 3, 'size ok' );

foreach my $mfn ( 1 ... $size ) {
	my $rec = $input->fetch;

	ok( exists($rec->{TI}), 'TI' );

	cmp_ok($input->pos, '==', $mfn, "pos $mfn");

	diag "rec: ", dump($rec), "\n" if $debug;
}

