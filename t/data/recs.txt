FN ISI Export Format
VR 1.0
PT J
AU Knobel, R
   Holditch-Davis, D
AF Knobel, Robin
   Holditch-Davis, Diane
TI Thermoregulation and heat loss prevention after birth and during
   neonatal intensive-care unit stabilization of extremely low-birthweight
   infants
SO JOGNN-JOURNAL OF OBSTETRIC GYNECOLOGIC AND NEONATAL NURSING
LA English
DT Article
DE delivery room resuscitation; ELBW infants; hypothermia; neonatal
   thermoregulation
ID PREMATURE-INFANTS; MORTALITY; ASSOCIATION; SURVIVAL; DELIVERY; TRIAL;
   LIFE
AB Extremely low-birthweight infants have inefficient thermoregulation due
   to immaturity and may exhibit cold body temperatures after birth and
   during their first 12 hours of life. Hypothermia in these infants can
   lead to increased morbidity and mortality. Anecdotal notes made during
   our recent study revealed extremely low-birthweight infants'
   temperatures decreased with caregiver procedures such as umbilical line
   insertion, intubations, obtaining chest x-rays, manipulating
   intravenous lines, repositioning, suctioning, and taking vital signs
   during the first 12 hours of life. Therefore, nursing interventions
   should be undertaken to prevent heat loss during these caregiver
   procedures. Nurses can improve the thermal environment for extremely
   low-birthweight infants by prewarming the delivery room and placing the
   infant in a plastic bag up to the neck during delivery room
   stabilization to prevent heat loss.
C1 Univ N Carolina, Sch Nursing, Chapel Hill, NC USA.
   Duke Univ, Sch Nursing, Durham, NC USA.
RP Knobel, R, 408 Long Point Rd, Chocowinity, NC 27817 USA.
EM rbknobel@earthlink.net
CR *AM AC PED AM HEAR, 2005, SUMM MAJOR CHANG GUI
   *AM AC PED COLL OB, 1988, GID PER CAR
   ANDERSON P, 1998, FETAL NEONATAL PHYSL, V1, P837
   ASKIN DF, 2002, JOGNN, V31, P318
   BARRETT E, 2003, MED PHYSL CELLULAR M, P1035
   CLOHERTY J, 2003, MANUAL NEONATAL CARE
   CRAMER K, 2005, J PERINATOLOGY, V25, P763
   DAY RL, 1964, PEDIATRICS, V34, P171
   DESHPANDE SA, 1997, ARCH DIS CHILD, V76, F15
   GUYTON A, 2006, TXB MED PHYSL
   HAMMARLUND K, 1979, ACTA PAEDIATR SCAND, V68, P795
   HATAI S, 1902, ANAT ANZEIGER, V21, P369
   HAZAN J, 1991, AM J OBSTET GYNECOL, V164, P111
   HORNS K, 2002, ADV NEONATAL CARE, V2, P149
   HOUSTEK J, 1993, J CLIN ENDOCR METAB, V77, P382
   HULL D, 1977, SCI FDN OBSTET GYNAE, P540
   JONES E, 2003, MED PHYSL CELLULAR M, P1190
   KNOBEL R, 2006, THESIS U N CAROLINA, P1
   KNOBEL RB, 2005, J PERINATOL, V25, P304
   KNOBEL RB, 2005, J PERINATOL, V25, P514
   LOUGHEAD MK, 1997, PEDIAT NURS, V23, P11
   LYON AJ, 1997, ARCH DIS CHILD, V76, F47
   MALIN SW, 1987, PEDIATRICS, V79, P47
   MATHEW R, 1998, FETAL NEONATAL PHYSL, V1, P924
   MATHEWS TJ, 2006, NATL VITAL STAT REP, V54, P1
   NADEL E, 2003, MED PHYSL, P1231
   NECHAD M, 1986, BROWN ADIPOSE TISSUE, P1
   RICHARDOSN DK, 2001, J PEDIATR, V138, P92
   SAUER P, 1995, THERMOREGULATION SIC, P9
   SEDIN G, 1995, THERMOREGULATION SIC, P21
   SERI I, 1998, FETAL NEONATAL PHYSL, V2, P1726
   SILVERMAN WA, 1958, PEDIATRICS, V22, P876
   SINCLAIR JC, 1992, EFFECTIVE CARE NEWBO, P40
   THOMAS KA, 2003, J PERINATOL, V23, P640
   VOET D, 2002, FUNDAMENTALS BIOCH, P492
   VOHRA S, 1999, J PEDIATR, V134, P547
   VOHRA S, 2004, J PEDIATR, V145, P750
   WIDMAIER J, 2005, VANDERS HUMAN PHYSL
NR 38
TC 0
PU BLACKWELL PUBLISHING
PI OXFORD
PA 9600 GARSINGTON RD, OXFORD OX4 2DQ, OXON, ENGLAND
SN 0884-2175
J9 JOGNN
JI JOGNN
PD MAY-JUN
PY 2007
VL 36
IS 3
BP 280
EP 287
PG 8
SC Nursing; Obstetrics & Gynecology
GA 171YU
UT ISI:000246772100011
ER

PT J
AU Sandberg, KL
   Poole, SD
   Hamdan, A
   Minton, PA
   Sundell, HW
AF Sandberg, Kenneth L.
   Poole, Stanley D.
   Hamdan, Ashraf
   Minton, Patricia A.
   Sundell, Hakan W.
TI Prenatal nicotine exposure transiently alters the lung mechanical
   response to hypoxia in young lambs
SO RESPIRATORY PHYSIOLOGY & NEUROBIOLOGY
LA English
DT Article
DE respiratory physiology; respiratory function tests; nicotine; prenatal
   exposure; tobacco smoke pollution; hypoxia; sheep
ID INFANT-DEATH-SYNDROME; VENTILATORY RESPONSES; CARDIORESPIRATORY
   RESPONSE; PULMONARY-FUNCTION; CIGARETTE-SMOKING; MATERNAL SMOKING;
   GENE-EXPRESSION; NEWBORN RAT; SURFACTANT; COTININE
AB To test the hypothesis that fetal nicotine exposure alters the lung
   mechanical response to hypoxia (10% 02) 10 lambs were exposed during
   the last fetal trimester to a low dose nicotine (LN) and 10 to a
   moderate dose (MN) (maternal dose 0.5 and 1.5 mg/(kg day) free base,
   respectively). There were 10 controls (C). At 12 days, minute
   ventilation increased significantly less in NIN compared with LN but
   not with C. In contrast to C and LN, MN did not show anticipated
   increases in dynamic compliance, specific compliance and FRC or
   decrease in lung resistance but had signs of airway hyperreactivity
   during hypoxia. Nicotine exposure did not alter the cardiovascular
   response. These adverse effects decreased with advancing age. In
   summary, prenatal nicotine exposure alters the lung mechanical response
   to hypoxia. We speculate that prenatal nicotine-induced alterations of
   lung mechanics during hypoxia may contribute to an increased
   vulnerability to hypoxic stress during infancy. (C) 2006 Elsevier B.V.
   All rights reserved.
C1 Vanderbilt Univ, Sch Med, Dept Pediat, Nashville, TN 37232 USA.
   Gothenburg Univ, Dept Women & Child Hlth, SE-41685 Gothenburg, Sweden.
RP Sundell, HW, Vanderbilt Univ, Sch Med, Dept Pediat, B-1220,MCN,
   Nashville, TN 37232 USA.
EM hakan.sundell@vanderbilt.edu
CR ALTMAN DG, 1991, PRACTICAL STAT MED R, P431
   BAMFORD OS, 1996, RESP PHYSIOL, V106, P1
   BAMFORD OS, 1999, RESP PHYSIOL, V117, P29
   BONORA M, 1997, J APPL PHYSIOL, V83, P700
   BONORA M, 1999, J APPL PHYSIOL, V87, P15
   CHEN CM, 2005, PEDIATR PULM, V39, P97
   COLLINS MH, 1985, PEDIATR RES, V19, P408
   CRAPO RO, 2000, AM J RESP CRIT CARE, V161, P309
   DENJEAN A, 1991, RESP PHYSIOL, V83, P201
   EDEN GJ, 1987, J PHYSIOL-LONDON, V392, P11
   EFRON B, 1971, BIOMETRIKA, V58, P403
   ELLIOT J, 1998, AM J RESP CRIT CARE, V158, P802
   ELLIOT J, 2001, AM J RESP CRIT CARE, V163, P140
   FILIANO JJ, 1994, BIOL NEONATE, V65, P194
   GAUDA EB, 2001, J APPL PHYSIOL, V91, P2157
   GREEN M, 1966, J PHYSIOL-LONDON, V186, P363
   HAFSTROM A, 2005, RESP PHYSIOL NEUROBI, V149, P325
   HAFSTROM O, 2002, AM J RESP CRIT CARE, V166, P1544
   HAFSTROM O, 2002, AM J RESP CRIT CARE, V166, P92
   HARPER RM, 2000, RESP PHYSIOL, V119, P123
   HILL P, 1983, J CHRON DIS, V36, P439
   HJALMARSON O, 1974, ACTA PAEDITA SCAND S, P5
   ISCOE S, 1995, J APPL PHYSIOL, V78, P117
   JACOB P, 1981, J CHROMATOGR, V222, P61
   JACOB P, 1991, BIOL MASS SPECTROM, V20, P247
   JOYCE BJ, 2001, PEDIATR RES, V50, P641
   KINNEY HC, 1993, NEUROSCIENCE, V55, P1127
   LEWIS KW, 1995, J PEDIATR, V127, P691
   MARITZ GS, 1993, CELL BIOL INT, V17, P1085
   MEAD J, 1955, J CLIN INVEST, V34, P1005
   MILERAD J, 1993, ACTA PAEDIATR, V82, P70
   MITCHELL EA, 1999, SMOKING SUDDEN INFAN
   NADEL JA, 1962, J PHYSIOL-LONDON, V163, P13
   NAVARRO HA, 1989, BRAIN RES BULL, V23, P187
   OTIS AB, 1956, J APPL PHYSIOL, V8, P427
   OYARZUN MJ, 1977, J APPL PHYSIOL, V43, P39
   POOLE KA, 2000, AM J RESP CRIT CARE, V162, P801
   POWELL FL, 1998, RESP PHYSIOL, V112, P123
   PROSKOCIL BJ, 2005, AM J RESP CRIT CARE, V171, P1032
   ROGNUM TO, 1988, PEDIATRICS, V82, P615
   ROY TS, 1998, J PHARMACOL EXP THER, V287, P1136
   SANDBERG K, 1987, ACTA PAEDIATR SCAND, V76, P30
   SANDBERG K, 1991, PEDIATR RES, V30, P181
   SANDBERG K, 2004, PEDIATR RES, V56, P432
   SCHUEN JN, 1997, RESP PHYSIOL, V109, P231
   SEKHON HS, 1999, J CLIN INVEST, V103, P637
   SEKHON HS, 2001, AM J RESP CRIT CARE, V164, P989
   SEKHON HS, 2002, AM J RESP CELL MOL, V26, P31
   SJOQVIST BA, 1986, MED BIOL ENG COMPUT, V24, P83
   SLADEK M, 1993, PEDIATR RES, V34, P821
   SLOTKIN TA, 1997, TERATOLOGY, V55, P177
   SOVIK S, 1999, EARLY HUM DEV, V56, P217
   STEELE R, 1966, CAN MED ASSOC J, V94, P1165
   STJOHN WM, 1999, NEUROSCI LETT, V267, P206
   UEDA Y, 1999, J PEDIATR 1, V135, P226
NR 55
TC 0
PU ELSEVIER SCIENCE BV
PI AMSTERDAM
PA PO BOX 211, 1000 AE AMSTERDAM, NETHERLANDS
SN 1569-9048
J9 RESPIR PHYSIOL NEUROBIOL
JI Respir. Physiol. Neuro.
PD JUN 15
PY 2007
VL 156
IS 3
BP 283
EP 292
PG 10
SC Physiology; Respiratory System
GA 161UH
UT ISI:000246041200006
ER

EF