search_display('MFN',
	rec('000')
);

display('ISBN',
	rec('10')
);

display('ISSN',
	rec('11')
);

search('ISN',
	rec('10'),
	rec('11'),
);

search_display('Language',
	rec('101')
);

search_display('TitleProper',
	regex( 's/<[^>]*>//g',
		rec('200','a')
	)
);

search_display('titleNo',
	rec('200','9')
);

search_display('Subtitle',
	rec('200','e')
);

search_display('TitleProper2',
	rec('200','c')
);

search_display('ParallelTitle',
	rec('200','d')
);

search_display('Responsibility',
	join_with(" ; ",
		rec('200','f'),
		rec('200','g')
	)
);

display('ResponsibilityFirst',
	rec('200','f')
);

display('ResponsibilitySecond',
	rec('200','g')
);

search_display('VolumeDesignation',
	rec('200','v')
);

search_display('EditionStatement',
	rec('205','a')
);

search_display('SerialNo',
	rec('207','a')
);

search_display('fond',
	rec('209','a')
);

search_display('PlacePublication',
	rec('210','a')
);

search_display('NamePublisher',
	rec('210','c')
);

search_display('DatePublication',
	rec('210','d')
);

search_display('PhysicalDescription',
	join_with(" : ",
		rec('215','a'),
		join_with(" ; ",
			rec('215','c'),
			rec('215','d'),
		)
	)
);

search_display('MaterialDesignation',
	rec('215','a')
);

search_display('PhysicalDetails',
	rec('215','c')
);

search_display('AccompanyingMaterial',
	rec('215','e')
);

search_display('Series',
	join_with(" = ", rec('225','a'),
		join_with(" : ", rec('225','d'),
			join_with(" ; ", rec('225','e'),
				join_with(". ", rec('225','v'),
					join_with(", ", rec('225','h'),
						join_with(" ; ", rec('225','i'),
							rec('225','w'),
						)
					)
				)
			)
		)
	)
);

search_display('SeriesTitle',
	rec('225','a')
);

search_display('GeneralNote',
	rec('300')
);

search_display('EditionNote',
	rec('305')
);

search_display('PhysicalDescriptionNote',
	rec('307')
);

search_display('IntellectResponsNote',
	rec('314')
);

search_display('InternalBibliographies',
	rec('320')
);

search_display('Frequency',
	rec('326')
);

search_display('ContentsNote',
	rec('327')
);

search_display('Summary',
	rec('330')
);

search_display('SystemRequirements',
	rec('337')
);

search_display('IssuedWith',
	join_with(': ', rec(423,'z'),
		join_with(' / ', rec(423,'a'),
			join_with(' ', rec(423,'c'),
				rec(423,'b')
			)
		)
	)
);

search_display('PartsID',
	rec('463','1')
);

search_display('PieceNum',
	rec('463','v')
);

search_display('PieceAnalitic',
	join_with(' / ',
		rec('464','a'),
		join_with(' ',
			rec(464,'g'),
			rec(464,'f'),
		)
	)
);

search_display('UniformHeading',
	join_with('. ',
		rec(500,'a'),
		rec(500,'b'),
	)
);

search_display('ExpandedTitle',
	rec(532)
);

search_display('Form',
	rec(608)
);

search_display('UncontrolledTerms',
	rec(610)
);

search('UDC_All',
	rec(675),
);

search_display('UDC',
	rec(675)
);

search_display('APACC',
	rec(686)
);

search_display('PersonalNamePrim',
	join_with(', ',
		rec(700,'a'),
		rec(700,'b'),
	)
);

search_display('PersonalNameSec',
	join_with(', ',
		rec(701,'a'),
		rec(701,'b'),
	)
);

search_display('PersonalNameOther',
	join_with(', ',
		rec(702,'a'),
		rec(702,'b'),
	)
);

search('Names',
	rec(700),
	rec(701),
	rec(702),
);

search_display('CorporateNamePrim',
	join_with(', ',
		rec(710,'a'),
		rec(710,'b'),
	)
);

search_display('CorporateNameSec',
	join_with(', ',
		rec(711,'a'),
		rec(711,'b'),
	)
);

search_display('OriginatingSource',
	rec(801)
);

search_display('URL',
	rec(856,'u')
);

search_display('level',
	rec(909)
);

search_display('ID',
	rec(900)
);

search('Set',
	rec(946,1),
	rec(461,1),
);

search_display('CallNo',
	rec(990)
);

search_display('InvNo',
	rec(991)
);

1;
