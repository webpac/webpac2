#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More;

BEGIN {

	eval "use CAM::PDF";

	if ( $@ ) {
		plan skip_all => "CAM::PDF required for WebPAC::Input::PDF";
	} else {
		plan tests => 11;
	}

	use_ok( 'WebPAC::Test' );
	use_ok( 'WebPAC::Input' );
}

my $module = 'WebPAC::Input::PDF';
diag "testing with $module";

ok(my $input = new WebPAC::Input(
	module => $module,
	no_progress_bar => 1,
	%LOG
), "new");

ok(my $db = $input->open(
	path => "$abs_path/data/UFOReport1998.pdf"
), "open");
ok(my $size = $input->size, "size");

$size = 3;

foreach my $mfn ( 1 ... $size ) {
	my $rec = $input->fetch;
	if ($mfn <= 10 || $mfn == 20) {
		ok($rec, "fetch $mfn");
	} else {
		ok(! $rec, "empty $mfn");
	}

	cmp_ok($input->pos, '==', $mfn, "pos $mfn");

	diag "rec: ", dump($rec), "\n" if $debug;
}

