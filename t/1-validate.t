#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 56;

BEGIN {
use_ok( 'WebPAC::Test' );
use_ok( 'WebPAC::Validate' );
}

ok(my $v = new WebPAC::Validate(%LOG), "new witout path");

ok( ! $v->{rules}, 'no path' );

ok($v = new WebPAC::Validate(
	path => "$abs_path/data/validate_test",
	%LOG,
), "new with path");

ok($v->{rules}, "rules exist");

is_deeply( $v->{rules}, {
	'900' => 1,
	'901' => [ 'a' ],
	'902' => [ 'b', 'c' ],
	'903' => [ 'a', 'b', 'c' ],
	'904' => [ 'a' ],
	'905' => [ 'a*' ],
	'906' => [ '0' ],
}, 'simple rules parsed');

diag dump( $v ) if ( $debug );

ok( $v->read_validate_file( "$abs_path/data/validate_test_simple" ), "read_validate_file" );

diag dump( $v ) if ( $debug );

ok($v->{rules}, "rules exist");

is_deeply( $v->{rules}, {
	'900' => [ 'a', 'b', 'c', 'd' ],
}, 'rules parsed');

ok( $v->read_validate_file( "$abs_path/data/validate_test" ), "read_validate_file" );

is_deeply( $v->{rules}, {
	'900' => 1,
	'901' => [ 'a' ],
	'902' => [ 'b', 'c' ],
	'903' => [ 'a', 'b', 'c' ],
	'904' => [ 'a' ],
	'905' => [ 'a*' ],
	'906' => [ '0' ],
}, 'rules');

ok($v->{rules}, "rules exist");

throws_ok { $v->validate_rec() } qr/rec/, "validate_rec need rec";

sub test_v {
	my $row = shift || die "no row?";

	my $d = dump( $row );

	$row->{'000'} = [ 42 ];

	$v->reset;
	my $e = $v->validate_rec( $row );

	diag "validate $d\n",dump($e) if ($debug);

	if (@_) {
		my $tmp = $e;
		while (@_) {
			my $k = shift @_;
			ok($tmp = $tmp->{$k}, "found $k") if (defined($k));
		}
		diag "tmp: ",dump($tmp) if ($debug);
		if ($tmp) {
			if (ref($tmp) eq 'HASH') {
				return $tmp;
			} else {
				diag "explanation: $tmp" if $debug;
			}
		}
	} else {
		ok(! $e, "validated $d");
		diag "expected error: ", dump($e) if($e);
	}

}

test_v({
	'900' => 'foo'
}, qw/900 not_repeatable/);

test_v({
	'900' => [ qw/foo bar baz/ ]
});

test_v({
	'901' => [ qw/foo bar baz/ ]
}, qw/901 missing_subfield/);

test_v({
	'901' => [ { 'a' => 42 } ]
});

test_v({
	'901' => [ { 'b' => 42 } ]
}, qw/901 subfield extra b/);

test_v({
	'902' => [ { 'b' => 1 }, { 'c' => 2 } ]
});

test_v({
	'902' => [ { 'a' => 0 }, { 'b' => 1 }, { 'c' => 2 } ]
}, qw/902 subfield extra a/);

test_v({
	'903' => [ { 'a' => 0 }, { 'b' => 1 }, { 'c' => 2 } ]
});

test_v({
	'903' => [ { 'a' => 0 }, { 'b' => 1 }, { 'c' => 2 }, { 'd' => 3 } ]
}, qw/903 subfield extra d/);

is_deeply(
	test_v({
		'903' => [ { 'a' => 0 }, { 'b' => 1 }, { 'c' => 2 }, { 'd' => 3 }, { 'e' => 4 } ]
	}, qw/903 subfield extra/),
{ 'd' => 1, 'e' => 1 }, 'additional fields d, e');

test_v({
	'904' => [ { 'a' => 1, } ]
});

test_v({
	'904' => [ { 'b' => 1 } ]
}, qw/904 subfield extra b/);

test_v({
	'904' => [ { 'a' => [ 1,2 ] } ]
}, qw/904 subfield extra_repeatable a/);

test_v({
	'905' => [ { 'a' => [ 1,2 ] } ]
});

test_v({
	'905' => [ ]
});

test_v({
	'906' => [ ]
});

test_v({
	'906' => [ { '0' => 'foo' } ]
});

my $expected_error = {
   900 => { not_repeatable => "probably bug in parsing input data" },
   901 => { missing_subfield => "a required", "dump" => "baz" },
   902 => {
            "dump"   => "^a1^b1^b2",
            subfield => { extra => { a => 1 }, extra_repeatable => { b => 1 } },
          },
   903 => {
            "dump"   => "^a1^a2^c1",
            subfield => { extra_repeatable => { a => 1 } },
          },
   904 => { subfield => { extra => { b => 1 }, missing => { a => 1 } } },
};


is_deeply(
	test_v({
		'900' => 'foo',
		'901' => [ qw/foo bar baz/ ],
		'902' => [ { 'a' => 1, 'b' => [ 1,2 ] } ],
		'903' => [ { 'a' => [ 1, 2 ], 'c' => 1, } ],
		'904' => [ { 'b' => 1 } ],
		'905' => [ { 'a' => 1 } ],
	}, undef),
$expected_error, 'validate without subfields');

ok(my $r1 = $v->report, 'report');

diag "report: $r1" if ( $debug );

is_deeply(
	test_v({
		'900' => 'foo',
		'901' => [ qw/foo bar baz/ ],
		'902' => [ { 'a' => 1, 'b' => [ 1,2 ], subfields => [ qw/a 0 b 0 b 1/ ] } ],
		'903' => [ { 'a' => [ 1, 2 ], 'c' => 1, subfields => [ qw/a 0 a 1 c 0/ ] } ],
		'904' => [ { 'b' => 1, subfields => [ qw/b 0/ ] } ],
		'905' => [ { 'a' => 1, subfields => [ qw/a 0/ ] } ],
	}, undef),
$expected_error, 'validate with subfields');

ok(my $r2 = $v->report, 'report');

cmp_ok($r1, 'eq', $r2, 'subfields same as non-subfields');
