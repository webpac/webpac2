#!/usr/bin/perl -w

use strict;
use lib 'lib';

use Test::More tests => 6;

BEGIN {
	use lib 'lib';
	use_ok( 'WebPAC::Test' );
	use_ok( 'WebPAC::Output::DBI' );
}

ok(my $out = new WebPAC::Output::DBI({
	dsn => 'dbi:Pg:dbname=webpac2',
	schema => "$abs_path/conf/schema.sql",
	%LOG
}), "new");

ok( $out->init, 'init' );

my $ds = { '_rows' => {
	'test' => [
		{ foo => 42 },
		{ foo => 1, bar => 11 },
		{ foo => 99, baz => 'text' },
	],
} };

ok( $out->add( 42, $ds ), 'add 42' );

ok( $out->finish, 'finish' );

