# warn dump(rec('AU'));
# warn dump(rec_array('AU'));

#di => split( /;/, rec(

row( 'citing',
	ut => rec('UT'),
	pt => rec('PT'),
	au => join_with(' ; ',
		rec('AU')
	),
	ti => rec('TI'),
	so => rec('SO'),
	la => rec('LA'),
	dt => rec('DT'),
	de => rec('DE'),
	id => rec('ID'),
	de => join_with(' ; ',
		rec('DE'),
	),
	id => join_with(' ; ',
		rec('ID'),
	),
	rp => uc ( rec('RP') ),
	nr => rec('NR'),
	tc => rec('TC'),
	pi => rec('PI'),
	py => rec('PY'),
	di => rec('DI'),
	sc => rec('SC')
);

if ( rec('RP') ) {
	row( 'rpco',
		ut => rec('UT'),
		rp => rec('RP'),
		rpco => 	
			regex('s/.*,(.*)/$1/',
				regex('s/\.$//',
					rec('RP')
				)
			),
	);
}

my @af = rec_array ('AF');
foreach my $au ( rec_array ('AU') ) {
	foreach my $cr ( rec_array('CR') ) {
		row( 'cited', 
			ut => rec('UT'),
			ca => config('input name'),
			cr_auth => $cr->{author}, 
			au => regex('s/,//',
				uc ( $au )
			),
			cr_full => $cr->{full},
			cr_year => $cr->{year},
			cr_ref => $cr->{reference}
		);
   	}
	row( 'authors',
		ut => rec('UT'),
		ca => config('input name'),
		au => regex('s/,//',
			uc ( $au )
		),
		af => shift @af,
	)
}

row( 'utca',
	ut => rec('UT'),
	ca => config('input name')
);


