
foreach my $au ( rec_array 'AU' ) {
	row( 'authors', au => $au );
	foreach my $cr ( rec_array 'CR' ) {
		if ( ! $cr->{author} ) {
#			warn "# cr ",dump( $cr );
			next;
		}
		row( 'cited',
			cited_au => $cr->{author},
			from_au => $au,
		);
	}
}

