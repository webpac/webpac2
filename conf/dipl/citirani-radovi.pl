# warn dump(rec('AU'));
# warn dump(rec_array('AU'));

row( 'citirani',
	ca => rec('A'),
	cr_auth => rec('B'),
	cr_ref => rec('C'),
	cr_year => rec('D'),
	cr_vol => rec('E'),
	cr_page => rec('F'),
	catc => rec('H'),
	cr_full => 
		join_with(', ',
			join_with(', P',
				join_with(', V',
					join_with(', ',
						rec('B'),
						rec('D'),
						rec('C'),
					),
					rec('E'),
				),
				rec('F')
			), 
			rec('G')
		),
	isi => rec('I'),
	camb => rec('J')
);
