tag('label',
	rec('UT'),
);

tag('autor',
	rec('AU')
);

tag('autor_full',
	rec('AF'),
);

tag('naslov',
	rec('TI'),
);

tag('publikacija',
	rec('SO')
);

tag('jezik',
	rec('LA'),
);

tag('vrsta_rada',
	rec('DT'),
);

tag('autorove_kljucne_rijeci',
	rec('DE'),
);

tag('isi_kljucne_rijeci',
	rec('ID')
);

tag('sazetak',
	rec('AB'),
);

tag('reprint_adresa',
	rec('RP')
);

tag('reprint_zemlja',
	regex('s/.*,//',
		rec('RP')
	)
);

tag('autorova_adresa',
	rec('C1')
);


tag('autorova_zemlja',
	regex('s/.*,//',
		rec('C1')
	)
);

tag('citirani_radovi',
	rec('CR','full')
);

tag('citirani_autor',
	rec('CR','author')
);

tag('citirani_ustanova)',
	rec('CR','institution')
);

tag('citirani_godina',
	rec('CR','year')
);

tag('citirani_publikacija',
	rec('CR','reference')
);

tag('citirani_stranica',
	rec('CR','page')
);

tag('citirani_volumen',
	rec('CR','volume')
);

tag('broj_citiranih_radova',
	rec('NR')
);

tag('broj_citata',
	rec('TC')
);

tag('izdavac',
	rec('PU')
);

tag('mjesto_izdavanja',
	rec('PI')
);

tag('adresa_izdavaca',
	rec('PA')
);

tag('issn',
	rec('SN')
);

tag('isbn',
	rec('BN')
);

tag('skraceni_naziv_publikacije',
	rec('J9')
);

tag('iso_skranceni_naziv',
	rec('JI')
);

tag('datum_izdavanja',
	rec('PD')
);

tag('godina_izdavanja',
	rec('PY')
);

tag('volumen',
	rec('VL')
);

tag('broj',
	rec('IS')
);

tag('dio',
	rec('PN')
);

tag('prva_stranica',
	rec('BP')
);

tag('zadnja_stranica',
	rec('EP')
);

tag('broj_stranica',
	rec('PG')
);

tag('podrucje',
	rec('SC')
);

