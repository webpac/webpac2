drop table if exists utca;

create table utca (
	id serial,
	ut text,
	ca text,
);

drop table if exists cited;

create table cited (
	id serial,
	cited_au text,
	from_au text,
	ut text,
	cited_full text
);

drop table if exists authors;

create table authors (
	id serial,
	au text not null
);


drop table if exists citing;

create table citing (
	id serial,
	ut text,
	ca text,
	pt text,
	au text,
	af text,
	ti text,
	so text,
	la text,
	dt text,
	c1 text,
	rp text,
	cr text,
	nr integer,
	pi text,
	py integer,
	sc text
);


-- create index cited_au on cited(au);
-- create index cited_cited on cited(cited);

