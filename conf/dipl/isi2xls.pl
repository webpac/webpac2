sub csv { to('csv',@_) }

#warn( dump(
#	rec('CR','author')
#)
#);

my $cr = join_with(' ; ',
		rec('CR','author')
	);

if ( $cr =~ m/SVERKO B/ ) {


csv('A',
	rec('PT'),
);

csv('B',
	join_with(', ',
		rec('AU')
	)
);

csv('C',
	rec('TI'),
);

csv('D',
	rec('DE'),
);

csv('E',
	rec('ID'),
);

csv('F',
	rec('TC'),
);

csv('G',
	rec('NR'),
);

csv('H',
	join_with(' | ',
		rec('CR','full')
	)
);

#csv('H',
#	rec('CR','author')
#);
#
#csv('I',
#	rec('CR','institution')
#);
#
#csv('J',
#	rec('CR','page')
#);
#
#csv('K',
#	rec('CR','reference')
#);
#
#csv('L',
#	rec('CR','volume')
#);
#
#csv('M',
#	rec('CR','year')
#);


csv('I',
	rec('BP'),
);

csv('J',
	rec('EP'),
);

csv('K',
	rec('DT'),
);

csv('L',
	rec('LA'),
);

csv('M',
	rec('SN'),
);

csv('N',
	rec('SO'),
);



}
