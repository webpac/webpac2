# Ova datoteka sadrži pravila za generiranje MARC21 ISO 2709 izlazne
# datoteke. Ulazni format je CDS/ISIS izvorni format, sa zapisima u
# UNIMARC formatu, sa vi¹estrukim odstupanjima od standarda.


## sve skupa se konvertira samo ako postoji polje 200^a
if ( rec('200','a') && rec('001') != '900101895' ) {

### LEADER

marc_leader('05',
	substr( rec('leader'), 5, 1 )
);

marc_leader('06',
	substr( rec('leader'), 6, 1 )  
);

marc_leader('07',
	substr( rec('leader'), 7, 1)
);

my $ldr17 = substr( rec('leader'), 17, 1 );
my $ldr17konv = {
	' ' => ' ',
	1 => 1,
	2 => 8,
	3 => 7
};
marc_leader('17', $ldr17konv->{$ldr17}) ;

my $ldr18 = substr( rec('leader'), 17, 1 );
my $ldr18konv = {
	' ' => 'i',
	'i' => 'i',
	'n' => ' ',
};
marc_leader('18', $ldr18konv->{$ldr18}) ;

marc('003','HR-ZaPFS');

my @a= (localtime) [5,4,3,2,1,0]; $a[0]+=1900; $a[1]++;
my $date = sprintf("%4d%02d%02d%02d%02d%04.1f",@a);

if ( rec('005') ) {
	marc('005',rec('005'));
} else {
	marc('005',$date);
}

if ( substr( rec('100','a'), 2, 6 ) ) {
	marc_fixed('008','00',
		substr( rec('100','a'), 2, 6)
	);
}

my $code06 = substr( rec('100','a'), 8, 1);
my $code06konv = {
	'a' => 'c',
	'b' => 'd',
	'c' => 'u',
	'd' => 's',
	'e' => 'r',
	'f' => 'q',
	'g' => 'm',
	'h' => 't',
	'i' => 'p',
	'j' => 'e',
	'|' => '|',
	' ' => '|'
};
marc_fixed('008','06', $code06konv->{$code06}) if $code06konv->{$code06};

my $codekonv = {
	'100 ' => '100u',     
	'18--' => '18uu',
	'19  ' => '19uu',
	'19__' => '19uu',
	'19--' => '19uu',
	'19##' => '19uu',
	'190 ' => '190u',
	'198 ' => '198u',
	'198-' => '198u',
	'198!' => '198u',
	'199-' => '199u',
	'20--' => '20uu',
	'20##' => '20uu',
	'200-' => '200u',
	'200.' => '200u',
	's.a.' => '    ',
	'4-7.' => '    ',
	'    ' => '    '
};
my $code07 = substr( rec('100','a'), 9, 4);
if ( $code07 =~ m/\d\d\d\d/ ) {
	marc_fixed('008','07',
		$code07
	);
} elsif ( $code07 =~ m/\s\s\s\s/ ) {
	if ( rec('210','c') =~ m/\d\d\d\d/ ) {
		marc_fixed('008','07',
			substr( rec('210','c'), 0, 4)
		);	
	}
} elsif ( $codekonv->{$code07} ) {
	marc_fixed('008','07',
		$codekonv->{$code07}
	);
}
					
my $code11 = substr( rec('100','a'), 13, 4);
if ( $code11 =~ m/\d\d\d\d/ ) {
	marc_fixed('008','11',
		$code11
	);
} elsif ( $codekonv->{$code11} ) {
	marc_fixed('008','11',
		$codekonv->{$code11}
	);
}

marc_fixed('008','15',
	frec('102','a')
);

if ( rec('110','a') ) {
	my $code18s = substr( rec('110','a'),1,1 );
	my $code18skonv = {
		'a' => 'd',
		'b' => 'c',
		'c' => 'w',
		'd' => 'e',
		'e' => 's',
		'f' => 'm',
		'g' => 'b',
		'h' => 'q',
		'i' => 't',
		'j' => 'f',
		'k' => 'a',
		'l' => 'g',
		'm' => 'h',
		'n' => 'i',
		'o' => 'j',
		'u' => 'u',
		'y' => ' ',
		'z' => 'z',
		'|' => '|',
		' ' => ' '
	};
	marc_fixed('008','18',$code18skonv->{$code18s}) if $code18skonv->{$code18s} ;
}

my $rec_105_a = rec('105','a');
my $mapping_105 = {
	'  000yy' => '        000yy',
	'  001yy' => '        001yy',
	'  100yy' => '        100yy',
	'a       000y' => 'a       000y ',
	'a  000yy' => 'a       000yy',
	'a  001yy' => 'a       001yy',
	'abk  000yy' => 'abk     000yy',
	'ad  000yy' => 'ad      000yy',
	'ad  001yy' => 'ad      001yy',
	'adf  001yy' => 'adf     001yy',
	'a  e  000yy' => 'a   e   000yy',
	'af' => 'af           ',
	'd  000yy' => 'd       000yy',
	'd  001yy' => 'd       001yy',
	'd  101yy' => 'd       101yy',
	'dc  000yd' => 'dc      000yd',
	'd  j  001yy' => 'd   j   001yy',
	'  e  000yy' => '    e   000yy',
	'   e    000yy' => '    e   000yy',
	'f  000yy' => 'f       000yy',
	'k  n  000yy' => 'k   n   000yy',
	'  n  000yy' => '    n   000yy',
};
if ( my $replace = $mapping_105->{$rec_105_a} ) {
	warn "# FIXED 105^a [$rec_105_a] -> [$replace]\n";
	$rec_105_a = $replace;
}


if ( $rec_105_a ) {
	my $code18 = substr( $rec_105_a, 0, 4);
	my $code18konv = {
		'    ' => '    ',
		'105 ' => '    ',
		'200 ' => '    ',
		'a   ' => 'a   ',
		'ab  ' => 'ab  ',
		'aB  ' => 'ab  ',
		'abd ' => 'abd ',
		'abf ' => 'abf ',
		'abk ' => 'abk ',
		'acd ' => 'acd ',
		'ad  ' => 'ad  ',
		'adb ' => 'adb ',
		'adf ' => 'adf ',
		'adh ' => 'adh ',
		'af' => 'af  ',
		'af  ' => 'af  ',
		'ah  ' => 'ah  ',
		'b   ' => 'b   ',
		'bd  ' => 'bd  ',
		'c   ' => 'c   ',
		'd   ' => 'd   ',
		'da  ' => 'da  ',
		'db  ' => 'db  ',
		'dc  ' => 'dc  ',
		'df  ' => 'df  ',
		'e   ' => 'e   ',
		'ed  ' => 'ed  ',
		'f   ' => 'f   ',
		'fk  ' => 'fk  ',
		'h   ' => 'h   ',
		'k   ' => 'k   ',
		't   ' => '    ',
		'y   ' => '    ',
		'Y   ' => '    '
	};
	if ( my $c = $code18konv->{$code18}) {
		marc_fixed('008','18', $c);
	} else {
		warn "missing [$code18]\n"
	}
}

if ( rec('110','a') ) {
	my $code19s = substr( rec('110','a'),2,1 );
	my $code19skonv = {
		'a' => 'r',
		'b' => 'n',
		'u' => 'u',
		'y' => 'x',
		'|' => '|',
	};
	marc_fixed('008','19',$code19skonv->{$code19s}) if $code19skonv->{$code19s};
}

if ( rec('110','a') ) {
	my $code21 = substr( rec('110','a'),0,1 );
	my $code21konv = {
		'a' => 'p',
		'b' => 'm',
		'c' => 'n',
		'z' => ' ',
		'|' => '|',
		' ' => '|'
	};
	marc_fixed('008','21',$code21konv->{$code21}) if $code21konv->{$code21};
}

my $code22 = substr( rec('100','a'), 17, 1);
my $code22konv = {
	'b' => 'a',
	'c' => 'b',
	'a' => 'j',
	'd' => 'c',
	'e' => 'd',
	'k' => 'e',
	'm' => 'g',
	'u' => ' ',
	'|' => '|',
	' ' => '|'
};
marc_fixed('008','22', $code22konv->{$code22}) if $code22konv->{$code22};

my $code23 = substr( rec('106','a'),0,1 );
marc_fixed('008','23', $code23);

if ( rec('110','a') ) {
	my $code24s = substr( rec('110','a'),3,1 );
	my $code24skonv = {
		'a' => 'b',
		'b' => 'c',
		'c' => 'i',
		'd' => 'a',
		'e' => 'd',
		'f' => 'e',
		'g' => 'r',
		'h' => 'y',
		'i' => 's',
		'j' => 'p',
		'k' => 'o',
		'l' => 'l',
		'm' => 'w',
		'n' => 'g',
		'o' => 'v',
		'p' => 'h',
		'r' => 'n',
		'z' => ' ',
		'|' => '|',
		' ' => '|'
	};
	marc_fixed('008','24',$code24skonv->{$code24s}) if $code24skonv->{$code24s};
}

if ( $rec_105_a ) {
	my $code24 = substr( $rec_105_a, 4, 4);
	my $code24konv = {
		'    ' => '    ',
		'0   ' => '    ',
		'1   ' => '    ',
		'a   ' => 'b   ',
		'ad  ' => 'ba  ',
		'af' => 'be  ',
		'b   ' => 'c   ',
		'bd  ' => 'ca  ',
		'c   ' => 'i   ',
		'd   ' => 'a   ',
		'df  ' => 'ae  ',
		'e   ' => 'd   ',
		'f   ' => 'e   ',
		'g   ' => 'r   ',
		'i   ' => 's   ',
		'j   ' => 'p   ',
		'm   ' => '    ',
		'n   ' => '1   ',
		'o   ' => '    ',
		'p   ' => 't   ',
		'u   ' => '    ',
		'v   ' => '    ',
		'va  ' => '    ',
		'y   ' => '    ',
		'z   ' => '    '
	};
	marc_fixed('008','24', $code24konv->{$code24}) if $code24konv->{$code24};
}

if ( rec('135','a') ) {
	my $code26 = substr( rec('135','a'), 0,1 );
	my $code26konv = {
		'a' => 'a',
		'b' => 'b',
		'c' => 'c',
		'd' => 'd',
		'u' => 'u',
		'v' => 'm',
		'z' => 'z'
	};
	marc_fixed('008','26', $code26konv->{$code26}) if $code26konv->{$code26};
}

my $code28 = substr( rec('100','a'), 20, 1);
my $code28konv = {
	'a' => 'f',
	'b' => 's',
	'c' => '1',
	'd' => '1',
	'e' => 'c',
	'f' => 'i',
	'g' => 'z',
	'h' => 'o',
	'y' => ' ',
	'z' => 'z',
	'|' => '|',
	' ' => ' '
};
marc_fixed('008','28', $code28konv->{$code28}) if $code28konv->{$code28};

if ( rec('110','a') ) {
	my $code29s = substr( rec('110','a'),7,1 );
	marc_fixed('008','29',$code29s);
}

if ( $rec_105_a ) {
	my $code29 = substr( $rec_105_a, 8, 1);
	my $code29konv = {
		'0' => '0',
		'1' => '1',
		'|' => '|',
		' ' => '|'
	};
	marc_fixed('008','29', $code29konv->{$code29}) if $code29konv->{$code29};
}

if ( $rec_105_a ) {
	my $code30 = substr( $rec_105_a, 9, 1);
	my $code30konv = {
		'0' => '0',
		'1' => '1',
		'|' => '|',
		' ' => '|'
	};
	marc_fixed('008','30', $code30konv->{$code30}) if $code30konv->{$code30};
}

if ( $rec_105_a ) {
	my $code31 = substr( $rec_105_a, 10, 1);
	my $code31konv = {
		'0' => '0',
		'1' => '1',
		'|' => '|',
		' ' => '|'
	};
	marc_fixed('008','31', $code31konv->{$code31}) if $code31konv->{$code31};
}

if ( substr( rec('leader'),7,1 ) =~ m/s/ ) {
	my $code33s = substr( rec('100','a'),34,1 );
	my $code33skonv = {
		'a' => 'a',
		'b' => 'b',
		'c' => 'c',
		'd' => 'd',
		'e' => 'e',
		'f' => 'f',
		'g' => 'g',
		'h' => 'h',
		'z' => 'z',
		' ' => ' '
	};
	marc_fixed('008','33', $code33skonv->{$code33s}) if $code33skonv->{$code33s};
}

if ( $rec_105_a ) {
	my $code33 = substr( $rec_105_a, 11, 1);
	my $code33konv = {
		'a' => '1',
		'b' => '0',
		'c' => '0',
		'd' => '0',
		'e' => '0',
		'f' => '1',
		'g' => '0',
		'h' => '0',
		'y' => '0',
		'z' => '0',
		'|' => '|',
		' ' => '|'
	};
	# warn "missing [$code33]" unless defined $code33konv->{$code33} ;
	marc_fixed('008','33', $code33konv->{$code33}) if $code33konv->{$code33};
}

if ( $rec_105_a ) {
	my $code34 = substr( $rec_105_a, 12, 1);
	my $code34konv = {
		'a' => 'a',
		'b' => 'b',
		'c' => 'c',
		'd' => 'd',
		'y' => ' ',
		'|' => '|',
		' ' => '|'
	};
	marc_fixed('008','34', $code34konv->{$code34}) if $code34konv->{$code34};
}

marc_fixed('008','35',
	frec('101','a')
);

my $code38 = substr( rec('100','a'), 25, 1);
if ( $code38 =~ m/[abc]/ ) {
	marc_fixed('008','38','o');
} else {
	marc_fixed('008','38',' ');
}

### 015
marc('015','2',
	rec('020','a')
);
marc('015','a',
	rec('020','b')
);

### 017
marc('017','a',
	rec('021','b')
);
marc('017','b',
	rec('021','a')
);


### 020
marc('020','a', 
	rec('010','a')
); 
marc('020','z',
	rec('010','z')
);

### 022
marc('022','a', 
	rec('011','a')
); 
marc('022','z',
	rec('011','z')
);

### 030
marc('030','a',
	rec('040','a')
);

#warn( 
#	dump(
#		frec('010')
#	),
#);

### 035$
## moguæ problem u pretra¾ivanju ako ima zagrade, kako bi trebalo po standardu

marc('035','a',
	join_with('',
		'(HR-ZaPFS)',
		rec('001')
		)
);

### 040

marc('040','a',
	'HR-ZaPFS'
);
marc('040','b',
	'hrv'
);
marc('040','c',
	'HR-ZaPFS'
);
marc('040','e',
	'ppiak'
);

### 041 - indikatori
if ( rec('101','i1') ) {
	my $i1_041 = rec('101','i1');
	warn "IND1: [$i1_041]";
	my $i1_041konv = {
		'0' => '0',
		'1' => '1',
		'2' => '1'
	};
	marc_indicators('041', $i1_041konv->{$i1_041}, ' ');
}
### 041
marc_original_order('041','101');

### 044
if ( my $f102 = rec('102') ) {
	warn "XXX ",dump( rec('000'), rec('102'), $f102 );
	if ( $f102 =~ m/^  \s*\w+/ ) {
		warn "ERROR: 044 IGNORED from 102 [$f102]\n";
	} else {
		marc_original_order('044','102');
	}
}



### 080

marc_indicators('080',' ',' ');

marc('080','a',
	rec('675','a')
);

marc('080','2',
	rec('675','v'),
);

### 084 - other classification (R)
marc('084','a',
	rec('686')
);

### 100 
marc_indicators('100','1',' ');
marc('100','a',
	rec('700', 'a')
);

### formalna odrednica - iz 503 u 110, i1=0, i2=1



### 111 i 110
## provjeriti za svaku bazu
## konverzija u 110 ili 111 ovisno o postojanju/nepostojanju nekih polja - provjeriti ispise naslova iz svake zbirke - moguæe su gre¹ke.
## popraviti interpunkciju

# ovo vrijedi za FFSFB
if ( rec('710','d') || rec('710','e') || rec('710','f') ) {

marc_indicators('111', 2, ' ');

marc_compose('111',
	'a', suffix(
		rec('710','b') ? '.' :
			'',
			rec('710','a'),
	),
	'e', rec('710','b'),
	'n', prefix(
		rec('710','d') ? '(' :
			'',
			rec('710','d')
	),
	'd', prefix(
		( ! rec('710','d') ) ? '(' :
		( ! rec('710','e') ) ? '(' :
			'; ',
			rec('710','f'),
	),
	'c', prefix(
		( rec('710','e') && ( rec('710','d') || rec('710','f') ) ) ? '; ' :
		( ! rec('710','d') && ! rec('710','f') )  ? '(' :
			'',
			rec('710','e'),
	),
	'+',')',
);
} else {
	marc_indicators('110', 2, ' ');
	marc('110','a',
		rec('710','a')
	);
	marc('110','b',
		rec('710','b')
	);
	if ( rec('710','c') ) {
		marc('110','c',
			surround('(', ')', rec('710','c'))
		);
	}
}

### 245 indikatori
## i1 = 0 za anonimne publikacije, i1 = 1 ako postoji 700 ili 710
## i2 = pretpostavlja se na temelju èlana na poèetku naslova i jezika

marc_indicators('245', rec('200','i1'), 0);

if ( rec('200','a') =~ m/^Einen / ) {
	marc_indicators('245', rec('200','i1'), 6);
} 
if ( rec('200','a') =~ m/^Eine / ) {
	marc_indicators('245', rec('200','i1'), 5);
} 
if ( rec('200','a') =~ m/^(Die|Das|Der|Ein|Les|Los|The) / ) {
	marc_indicators('245', rec('200','i1'), 4);
} 
if ( rec('200','a') =~ m/^(Um|Un|An|La|Le|Lo|Il) / ) { 
	marc_indicators('245', rec('200','i1'), 3);
} 
if ( ( rec('101') =~ m/ENG/ ) && ( rec('200','a') =~ m/^A / ) ) { 
	marc_indicators('245', rec('200','i1'), 2);
}
if ( rec('200','a') =~ m/^L / ) { 
	marc_indicators('245', rec('200','i1'), 2);
} 
if ( rec('200','a') =~ m/^L'/ ) { 
	marc_indicators('245', rec('200','i1'), 2);
} 


### 245
## postoji modify
## da li treba makivati razmake u inicijalima?

#_debug(3);

if ( rec('461','1') ) {
	marc('245','a',
		lookup(
			sub { rec('200','a') },
			'pravo','bib',
			sub { prefix('001', rec('001') ) },
			sub { rec('461','1') }
		),
	);
	marc('245','b',
		lookup(
			sub { rec('200','e') },
			'pravo','bib',
			sub { prefix('001', rec('001') ) },
			sub { rec('461','1') }
		),
	);
	marc('245','c',
		lookup(
			sub { rec('200','f') },
			'pravo','bib',
			sub { prefix('001', rec('001') ) },
			sub { rec('461','1') }
		),
	);
	marc('245','n',
		rec('461','a'),
	);
	marc('245','p',
		rec('200','a'),
	);
} else {	

marc_compose('245',
	'a', suffix(
		( ! ( rec('200','d') || rec('200','e') || rec('200','k') || rec('200','h') ) ) && ( rec('200','f') ) ? ' /' :  
		( rec('200','d') ) ? ' =' :
		( rec('200','e') ) ? ' :' :
		( rec('200','k') ) ? ' ;' :
		( rec('200','k') ) ? ' ;' :
			'.', 
			join_with(' ; ', rec('200','a') ),
	),
	'b', suffix(
		( rec('200','d') && rec('200','f') ) ? ' /' : 
		( rec('200','d') && rec('200','c') ) ? '.'  : 
			'',
			rec('200','d'),
	),
	'b', suffix(
		( rec('200','e') && rec('200','f') ) ? ' /' : 
		( rec('200','e') && rec('200','c') ) ? '.'  : 
			'',
			rec('200','e'),
	),
	'b', suffix(
		( rec('200','k') && rec('200','f') ) ? ' /' : 
		( rec('200','k') && rec('200','c') ) ? '.'  : 
			'',
			rec('200','k'),
	),
	'n', suffix(
		( rec('200','h') && rec('200','f') ) ? ' / ' :
			'',
			rec('200','h'),
	),
	'c', suffix(
		( rec('200','f') && rec('200','c') ) ? '. ' :
			'',
			join_with(' ; ',
				regex('s/(\S\.)\s(\S\.\s)/$1$2/g',
					rec('200','f'),
				),
				regex('s/(\S\.)\s(\S\.\s)/$1$2/g',
					rec('200','g')
				)
			)
	),
	## append to last subfield
	'+', suffix('.',
		join_with(' / ',
			rec('200','c'),
			rec('200','x')
		)
	),
);
}

#_debug(0);

### 246
## i1=1 ukoliko pravilo nala¾e napomenu, ali napomenu necemo pisati ponovo u 500
## i1=3 ako pravlo na nala¾e napomenu
## vidi na wiki
## i2 - pogledati za svaku bazu sto su ti naslovi, pa onda oderditi indikatoda oderditi indikatoree
marc_indicators('246', 3, ' ');
marc('246','a',
	rec('532')
);

### 250
## zapisima koji nemaju potpolje, dodaje se ^a u modify - provjeriti za svaku bazu
marc('250','a',
	suffix(
		rec('205','f') ? ' / ' : 
			'',
			rec('205','a') 
	),
);
marc('250','b',
	rec('205','f'),
);

### 260
## ponovljiva potpolja u originalnom redosljedu - priprema u modify
marc_original_order('260', '210');


### 300
## urediti interpunkcije za sve kombinacije
marc('300','a',
	suffix(
		rec('215','c') ? ' : ' :
		rec('215','d') ? ' ; ' :
		rec('215','e') ? ' + ' :
			'',
			rec('215','a')
	)
);
marc('300','b',
	suffix(
		( rec('215','c') && rec('215','d') ) ? ' ; ' :
		( rec('215','c') && rec('215','e') ) ? ' + ' :
			'',
			rec('215','c')
	)
);
marc('300','c',
	suffix(
		rec('215','e') ? ' + ' :
			'',
			rec('215','d')
	)
);
marc('300','e',
	rec('215','e')
);

marc_indicators('326', rec('207','i2'), ' ');
marc('326','a',
	rec('207','a')
);
marc('326','z',
	rec('207','z')
);


### 490

if ( rec('225','i1') =~ m/0/ || rec('225','i1') =~ m/2/ ) {
	marc_indicators('490','1',' ');
} elsif ( rec('225','i1') =~ m/1/ ) {
	marc_indicators('490','2',' ');
}

marc_original_order('490','225');
	
### 500

marc('500','a',
	rec('300')
);

marc('500','a',
	rec('305')
);

marc('500','a',
	rec('307')
);

marc('500','a',
	rec('314')
);

### 502 - bilje¹ka o disertacijama
## odgovarajuæeg polja nema u originalnim podacima. mo¾da se mo¾e pretpostaviti?

### 504

marc('504','a',
	regex('s/\s*[\\r\\n]+\s*\**\s*/ ; /g',
		rec('320')
	)
);

### 505
## NSK konvertira 327 (NP) u 501 (R)
## u LOC konv. tablici stoji 327 (NP) u 505 (NR)
## standard i validacije dopustaju 501 (R) i 505 (R)
## za svaku bazu posebno provjeriti sta je u poljima 327 i 330
if ( config() =~ m/ffps|fftu/ ) {
	marc_indicators('505', '0', ' ');
	marc('505','a',
		regex('s/\s*[\\r\\n]+\s*\**\s*/ ; /g',
			rec('327')
		)	
);
}

if ( config() =~ m/ffan/ ) {
	marc_indicators('505', '0', ' ');
	marc('505','a',
		regex('s/[<>]//g',
			rec('330')
		)	
);
}

### 520
## NSK konvertira 330 (P) u 520 (P) = LOC konv. tablica
if ( config() =~ m/ffps/ ) { 
	marc('520','a',
		regex('s/[\\r\\n]+/. /g',
			rec('330')
		)	
	);
}

### 526 - STUDY PROGRAM INFORMATION NOTE
## u nekim bazame je to u 996 - provjeriti za svaku bazu sta je u 996?
marc_indicators('526', 8, ' ');
marc('526','a',
	rec('996')
);

### 538 - zahtjevi sustava
marc('538','a',
 	regex('s/\s*[\\r\\n]+\s*\**\s*/ ; /g',
		rec('337','a')
	)
);

### 653 - Index Term-Uncontrolled
## da li ponovljivo polje ili potpolje, pogledati u Koha tags
marc_indicators('653', ' ', ' ');
marc('653','a',
	rec('610')
);

### 655 - Index Term-Genre/Form 
marc_indicators('655', ' ', 4);
marc('655','a',
	rec('608')
);

#_debug(2);


### 700
## ako je u originalu ponovljivo polje treba bioti i konvertirano u ponovljivo !!
## ako ima samo podpolje a onda je pseudonim - nakon konverzije treba provjeriti
if ( rec('701','a') =~ m/, / ) {
	marc_indicators('700', 1, ' ');
} else {
	marc_indicators('700', 0, ' ');
}

marc('700','a',
	rec('701', 'a'),
);

marc('700','a',
	join_with(', ',
		rec('701', 'c'),
		rec('701', 'd')
	),
);
marc('700','a',
	join_with(', ',
		rec('701', 'e'),
		rec('701', 'f')
	),
);
marc('700','a',
	join_with(', ',
		rec('701', 'g'),
		rec('701', 'h')
	),
);
marc('700','a',
	rec('702','a'),
);
marc('700','a',
	join_with(', ',
		rec('702','c'),
		rec('702','d')
	)
);
marc('700','a',
	join_with(', ',
		rec('702','e'),
		rec('702','f')
	)
);
marc('700','a',
	join_with(', ',
		rec('702','g'),
		rec('702','h')
	)
);

#_debug(0);

## eventualno nadopuniti 710 i 711 - provjetiti da li u podacima postoji u ISIS-u


### 740 - Added Entry - Uncontrolled Related/Analytical Title 
## raspraviti
## nadopuniti prvi indikator prema èlanovima
marc_indicators('740', '0', ' ');

# if ( ! rec('464') ) {
# marc('740','a',
# 	rec('200','c')
# );

marc('740','a',
	rec('200','k')
);

# } else {

#marc('740','a',
#	rec('464','a')
#);

# };

### 760 w ---------------------------------------------
## generiranje zapisa o nakladnièkoj cjelini i povezivanje podreðenog zapisa s nadreðenim

#if (
#	rec('225','a')	# da li je nakladnièka cjelina?
#) {

#my $series_key =
#join_with('',
#	rec('225','a'),
#	rec('210','a'),
#	rec('210','b'),
#);
#
#my $series = get( $series_key );
#
#if ($series) {
#	warn "nije novi";
#} else {
#
#$series = join_with('',
#	uc( config() ),
#	' LS',
#	rec('000')
#);
#
#set( $series_key => $series );
#
#} # $series
#} # nakladnièka cjelina
#
#my $series_key = join_with('',
#	rec('225','a'),
#	rec('210','a'),
#	rec('210','b'),
#);
#
#if ($series_key) {
#
#	marc_indicators('760', 0, ' ');
#	
#	marc('760','t',
#		rec('225','a')
#	);
#	marc('760','w',
#		get( $series_key )
#	);
#}

# ## testing
#
# my $dup_key = join_with('',
#	rec('200','a'),
#);
#
#if ($dup_key) {
#	marc('995','a',
#		get( $dup_key )
#	);
#}
#
#
# marc_indicators('776', 0, ' ');
# 
# marc('776','a',
#	rec('452','1')
# );
#
# -------------------------------------------------------------------

### 773 

if ( rec('463') ) {

my $id463 = substr( rec('463','1'), 3, 9 ) ;

	marc('773','g',
		rec('463','v'),
	);
	marc('773','t',
		lookup(
			sub { rec('200','a') },
			'pravo','bib',
			sub { prefix('001', rec('001') ) },
			sub { rec('463','1') }
		),
	);
	marc('773','w',
		prefix('(HR-ZaPFS)', $id463 ),
	);
}


### 774

if ( rec('461') ) {

my $id461 = substr( rec('461','1'), 3, 9 ) ;

	marc('774','t',
		lookup(
			sub { rec('200','a') },
			'pravo','bib',
			sub { prefix('001', rec('001') ) },
			sub { rec('461','1') }
		)
	);
		
	marc('774','w',
		prefix('(HR-ZaPFS)', $id461 )
	);
}
	


### 774 - Constituent Item Entry - to bi trebali koristiti ali ne koristimo jer NSK to koristi za ne¹to drugo
### 787 - Nonspecific Relationship Entry 

#marc_indicators('787','0',' ');
#
#marc_compose('787',
#	'g', rec('230','v'),
#	't', rec('230','a'),
#	'b', rec('240','a'),
#	'd', rec('250','a'),
#	'd', rec('250','d'),
#	'h', rec('260','a'),
#	'k', rec('270','a'),
#	'n', join_with('. - ',
#		rec('280'),
#	),
#	'x', join_with(', ',
#		rec('290'),
#	)
#);
#marc_compose('787',
#	'g', rec('231','v'),
#	't', rec('231','a'),
#	'b', rec('241','a'),
#	'd', rec('251','a'),
#	'd', rec('251','d'),
#	'h', rec('261','a'),
#	'k', rec('271','a'),
#	'n', join_with('. - ',
#		rec('281'),
#	),
#	'x', join_with(', ',
#		rec('291'),
#	)
#);
#marc_compose('787',
#	'g', rec('232','v'),
#	't', rec('232','a'),
#	'b', rec('242','a'),
#	'd', rec('252','a'),
#	'd', rec('252','d'),
#	'h', rec('262','a'),
#	'k', rec('272','a'),
#	'n', join_with('. - ',
#		rec('282'),
#	),
#	'x', join_with(', ',
#		rec('292'),
#	)
#);
#marc_compose('787',
#	'g', rec('233','v'),
#	't', rec('233','a'),
#	'b', rec('243','a'),
#	'd', rec('253','a'),
#	'h', rec('263','a'),
#	'k', rec('273','a'),
#	'n', join_with('. - ',
#		rec('283'),
#	),
#	'x', join_with(', ',
#		rec('293'),
#	)
#);


### 760
if ( rec('410','i2') =~ m/0/ ) {
	marc_indicators('760',' ','1');
} elsif ( rec('410','i2') =~ m/1/ ) {
	marc_indicators('760',' ','0');
}

marc('760','a',
	lookup(
		sub { rec('200','a') },
		'pravo','bib',
		sub { prefix('001', rec('001') ) },
		sub { rec('410','1') }
	)
);

marc('760','w',
	lookup(
		sub { '(HR-ZaPFS)' . rec('001') },
		'pravo','bib',
		sub { prefix('001', rec('001') ) },
		sub { rec('410','1') }
	)
);





### 852 - ne koristimo, koristimo  942 i 952

### 876 - item information - basic bibliographic unit (R)
## da li ovdje zapisati stare inventarne brojeve?
#marc('876','a',
#	rec('991','t'),
#);

### 886 - former marc
# inicijale treba prebaciti u neko lokalno polje, a ovo polje nam ne treba
#
# marc_indicators('886', 2, ' ');
# marc('886','2',
#	'ffmarc'
# );
# marc('886','a',
#	'994'
# );
# marc('886','b',
#	join_with(''.
#		'##^a',
#		join_with('',
#			prefix('^a',
#				rec('994','a'),
#			),
#			prefix('^b',
#				rec('994','b')
#			)
#		)
#	)
#);

marc('866','a',
 	regex('s/\s*[\\r\\n]+\s*\**\s*/ ; /g',
		rec('999','a')
	)
);


### KOHA 942

if ( rec('leader') =~ m/m/ ) {
	marc('942','b', 'LIB');
        marc('942','c','K');

} else {
	marc('942','b', 'PER');
        marc('942','c','S');
}       

marc('942','d',
	join_with(' | ',
		rec('990')
	)
);	

if ( rec('700','a') ) {
	if ( rec('200','a') =~ m/^(The|Die|Das|Der|Ein|Les) / ) {
		marc('942','i',
			join_with(' ',
				regex('s/(^.{3}).*/$1/',
					uc ( rec('700','a') )
				),
				regex('s/^.{4}(.{1}).*/$1/',
					lc ( rec('200','a') )
				)
			)
		);
	} elsif ( rec('200','a') =~ m/^(Um|Un|An|La|Le|Il) / ) {
		marc('942','i',
			join_with(' ',
				regex('s/(^.{3}).*/$1/',
					uc ( rec('700','a') )
				),
				regex('s/^.{3}(.{1}).*/$1/',
					lc ( rec('200','a') )
				)
			)
		);
	} elsif ( rec('200','a') =~ m/^Eine / ) {
		marc('942','i',
			join_with(' ',
				regex('s/(^.{3}).*/$1/',
					uc ( rec('700','a') )
				),
				regex('s/^.{5}(.{1}).*/$1/',
					lc ( rec('200','a') )
				)
			)
		);
	} elsif ( rec('200','a') =~ m/^Einen / ) {
		marc('942','i',
			join_with(' ',
				regex('s/(^.{3}).*/$1/',
					uc ( rec('700','a') )
				),
				regex('s/^.{6}(.{1}).*/$1/',
					lc ( rec('200','a') )
				)
			)
		);
	} elsif ( rec('200','a') =~ m/^(A|L) / ) {
		marc('942','i',
			join_with(' ',
				regex('s/(^.{3}).*/$1/',
					uc ( rec('700','a') )
				),
				regex('s/^.{2}(.{1}).*/$1/',
					lc ( rec('200','a') )
				)
			)
		);
	} else {
		marc('942','i',
			join_with(' ',
				regex('s/(^.{3}).*/$1/',
					uc ( rec('700','a') )
				),
				regex('s/(^.{1}).*/$1/',
					lc ( rec('200','a') )
				)
			)
		);
	}
} elsif ( rec('710','a') ) {
	if ( rec('200','a') =~ m/^(The|Die|Das|Der|Ein|Les) / ) {
		marc('942','i',
			join_with(' ',
				regex('s/(^.{3}).*/$1/',
					uc ( rec('710','a') )
				),
				regex('s/^.{4}(.{1}).*/$1/',
					lc ( rec('200','a') )
				)
			)
		);
	} elsif ( rec('200','a') =~ m/^(Um|Un|An|La|Le|Il) / ) {
		marc('942','i',
			join_with(' ',
				regex('s/(^.{3}).*/$1/',
					uc ( rec('710','a') )
				),
				regex('s/^.{3}(.{1}).*/$1/',
					lc ( rec('200','a') )
				)
			)
		);
	} elsif ( rec('200','a') =~ m/^Eine / ) {
		marc('942','i',
			join_with(' ',
				regex('s/(^.{3}).*/$1/',
					uc ( rec('710','a') )
				),
				regex('s/^.{5}(.{1}).*/$1/',
					lc ( rec('200','a') )
				)
			)
		);
	} elsif ( rec('200','a') =~ m/^Einen / ) {
		marc('942','i',
			join_with(' ',
				regex('s/(^.{3}).*/$1/',
					uc ( rec('710','a') )
				),
				regex('s/^.{6}(.{1}).*/$1/',
					lc ( rec('200','a') )
				)
			)
		);
	} elsif ( rec('200','a') =~ m/^(A|L) / ) {
		marc('942','i',
			join_with(' ',
				regex('s/(^.{3}).*/$1/',
					uc ( rec('710','a') )
				),
				regex('s/^.{2}(.{1}).*/$1/',
					lc ( rec('200','a') )
				)
			)
		);
	} else {
		marc('942','i',
			join_with(' ',
				regex('s/(^.{3}).*/$1/',
					uc ( rec('710','a') )
				),
				regex('s/(^.{1}).*/$1/',
					lc ( rec('200','a') )
				)
			)
		);
	}
} elsif ( rec('200','a') =~ m/^(The|Die|Das|Der|Ein|Les) / ) {
	marc('942','i',
		regex('s/^.{4}(.{3}).*/$1/',
			uc ( rec('200','a') )
		),
	);
		
} else {
	marc('942','i',
		regex('s/(^\w{3}).*/$1/',
			uc ( rec('200','a') )
		)
	)
}


### Koha podaci o primjercima

#marc('952','3',
#	join_with(' | ' ,
#		rec('991','a'),
#		rec('991','b')
#	)
#);
#
#marc('952','h',
#	join_with(' |', 
#		rec('990') 
#	)
#);
#
#if ( rec('leader') =~ m/m/ ) {
#	marc('952','y','K');
#} else {
#	marc('952','y','S');
#}

# lokalna polja (stari inv. broj, 992 i 023)

marc('991','a',
	join_with(' | ',
		rec('991','a'),
		rec('991','b')
	)
);

marc('992','a',
	join_with(' | ',
		rec('992','a'),
		rec('992','b')
	)
);

marc('993','a',
	join_with(' | ',
		rec('023','a'),
		rec('023','b')
	)
);

marc('994','a',
	rec('801','b'),
);

} # polje 200a
