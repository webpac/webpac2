=pod

=head1 NAME

WebPAC lookup file definition

=cut

@lookup = (
	{ 'key' => 'd:v900', 'val' => 'filter{CROVOC_tree}v250^a v800' },
#	{ 'eval' => '"v901^a" eq "Podru�je"', 'key' => 'pa:v561^4:v562^4:v461^1', 'val' => 'v900' },
#	{ 'eval '=> '"v901^a" eq "Mikrotezaurus"', 'key' => 'a:v561^4:v562^4:v461^1', 'val' => 'v900' },
#	{ 'eval' => '"v901^a" eq "Deskriptor"', 'key' => 'a:v561^4:v562^4:v461^1', 'val' => 'v900' },
	{ 'key' => 'a:v561^4:v562^4:v461^1', 'val' => 'v900' },
	{ 'key' => '900_mfn:v900', 'val' => 'v000' },
	# tree structure
	{ 'eval' => 'length("v251") == 2 && "v800" =~ m/EUROVOC/ || "v800" =~ m/CROVOC/ && "v251" =~ m/^(H|HD|L|Z|P)$/', 'key' => 'root:v251', 'val' => 'v900' },
	{ 'eval' => '"v251"', 'key' => 'code:v900', 'val' => 'v561^4:v251' },
	{ 'eval' => '"v561^4" && "v562^4"', 'key' => 'code:v900', 'val' => 'v561^4:v562^4' },
	{ 'key' => 'crovoc:v900', 'val' => 'filter{CROVOC}v800' },
);
