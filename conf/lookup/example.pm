=pod

=head1 NAME

WebPAC lookup file definition example

Required fields are C<key> and C<val>. Optional field C<eval> is perl code
to evaluate before storing value in lookup.

=cut

@lookup = (
	{ 'key' => '900:v900', 'val' => 'filter{upper}v800' },
	{ 'eval' => qq{length("v900") == 3},
		'key' => '900:v900', 'val' => 'v800'
	},
	{ 'key' => '800:v800', 'val' => 'v900' },
	{ 'key' => '000:v000', 'val' => 'v000' },
);
