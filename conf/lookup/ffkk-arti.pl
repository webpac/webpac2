
marc_indicators('777', 0, ' ');

marc('777','w',
	lookup(
		sub { 'ffkk/peri/' . rec('000') },
		'ffkk','peri',
		sub { rec('11') }
	) ||
	lookup(
		sub { 'ffkk/peri/' . rec('000') },
		'ffkk','peri',
		sub { first(rec(200,'a')) . ' ' . first(rec('200','e')) },
	) ||

	lookup(
		sub { "ffkk/peri/" . rec('000') },
		'ffkk','peri',
		sub { rec(200,'a') },
#		sub { rec(900,'x') },
	)

);

