tag('biblionumber',		rec('999','c'));
tag('035',			rec('035','a'));
tag('isbn',			rec('020','a'));
tag('issn',			rec('022','a'));
tag('autor',			rec('100','a'));
tag('naslov',			rec('245','a'));
tag('naslov_ostatak', 		rec('245','b'));
tag('naslov_podredjeni_oznaka', rec('245','n'));
tag('naslov_podredjeni',	rec('245','p'));
tag('odgovornost',		rec('245','c'));
tag('godina',			rec('260','c'));
tag('brojcani',			rec('362','a'));
tag('napomena_ocjenski',	rec('502'));
tag('URL',			rec('856','u'));
tag('fond',			
			join_with(' ; ',
				rec('866','a')
			)
);
tag('signatura_stara',		rec('942','d'));
tag('signatura_predlozena',
			join_with(' ',
				rec('942','h'),
				rec('942','i')
			)
);
tag('frameworkcode',		rec('942','b'));
tag('vrsta_gradje',		rec('942','c'));
tag('serial_flag',		rec('942','s'));
tag('zbirka',			rec('952','8'));
tag('vrsta_primjerka',		rec('952','y'));
tag('enumeration',		rec('952','h'));
tag('signatura',		rec('952','o'));
tag('inventarni',		rec('952','t'));

