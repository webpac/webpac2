row('rbaze',
	baza => config('input name'),
	coverage_policy => rec('A'),
	source_type => rec('B'),
	isn => rec('C'),
	title => rec('D'),
	publisher => rec('E'),
	coverage_start => rec('F'),
	coverage_stop => rec('G'),
	peer_reviewed => rec('H'),
	cited_refs_start => rec('I'),
	cited_refs_stop => rec('J'),
	availability => rec('K')
);
