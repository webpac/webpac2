marc_clone();

marc_remove('100');

marc('100','a',
	rec('100','a')
);

marc_repeatable_subfield('100','c',
	rec('100','c'),
	lookup(
		sub { rec('B') },
		'bibliografija','zaposleni',
		sub { rec('D') },
		sub { 
			regex('s/,$//',
				rec('100','a')
			)
		}
	)
);
marc_remove('680','a');

if ( rec('680','a') =~ m/Filozofski fakultet u Zagrebu/ ) {
	marc('100','g',
		rec('680','a')
	)
}

marc_remove('000');
