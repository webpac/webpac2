my $covpolicy;
my $covstatus;

( $covpolicy, $covstatus ) = split(/\s*-\s*/, rec('F'));

my @isn = regex('s/^.*(\d\d\d\w)(\d\d\d\w).*/$1-$2/', rec('B'));

row('rbaze',
	baza => config('input name'),
	isn => $isn[0],
	eissn => rec('C'),
	title => rec('A'),
	coverage_policy => $covpolicy,
	coverage_status => $covstatus,
	publisher => rec('D'),
	country => rec('E')
);
