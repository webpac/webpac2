my $isn;
my $eissn = '';

( $isn, $eissn ) = regex('s/\s*$//', split(/\s*;\s*/, rec('B')));

row('rbaze',
	baza => config('input name'),
	isn => $isn,
	eissn => $eissn,
	title => rec('A')
);
