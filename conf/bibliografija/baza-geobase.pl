my @isn = regex('s/^.*(\d\d\d\w)(\d\d\d\w).*/$1-$2/', rec('C'));

row('rbaze',
	baza => config('input name'),
	title => rec('A'),
	source_type => rec('B'),
	isn => $isn[0],
	eissn => rec('D'),
	publisher => rec('E'),
	country => rec('F'),
	coverage_policy => rec('G'),
	subject => join_with(' -- ',
			rec('H'),
			rec('I'),
			rec('J'),
			rec('K'),
			rec('L'),
			rec('M'),
			rec('N')
		)
);
