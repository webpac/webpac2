if ( rec('942','u') ) {

marc_clone();

marc_remove('008');

marc_fixed('008','00',
        substr( rec('008'), 0, 14 )
);

my $zemlja = 
		lookup(
			sub { rec('A') },
			'rbaze','zemlja',
			sub { rec('C') },
			sub { rec('999','c') }
		);
if ( $zemlja ) {
	marc_fixed('008','15',$zemlja);
}

marc_fixed('008', '18',
        substr( rec('008'), 18, 22 )
);

marc_remove('773');

marc('773','3', rec('773','3') );
marc('773','6', rec('773','6') );
marc('773','7', rec('773','7') );
marc('773','8', rec('773','8') );
marc('773','9', rec('773','9') );
marc('773','a', rec('773','a') );
marc('773','b', rec('773','b') );
marc('773','d', rec('773','d') );
marc('773','g', rec('773','g') );
marc('773','h', rec('773','h') );
marc('773','i', rec('773','i') );
marc('773','k', rec('773','k') );
marc('773','m', rec('773','m') );
marc('773','n', rec('773','n') );
marc('773','o', rec('773','o') );
marc('773','p', rec('773','p') );
marc('773','q', rec('773','q') );
marc('773','r', rec('773','r') );
marc('773','s', rec('773','s') );
marc('773','t', rec('773','t') );
marc('773','u', rec('773','u') );
marc('773','w', rec('773','w') );

my $issn = 
	lookup(
		sub { rec('B') },
		'rbaze','zemlja',
		sub { rec('C') },
		sub { rec('999','c') }
	);
if ( $issn ) {
	marc('773','x', $issn );
}

marc('773','y', rec('773','y') );
marc('773','z', rec('773','z') );

if ( rec('999','c') =~ m/305996/ ) {
	marc_remove('520');
}

marc_remove('942');

marc('942','a',
	rec('942','a')
);
marc('942','b',
	rec('942','b')
);
marc('942','c',
	rec('942','c')
);
marc('942','d',
	rec('942','d')
);
marc('942','e',
	rec('942','e')
);
marc('942','f',
	rec('942','f')
);
marc('942','g',
	rec('942','g')
);
marc('942','h',
	rec('942','h')
);
marc('942','i',
	rec('942','i')
);
marc('942','j',
	rec('942','j')
);
marc('942','k',
	rec('942','k')
);
marc('942','l',
	rec('942','l')
);
marc('942','m',
	rec('942','m')
);
marc('942','n',
	rec('942','n')
);

if ( $issn  ) {
	marc_repeatable_subfield('942','r',
		lookup(
			sub{ 
				join_with(' ; ',
					rec('B'),
					rec('N'),
					rec('O'),
					join_with('-',
						rec('P'),
						rec('R'),
					),
				)
			},
			'rbaze','rbaze',
			sub{ regex('s/(-|\s+)//g', rec('C')) },
			sub{ regex('s/(-|\s+)//g', $issn ) }
		)
	);
}
if ( $issn ) {
	marc_repeatable_subfield('942','r',
		lookup(
			sub{ 
				join_with(' ; ',
					rec('B'),
					rec('N'),
					rec('O'),
					join_with('-',
						rec('P'),
						rec('R'),
					),
				)
			},
			'rbaze','rbaze',
			sub{ regex('s/(-|\s+)//g', rec('D')) },
			sub{ regex('s/(-|\s+)//g', $issn ) }
		)
	);
}

marc('942','s',
	rec('942','s')
);
marc('942','t',
	rec('942','t')
);
marc('942','u',
	rec('942','u')
);
marc('942','v',
	rec('942','v')
);
marc('942','x',
	rec('942','x')
);
marc('942','y',
	rec('942','y')
);
marc('942','z',
	rec('942','z')
);

}
