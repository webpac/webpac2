row('rbaze',
	baza => config('input name'),
	source_type => rec('A'),
	isn => rec('B'),
	title => rec('C'),
	publisher => rec('D'),
	ft_start => rec('E'),
	ft_stop => rec('F'),
	ft_delay => rec('G'),
	peer_reviewed => rec('H'),
	pdf_images => rec('I'),
	image_quick_view => rec('J'),
	availability => rec('K')
);
