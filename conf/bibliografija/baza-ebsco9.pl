row('rbaze',
	baza => config('input name'),
	source_type => rec('A'),
	isn => rec('B'),
	title => rec('C'),
	publisher => rec('D'),
	coverage_start => rec('E'),
	coverage_stop => rec('F'),
	ft_start => rec('G'),
	ft_stop => rec('H'),
	ft_delay => rec('I'),
	pdf_images => rec('J'),
	image_quick_view => rec('K'),
	availability => rec('L')
);
