row('rbaze',
	baza => config('input name'),
	coverage_policy => rec('A'),
	source_type => rec('B'),
	isn => rec('C'),
	title => rec('D'),
	publisher => rec('E'),
	coverage_start => rec('F'),
	coverage_stop => rec('G'),
	ft_start => rec('H'),
	ft_stop => rec('I'),
	ft_delay => rec('J'),
	peer_reviewed => rec('K'),
	pdf_images => rec('L'),
	image_quick_view => rec('M'),
	cited_refs_stop => rec('O'),
	cited_refs_start => rec('N'),
	availability => rec('P')
);
