sorted('country',
	join_with(' | ',
		rec('001'),
		join_with(' ; ',
			rec('101','a')
		)
	)
);

sorted('title',
	join_with(' | ',
		rec('200','a')
	)
);

sorted('orig_county',
	join_with(' | ',
		rec('801','a')
	)
);

sorted('orig_agency',
	join_with(' | ',
		rec('801','b')
	)
);

sorted('orig_crules',
	join_with(' | ',
		rec('801','g')
	)
);

sorted('callnum',
	join_with(' | ',
		rec('990','a')
	)
);

sorted('nacin_nabave)',
	join_with(' | ',
		rec('991','a')
	)
);

sorted('invbr',
	join_with(' | ',
		rec('991','b')
	)
);

sorted('invbr_lokacija',
	join_with(' | ',
		rec('001'),
		join_with(' ; ',
			rec('991','b'),
		),
		join_with(' ; ',
			rec('801','b')
		)
	)
);


