sub csv { to('csv',@_) }

csv('A',
	rec('001')
);

csv('B',
	rec('200','a')
);

csv('C',
	join_with(' --- ',
		rec('801','a')
	)
);

csv('D',
	join_with(' --- ',
		rec('801','b')
	)
);

csv('E',
	join_with(' --- ',
		rec('801','g')
	)
);

csv('F',
	rec('990','a')
);

csv('G',
	rec('991','a')
);

csv('H',
	rec('991','b')
);

csv('I',
	join_with(' --- ',
		rec('992','a')
	)
);

csv('J',
	join_with(' --- ',
		rec('210','d')
	)
);

csv('K',
	rec('101','a')
);

