# Ova datoteka sadrzi pravila za generiranje MARC21 ISO 2709 izlazne
# datoteke. Ulazni format je CDS/ISIS izvorni format, sa zapisima u
# UNIMARC formatu, koji sadrzi visestruka odstupanja od standarda.

## konvertira se ako postoji polje 200^a
if ( rec('200','a') ) {

### LEADER

## LDR 05 - n - new
marc_leader('05','n');

## LDR 06 - a - language material 
marc_leader('06','a');

## LDR 07 - m - Monograph/item
marc_leader('07','m');

## LDR 17 - Encoding level ; 7 - minimal level, u - unknown
marc_leader('17','u');

## LDR 18 - i - isbd 
marc_leader('18','i');

## LDR 19 - Multipart resource record level ; # - Not specified or not applicable, a - Set, b - Part with independent title, c - Part with dependent title 

### 003
marc_fixed('003','00','HR-ZaHPM');

### 008 - All materials
## dodati sve moguce slucajeve za datum, popuniti ono sto nedostaje

if ( rec('100') ) {
	marc_fixed('008','00',
		regex('s/^\d\d//',
			rec('100')
		),
	);
	marc_fixed('008','02',
		regex('s/^\d\d\d\d//',
			rec('100')
		),
	);
} else {
	marc_fixed('008','00',
		'000000'
	);
}


## 008 06 - Type of date/Publication status

marc_fixed('008','06',
	's'
);

## 008 07-10 - Date 1
## 008 11-14 - Date 2 
## 210d kroz modify postaje 210c

if ( rec('210','c') ) {
	my $d1 = '';
	my $d2 = '    ';
	if ( rec('210','c') =~ m/(\d{4})-/ ) {
		marc_fixed('008','06','m');
		if ( rec('210','c') =~ m/(\d{4})-/ ) {
			$d1 = $1;
			$d2 = '9999';
		}
		if ( rec('210','c') =~ m/-(\d{4})/ ) {
			$d2 = $1;
		}
	} else {
		marc_fixed('008','06','s');
		if ( rec('210','c') =~ m/(\d{4})/ ) {
			$d1 = $1;
		}
		if ( rec('210','c') =~ m/(\d{2})(--|__)/ ) {
			$d1 = $1.'uu';
		} 
		if ( rec('210','c') =~ m/(\d{3})(-|_)/ ) {
			$d1 = $1.'u';
		}
	}
		
	marc_fixed('008','07',$d1);		# 07-10 - Date 1
	marc_fixed('008','11',$d2);		# 07-10 - Date 1

} else {
	marc_fixed('008','06','n');		# 06 - n = unknown date
	marc_fixed('008','07','uuuu');
	marc_fixed('008','11','    ');		# 07-10 - Date 1
}


## 008 15-17 - Place of publication, production, or execution - ako nema 102, popunjava se s |
marc_fixed('008','15',
	rec('102')
);

## 008 35-37 - Language
if ( frec('101','a') =~ m/(\w\w\w)/ ) {
	marc_fixed('008','35', $1 );
}
					
## 008 38 - Modified record
marc_fixed('008','38','|');		

## 008 39 - Cataloging source - d (other)
marc_fixed('008','39','d');		

### 008 - Books 
## 008 18-21 - Illustrations
if ( rec('215','c') && rec('215','c') =~ m/ilustr/ ) {
	marc_fixed('008','18','a')
}

## 008 22 - Target audience
marc_fixed('008','22','|');

## 008 23 - Form of item
marc_fixed('008','23','|');

## 008 24-27 - Nature of contents
marc_fixed('008','24','||||');

## 008 28 - Government publication
marc_fixed('008','28','|');

## 008 29 - Conference publication
marc_fixed('008','29','|');

## 008 30 - Festschrift
marc_fixed('008','30','|');

## 008 31 - Index
marc_fixed('008','31','|');

## 008 32 - Undefined
## 008 33 - Literary form
marc_fixed('008','33','|');

## 008 34 - Biography
marc_fixed('008','34','|');

### 020
## postoji modify za polje 10 -> drugi ISBN pocinje prefixom "ISBN" koji se mice (pr. u sfb)
#if ( frec('10') ne ( frec('290') ) ) {
	if ( rec('10','a') !~ /pogre/ ) {
		marc('020','a', 
			# isbn_13(
				regex('s/\s\s/ /g',
				# regex('s/\(\d\)\(/$1 \(//g',
					rec('10','a')
				)
			# )
		); 
	}
	if ( rec('10','a') =~ /pogre/ ) {
		marc('020','z',
			regex('s/\s\s/ /g',
				rec('10','a')
			)
		);
	}
	marc('020','z',
		rec('10','z')
	);
#}

### 035
## marc 035a - System Number 
## polje moze sadrzavati slova i razmake
## moguc problem u pretrazivanju ako ima zagrade, kako bi trebalo po standardu

marc('035','a',
join_with('',
	# config('input normalize path'),
	# config('name'),
	'(HR-ZaHPM)isis',
	# config(),
	# config('input name'),
	# id(),
	# rec('994','a'),
	rec('000')
	)
);

### 040
## za sve je isti

marc('040','a',
	'HR-ZaHPM'
);
marc('040','b',
	'hrv'
);
marc('040','c',
	'HR-ZaHPM'
);
marc('040','e',
	'ppiak'
);

### 041
## ponovljivo polje (101) konvertira se u ponovljivo potpolje (041a)
## koristi se kad ima vise od jednog jezika, ili kad se radi o prijevodu
## nadopuniti


if ( ( count( rec('101','a') ) > 1 ) || ( rec('101','b') ) || ( rec('101','h') ) ) {
	if ( ( grep( m/prijevod/i, rec('300') ) ) || ( rec('101','h') ) ) {
		marc_indicators('101', 1, ' ');
	} else { 
		marc_indicators('101', ' ', ' ');
	}
	marc_original_order('041','101');
}


### 044
## koristi se kad ima vise zemalja izdavanja

### 080 - UDK

marc('080','a',
	rec('675')
);

### 100 
## I1 = 0 ako je pseudonim (ima 700a, nema 700b)

if ( rec('700','a') && ! rec('700','b') ) {
	marc_indicators('100', 0, ' ');
} else {
	marc_indicators('100', 1, ' ');
}

marc('100','a',
	join_with(', ',
		rec('700','a'),
		rec('700','b')
	)
);

### formalna odrednica - iz 503 u 110, i1=0, i2=1

# nadopuniti nakon konverzije


### 111 i 110
## konverzija u 110 ili 111 ovisno o postojanju/nepostojanju nekih polja

if ( ! rec('700') ) {

if ( rec('710','d') || rec('710','e') || rec('710','f') ) {

	marc_indicators('111', 2, ' ');
	
	if ( rec('710','d') ) {
		marc_compose('111',
			'a', suffix(
				( rec('710','d') || rec('710','f') || rec('710','e') ) ? '' :
					'.',
					rec('710','a'),
			),
			'n', surround( '(', ' :', 
					rec('710','d'),
			),
			'd', suffix(
				( ! rec('710','d') ) ? ')' :
				( ! rec('710','e') ) ? ')' :
					' :',
					rec('710','f'),
			),
			'c', suffix(')',
				rec('710','e'),
			),
		);
	} else {
		marc_compose('111',
			'a', suffix(
				( rec('710','d') || rec('710','f') || rec('710','e') ) ? '' :
					'.',
					rec('710','a'),
			),
			'd', surround('(',' :',
					rec('710','f'),
			),
			'c', suffix(')',
				rec('710','e'),
			),
		);
	}
} else {
	marc_indicators('110', 2, ' ');
	marc('110','a',
		rec('710','a')
	);
	marc('110','b',
		rec('710','b')
	);
	if ( rec('710','c') ) {
		marc('110','c',
			surround('(', ')', rec('710','c'))
		);
	}
}

}

### 245 indikatori
## i1 = 0 za anonimne publikacije, i1 = 1 ako postoji 700 ili 710
## i2 = pretpostavlja se na temelju clana na pocetku naslova i jezika

my $titleind1;

if ( rec('700') || rec('710') || rec('711') ) {
	$titleind1 = 1;
} else {
	$titleind1 = 0;
}
	
marc_indicators('245', $titleind1, 0);

if ( rec('200','a') =~ m/^Einen / ) {
	marc_indicators('245', $titleind1, 6);
} 
if ( rec('200','a') =~ m/^Eine / ) {
	marc_indicators('245', $titleind1, 5);
} 
if ( rec('200','a') =~ m/^(Die|Das|Der|Ein|Les|Los|The) / ) {
	marc_indicators('245', $titleind1, 4);
} 
if ( rec('200','a') =~ m/^(Um|Un|An|La|Le|Lo|Il) / ) { 
	marc_indicators('245', $titleind1, 3);
} 
if ( ( rec('101') =~ m/eng/ ) && ( rec('200','a') =~ m/^A / ) ) { 
	marc_indicators('245', $titleind1, 2);
}
if ( rec('200','a') =~ m/^L / ) { 
	marc_indicators('245', $titleind1, 2);
} 
if ( rec('200','a') =~ m/^L'/ ) { 
	marc_indicators('245', $titleind1, 2);
} 


### 245
## postoji modify
## da li treba makivati razmake u inicijalima?

#_debug(3);

marc_compose('245',
	'a', suffix(
		( ! ( rec('200','d') || rec('200','e') || rec('200','k') ) ) && ( rec('200','f') ) ? ' /' :  
		( rec('200','d') ) ? ' =' :
		( rec('200','e') ) ? ' :' :
		( rec('200','k') ) ? ' ;' :
		( rec('200','h') ) ? '.' :
			'.', 
			rec('200','a'),
	),
	'b', suffix(
		( rec('200','d') && ! rec('200','f') ) ? '.' : 
		( rec('200','d') && ! rec('200','f') ) ? '.'  : 
			'',
			rec('200','d'),
	),
	'b', suffix(
		( rec('200','e') && rec('200','f') ) ? ' /' : 
		( rec('200','e') && rec('200','h') && ! rec('200','f') ) ? '.'  : 
			'',
			rec('200','e'),
	),
	'b', suffix(
		( rec('200','k') && rec('200','f') ) ? ' /' : 
		( rec('200','k') && rec('200','c') ) ? '.'  : 
			'',
			rec('200','k'),
	),
	'n', suffix(
		( rec('200','h') && rec('200','f') ) ? ' /'  :
		( rec('200','h') && rec('200','i') )  ? '.' :
		( rec('200','h') && ! ( rec('200','i') || rec('200','f') ) )  ? '.' :
			'',
			rec('200','h'),
	),
	'p', suffix(
		( rec('200','i') && rec('200','f') ) ? ' /'  :
		( rec('200','i') && ! rec('200','f') )  ? '.' :
			'',
			rec('200','i'),
	),
	'c', suffix(
		( rec('200','f') && rec('200','c') ) ? '. ' :
			'',
			join_with(' ; ',
				regex('s/(\S\.)\s(\S\.\s)/$1$2/g',
					rec('200','f'),
				),
				regex('s/(\S\.)\s(\S\.\s)/$1$2/g',
					rec('200','g')
				)
			)
	),
	## append to last subfield
	'+', suffix('.',
		join_with(' / ',
			rec('200','c'),
			rec('200','x')
		)
	),
);

#_debug(0);

### 250
## zapisima koji nemaju potpolje, dodaje se ^a u modify - provjeriti za svaku bazu
marc_compose('250',
	'a',suffix(
		rec('205','f') ? ' / ' : 
			'',
			rec('205','a')
	),
	'b', rec('205','f'),
);

### 260
## ponovljiva potpolja u originalnom redosljedu - priprema u modify
marc_original_order('260', '210');


### 300
## urediti interpunkcije za sve kombinacije
marc('300','a',
	suffix(
		rec('215','c') ? ' :' :
		rec('215','d') ? ' ;' :
		rec('215','e') ? ' +' :
			'',
			rec('215','a')
	)
);
marc('300','b',
	suffix(
		( rec('215','c') && rec('215','d') ) ? ' ;' :
		( rec('215','c') && rec('215','e') ) ? ' +' :
			'',
			rec('215','c')
	)
);
marc('300','c',
	suffix(
		rec('215','e') ? ' +' :
			'',
			rec('215','d')
	)
);
marc('300','e',
	rec('215','e')
);


if ( rec('225') ) {
	marc_indicators('490', 0, ' ');
	marc('490','a',
		rec('225','a')
	);
	marc('490','v',
		rec('225','v')
	);
}

# marc_indicators('830',' ','0');
# i2 ovisi clanu - popraviti 
#
# marc('830','a',
#	regex('s/(^.*)\s\/\s(.*)/$1 ($2)/g',
#		rec('225','a')
#	)
# );


### 830 - indikatori
## 2.ind. prema jeziku i clanu, uskladiti s 245

#marc_indicators('440', ' ', '0');
#
#if ( rec('225','a') =~ m/^Einen / ) {
#		marc_indicators('440', ' ', 6);
#} 
#
#if ( rec('225','a') =~ m/^Eine / ) {
#		marc_indicators('440', ' ', 5);
#} 
#
#if ( rec('225','a') =~ m/^(Die|Das|Der|Ein|Les|The) / ) {
#		marc_indicators('440', ' ', 4);
#} 
#
#if ( rec('225','a') =~ m/^(Um|Un|An|La|Le|Il) / ) { 
#		marc_indicators('440', ' ', 3);
#} 
#
#if ( rec('225','a') =~ m/^(A|L') / ) { 
#		marc_indicators('440', ' ', 2);
#} 

#if (
#	count( lookup(
#		sub { 1 },
#		'ffps','libri',
#		sub { rec('225','a') }
#	) ) > 1
#) {


#_debug(0);


### 500

marc('500','a',
	rec('300')
);

### 502 - biljeska o disertacijama
## odgovarajuceg polja nema u originalnim podacima. mozda se moze pretpostaviti?

### 504
marc('504','a',
	rec('320')
);


### 653 - Index Term-Uncontrolled
## da li ponovljivo polje ili potpolje, pogledati u Koha tags
marc_indicators('653', ' ', ' ');
marc('653','a',
	rec('610')
);



### 700
## ako je u originalu ponovljivo polje treba bioti i konvertirano u ponovljivo !!
## ako ima samo podpolje a onda je pseudonim - nakon konverzije treba provjeriti

if ( rec('701','a') ) {
	#_debug(3);
	marc_indicators('701', 1, ' '); # override original indicators
	marc_original_order('700','701'); 
	#_debug(0);
}

my $relcode = rec('702','4');
my $relcode_konv = {
        'autor' => 'aut',
        'autor dodatka' => 'wam',
        'autor fotografija' => 'pht',
        'autorica' => 'aut',
        'autorica i urednica' => 'aut edt',
        'autorica teksta' => 'aut',
        'autorica uvodnog teksta' => 'aui',
        'autor izložbe' => 'cur',
        'autor izložbe i fotografije' => 'cur pht',
        'autor kataloških jedinica' => 'aqt',
        'autorov suradnik' => 'oth',
        'autor popratnih tekstova i kataloga' => 'aqt',
        'autor predgovora' => 'aui',
        'autor projekta' => 'pdr',
        'autor projekta i urednik, izrada kazala' => 'pdr edt aqt',
        'autor teksta' => 'aut',
        'autor teksta i urednik' => 'aut edt',
        'autor tekstova u jednom poglavlju i autor fotografija' => 'aut pht',
        'autor uvoda' => 'aui',
        'bibliografski urednik' => 'pbd',
        'crtač geografskih karata' => 'ill',
        'crteži' => 'ill',
        'crteži karata' => 'ill',
        'dizajner' => 'dsr',
        'editor' => 'edt',
        'fotograf' => 'pht',
        'fotografija' => 'pht',
        'fotografije' => 'pht',
        'fotografije, engleski prijevod' => 'pht trl',
        'fotografije i crteži' => 'pht ill',
        'general editor' => 'edt',
        'GIS i izrada karata' => 'ctg',
        'glavna urednica' => 'edt',
        'glavni i odgovorni urednik' => 'edt',
        'glavni urednik' => 'edt',
        'ilustracije' => 'ill',
        'ilustracije i izrada karata' => 'ill ctg',
        'ilustrator' => 'ill',
        'ilustratorica' => 'ill',
        'izrada crteža' => 'ill',
        'izrada karata' => 'ill',
        'izrada karata antičkih prometnica' => 'ctg',
        'izrada kazala' => 'ant',
        'izvedba CD-ROMa' => 'oth',
        'kartograf' => 'ctg',
        'kolorirani bakropisi' => 'ill',
        'koncepcija' => 'ccp',
        'likovno-grafička oprema' => 'ill',
        'likovno oblikovanje' => 'ill',
        'obrada izvornih karata i izrada kartografskih originala' => 'ctg',
        'pisac predgovora' => 'aui',
        'pisac uvoda' => 'aui',
        'pomoćnica urednika' => 'edt',
        'pomoćnik glavnog urednika' => 'edt',
        'pomoćnik urednika' => 'edt',
        'pomoćni urednik' => 'edt',
        'predgovor' => 'aui',
        'predsjednik redakcijskog kolegija' => 'pbd',
        'prevoditelj' => 'trl',
        'prevoditeljica' => 'trl',
        ' prevoditeljica' => 'trl',
        'prevoditeljica i redaktorica' => 'trl',
        'priprema fotografija' => 'oth',
        'priređivač' => 'edt',
        'priređivač izdanja' => 'pbd',
        'redaktor' => 'oth',
        'savjetnik' => 'oth',
        'series editor' => 'pbd',
        'stručna suradnica' => 'ctb',
        'stručna suradnica i autorica teksta' => 'aut',
        'stručni koordinator' => 'oth',
        'stručni suradnik' => 'oth',
        'stručni suradnik i autor teksta' => 'aut',
        'stručni urednik' => 'edt',
        'suradnica' => 'oth',
        'suradnik' => 'oth',
        'tehnička realizacija CD-ROM-a' => 'oth',
        'tekstovi o muzejima' => 'aut',
        'tekstovi o zbirkama vjerskih zajednica' => 'aut',
        'urednica' => 'edt',
        'urednik' => 'edt',
        'urednik i autor' => 'edt aut',
        'urednik i urednik nakladničke cjeline' => 'edt pbd',
        'urednik izdanja' => 'edt',
        'urednik knjige i nakladničke cjeline' => 'edt',
        'urednik nakladničke cjeline' => 'pbd',
        'urednik pretiska' => 'edt',
        'urednik serije' => 'edt',
        'uvod' => 'aui',
        'zemljopisne karte' => 'ill',
        'zemljovidi' => 'ill',
        'zvučni snimci' => 'rce'
};


if ( rec('702','a') ) {
	marc_indicators('700', 1, ' ');

	foreach my $repeat ( rec_array('702') ) {
		my $sf_a = $repeat->{a} || die "702 doesn't have a ", dump( $repeat );
		if ( my $sf_4 = $repeat->{4} ) {
			my $relcode = $relcode_konv->{ $sf_4 } || die "no relcode_konv for ",dump( $sf_4 );
			marc_compose('700',
				'4' => $relcode,
				'a' => $sf_a,
			);
		} else {
			marc('700', 'a', $sf_a );
		}
	}
}

#_debug(0);

## eventualno nadopuniti 710 i 711 - provjetiti da li u podacima postoji u ISIS-u

if ( rec('700') ) {

if ( rec('710','d') || rec('710','e') || rec('710','f') ) {

marc_indicators('711', 2, ' ');

marc_compose('711',
	'a', suffix(
		rec('710','b') ? '.' :
			'',
			rec('710','a'),
	),
	'e', rec('710','b'),
	'n', prefix(
		rec('710','d') ? '(' :
			'',
			rec('710','d')
	),
	'd', prefix(
		( ! rec('710','d') ) ? '(' :
		( ! rec('710','e') ) ? '(' :
			'; ',
			rec('710','f'),
	),
	'c', prefix(
		( rec('710','e') && ( rec('710','d') || rec('710','f') ) ) ? '; ' :
		( ! rec('710','d') && ! rec('710','f') )  ? '(' :
			'',
			rec('710','e'),
	),
	'+',')',
);
} else {
	marc_indicators('710', 2, ' ');
	marc('710','a',
		rec('710','a')
	);
	marc('710','b',
		rec('710','b')
	);
	if ( rec('710','c') ) {
		marc('710','c',
			surround('(', ')', rec('710','c'))
		);
	}
}

}

### 852 - ne koristimo, koristimo  942 i 952

### 876 - item information - basic bibliographic unit (R)
## da li ovdje zapisati stare inventarne brojeve?
#marc('876','a',
#	rec('991','t'),
#);


### KOHA items
marc('942','b',
	'LIB'
);

my $keyword = dump( rec('610') );
my $itype;

if ( rec('200','a') =~ m/Elektroni/ ) {
	marc('942','c',
		'CD'
	);
	$itype = 'CD';
} elsif ( $keyword =~ m/disertacija/ ) {
	marc('942','c',
		'DIS'
	);
	$itype = 'DIS';
} elsif ( $keyword =~ m/magistarski/ ) {
	marc('942','c',
		'MAG'
	);
	$itype = 'MAG';
} elsif ( rec('990') =~ m/(separat|posebni)/ ) {
	marc('942','c',
		'SEP'
	);
	$itype = 'SEP';
} elsif ( rec('990') =~ m/starih/ ) {
	marc('942','c',
		'OLD'
	);
	$itype = 'OLD';
} else {
	marc('942','c',
		'BOOK'
	);
	$itype = 'BOOK';
}

# HPM local

marc('991','a',
	rec('991','b')
);
marc('992','a',
	rec('992')
);
marc('993','a',
	rec('990')
);


# Items

#marc('952','a',
#	'HPM'
#);
#marc('952','b',
#	'HPM'
#);
#marc('952','e',
#	rec('991','a')
#);
#marc('952','o',
#	rec('990')
#);
#marc('952','t',
#	rec('991','b')
#);

my $copynumber = rec('991','b') ;
my $callnumber = rec('990') ;

my $location = rec('992');
my $location_konv = {
	'ZB, Min. odjel i Geol. odjel, HPrM' => 'ZB',
	'ZB, Min. odjel i Geol. odjel, HPM' => 'ZB',
	'ZB i Min. odjel, HPrM' => 'ZB',
	'ZB i Min. odjel, HPM' => 'ZB',
	'ZB i Geol. odjel, HPrM' => 'ZB',
	'ZB i Geol. odjel, HPM' => 'ZB',
	'ZB, HRrM' => 'ZB',
	'ZB, HrPM' => 'ZB',
	'ZB, HPrM' => 'ZB',
	'ZB, HPM' => 'ZB',
	'ZB, Geol. odjel i Min. odjel, HPM' => 'ZB',
	'Min. odjel i ZB, HPM' => 'ZB',
	'Min. odjel i Geol. odjel, HPrM' => 'MP',
	'Min. odjel i Geol. odjel, HPM' => 'MP',
	'Min. odjel, HPrM' => 'MP',
	'Min. odjel, HPM' => 'MP',
	'Min. odjel' => 'MP',
	'Institut Makarska' => 'ZB',
	'Geol. odjel i ZB, HPM' => 'ZB',
	'Geol. odjel i Min. odjel, HPM' => 'MP',
	'Geol. odjel, HPrM' => 'GP',
	'Geol. odjel, HPM' => 'GP'
};

#dump ( $itype );

if ( rec('991','a') ) {
	foreach my $repeat ( rec_array('991') ) {
		my $sf_a = $repeat->{a} || die "991 doesn't have a ", dump( $repeat );
		if ( my $sf_b = $repeat->{b} ) {
			$sf_b || die "no inv";
			marc_compose('952',
				'a' => 'HPM',
				'b' => 'HPM',
				'c' => $location_konv->{$location},
				't' => $sf_b,
				'o' => $callnumber,
				'y' => $itype
			);
		}
	}
}

} # polje 201a
