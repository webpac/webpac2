# special mungle file which prepare data_structure from XML

my $h = get_ds->{node};
#warn "## hash to work on = ",dump( $h );

set_ds(
	Node_ID =>	$h->{id},
	Title =>	$h->{title},
	Author =>	$h->{author}->{content},
	Date =>		$h->{created},
	Content =>	$h->{doctext}->{content},
	Type =>		$h->{type}->{content},
);


