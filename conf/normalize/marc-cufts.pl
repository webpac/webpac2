marc('022','a',
	rec('022','a')
);

marc('022','y',
	rec('022','y')
);


marc_indicators('245', 0, 0);

marc('245','a',
	rec('245','a')
);

marc('245','b',
	rec('245','b')
);

marc('245','h',
	'[ Elektroni�ka gra�a ]'
);

marc_indicators('246', 0, 0);

marc('246','a',
	rec('246','a')
);

marc('500','a',
	rec('500','a')
);

marc('500','a',
	lookup(
		sub { 'Licenciran' },
		'wiley','licenced',
		sub { 
			lc( rec('A') )
		},
		sub { 
			regex('s/\s*(\.|:)\s*$//g',
				lc( rec('245','a') )
			);
		},
	)
);

marc_indicators('650', ' ', 2);

marc('650','a',
	rec('650','a')
);

marc('650','v',
	rec('650','v')
);

marc('650','z',
	rec('650','z')
);

marc('856','u',
	rec('856','u')
);


