tag('MFN',
	rec('000')
);

display('ISBN',
	rec('10')
);

display('ISSN',
	rec('11')
);

search('ISN',
	rec('10'),
	rec('11'),
);

tag('Language',
	rec('101')
);

tag('TitleProper',
	regex( 's/<[^>]*>//g',
		rec('200','a')
	)
);

tag('titleNo',
	rec('200','9')
);

tag('Subtitle',
	rec('200','e')
);

tag('TitleProper2',
	rec('200','c')
);

tag('ParallelTitle',
	rec('200','d')
);

tag('Responsibility',
	join_with(" ; ",
		rec('200','f'),
		rec('200','g')
	)
);

tag('VolumeDesignation',
	rec('200','v')
);

tag('EditionStatement',
	rec('205','a')
);

tag('SerialNo',
	rec('207','a')
);

tag('fond',
	rec('209','a')
);

tag('PlacePublication',
	rec('210','a')
);

tag('NamePublisher',
	rec('210','c')
);

tag('DatePublication',
	rec('210','d')
);

tag('PhysicalDescription',
	join_with(" : ",
		rec('215','a'),
		join_with(" ; ",
			rec('215','c'),
			rec('215','d'),
		)
	)
);

tag('MaterialDesignation',
	rec('215','a')
);

tag('PhysicalDetails',
	rec('215','c')
);

tag('AccompanyingMaterial',
	rec('215','e')
);

tag('Series',
	join_with(" = ", rec('225','a'),
		join_with(" : ", rec('225','d'),
			join_with(" ; ", rec('225','e'),
				join_with(". ", rec('225','v'),
					join_with(", ", rec('225','h'),
						join_with(" ; ", rec('225','i'),
							rec('225','w'),
						)
					)
				)
			)
		)
	)
);

tag('SeriesTitle',
	rec('225','a')
);

tag('GeneralNote',
	rec('300')
);

tag('EditionNote',
	rec('305')
);

tag('PhysicalDescriptionNote',
	rec('307')
);

tag('IntellectResponsNote',
	rec('314')
);

tag('InternalBibliographies',
	rec('320')
);

tag('Frequency',
	rec('326')
);

tag('ContentsNote',
	rec('327')
);

tag('Summary',
	rec('330')
);

tag('SystemRequirements',
	rec('337')
);

tag('IssuedWith',
	join_with(': ', rec(423,'z'),
		join_with(' / ', rec(423,'a'),
			join_with(' ', rec(423,'c'),
				rec(423,'b')
			)
		)
	)
);

display('Parts',
	lookup(
		prefix( 'dio-jzav:', rec(900) )
	)
);

search('Parts',
	lookup(
		prefix( 'id-dio-jzav:', rec(900) )
	)
);

display('PartsEF',
	lookup(
		prefix( 'naslov-efzg:001', rec('001') )
	)
);

tag('PartsID',
	rec('463','1')
);

tag('Piece',
	lookup(
		prefix( 'naslov-efzg:',
			first(
				rec(463,1)
			)
		)
	)
);

tag('PieceSubtitle',
	lookup(
		prefix( 'podnaslov-efzg:',
			first(
				rec(463,1)
			)
		)
	)
);

tag('PieceNum',
	rec('463','v')
);

tag('PieceAnalitic',
	join_with(' / ',
		rec('464','a'),
		join_with(' ',
			rec(464,'g'),
			rec(464,'f'),
		)
	)
);

tag('UniformHeading',
	join_with('. ',
		rec(500,'a'),
		rec(500,'b'),
	)
);

tag('ExpandedTitle',
	rec(532)
);

tag('Form',
	rec(608)
);

tag('UncontroledTerms',
	rec(610)
);

tag('UDC',
	rec(675)
);

tag('APACC',
	rec(686)
);

tag('PersonalNamePrim',
	join_with(', ',
		rec(700,'a'),
		rec(700,'b'),
	)
);

tag('PersonalNameSec',
	join_with(', ',
		rec(701,'a'),
		rec(701,'b'),
	)
);

tag('PersonalNameOth',
	join_with(', ',
		rec(702,'a'),
		rec(702,'b'),
	)
);

search('Names',
	rec(700),
	rec(701),
	rec(702),
);

tag('CorporateNamePrim',
	join_with(', ',
		rec(710,'a'),
		rec(710,'b'),
	)
);

tag('CorporateNameSec',
	join_with(', ',
		rec(711,'a'),
		rec(711,'b'),
	)
);

tag('OriginatingSource',
	rec(801)
);

tag('URL',
	rec(856,'u')
);

tag('level',
	rec(909)
);

tag('ID',
	rec(900)
);

display('Set',
	lookup(
		prefix( 'set-jzav:',
			rec(946,1),
		)
	),
	lookup(
		prefix( 'set-efzg:',
			rec(461,1),
		)
	)
);

search('Set',
	rec(946,1),
	rec(461,1),
);

tag('Set2',
	lookup(
		prefix( 'set-jzav:',
			lookup( 'parent-id:',
				rec(946,1)
			)
		)
	)
);

tag('CallNo',
	rec(990)
);

tag('InvNo',
	rec(991)
);

	




