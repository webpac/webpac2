if (
	rec('225','a')	# da li je nakladnicka cjelina?
#	&&		# i
#	count( lookup(
#		sub { 1 },
#		'ffsf','blibri',
#		sub {
#			join_with('',
#				rec('225','a'),
#				rec('210','a'),
#				rec('210','b'),
#			)
#		}
#	) ) > 1
) {

my $series_key =
join_with('',
	rec('225','a'),
	rec('210','a'),
	rec('210','b'),
);

my $series = get( $series_key );

if ( $series ) {
	# warn "nije novi";
} else {


$series = join_with('',
	config(),
	' LS',
	rec('000')
);

set( $series_key => $series );

# Leader
marc_leader('05','n');
marc_leader('06','a');
marc_leader('07','s');
marc_leader('17','7');
marc_leader('18','i');

## 008 

marc_fixed('008',00,
	'070401'
);

marc_fixed('008','35',
	first( lc( rec('101') ) )
);

marc('022','a',
	rec('225','x')
);

marc('035','6', $series );

marc('040','a',
	'HR FFZG'
);

marc('040','b',
	'hrv'
);

marc('040','e',
	'HR PPIAK'
);

marc_indicators('041', 0, ' ');

marc_repeatable_subfield('041','a',
	rec('101')
);

# indikatori za 245 

my $i1;

if ( rec('700') || rec('710') ) {
	$i1 = 1;

} else {
	$i1 = 0;
}
	
marc_indicators('245', $i1, 0);

if ( 	( rec('200','a') =~ m/^Die /) || 
	( rec('200','a') =~ m/^Das /) || 
	( rec('200','a') =~ m/^The /) ) {
		marc_indicators('245', $i1, 4);
} 

if ( 	( rec('200','a') =~ m/^A /) && ( rec(101) =~ m/ENG/ ) ) {
		marc_indicators('245', $i1, 2);
} 

if ( 	( rec('200','a') =~ m/^An /) && ( rec(101) =~ m/ENG/ ) ) {
		marc_indicators('245', $i1, 2);
} 


## 245

marc_compose('245',
	'a', suffix(
		( ! ( rec('225','d') || rec('225','e') ) ) && ( rec('225','f') ) ? ' / ' :  
		( rec('225','d') ) ? ' = ' :
		( rec('225','e') ) ? ' : ' :
		( rec('225','i') ) ? '. ' :
			'', 
			rec('225','a'),
	),
	'b', suffix(
		( rec('225','d') && rec('225','f') ) ? ' / ' : 
			'',
			rec('225','d'),
	),
	'b', suffix(
		( rec('225','e') && rec('225','f') ) ? ' / ' : 
			'',
			rec('225','e'),
	),
	'c', suffix(
		( rec('225','f') && rec('225','i') ) ? '. ' :
			'',
			rec('225','f'),
	),
	'p', rec('225','i'),
);

#marc_compose('245',
#	'a', suffix(
#		( ! rec('225','p') && ( rec('225','f') || rec('225','c') ) ) ? ' / ' :  
#		( rec('225','p') && rec('225','c') ) ? ' : '  : 
#			'', 
#			first(rec('225','a')),
#	),
#	'b', suffix(
#		( rec('225','p') && rec('225','f') ) ? ' / ' : 
#		( rec('225','p') && rec('225','c') ) ? '. '  : 
#			'',
#			rec('225','p'),
#	),
#	'c', suffix(
#		( rec('225','f') && rec('225','c') ) ? '. ' :
#			'',
#			rec('225','f'),
#	),
#);

marc_original_order('260', '210');

marc_remove('260','c');

## FIXME - UPISATI PODATAK U 260c

#_debug(2);

#marc('900','a',
#	lookup(
#		sub { rec('702','a') . ', ' . rec('702','b') },
#		'nsk','baza',
#		sub { rec('200','a') . rec('210','a') . rec('210','c') },
#		sub { rec('225','a') . rec('210','a') . rec('210','b') },
#	)
#);

#_debug(0);

marc_duplicate();
marc_remove('*');


} # $series

} # nakladnička cjelina
