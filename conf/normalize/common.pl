tag('MFN',
	rec('000')
);

display('ISBN',
	rec1('10'),
	rec('010','a')
);

display('ISBN_information',
	rec('010','b')
);

display('ISBN_incorrect',
	rec('010','z')
);

display('ISSN',
	rec1('11'),
	rec('011','a')
);

display('ISSN_incorrect',
	rec('011','z')
);

display('ISSN_cancelled',
	rec('011','y')
);

search('ISN',
	rec('10'),
	rec('010'),
	rec('11'),
	rec('011'),
);

tag('LanguageText',
	rec(101,'a')
);

tag('LanguageOriginal',
	join_with(', ',
		rec(101,'b'),
		rec(101,'d')
	)
);

tag('TitleProper',
	regex( 's/<[^>]*>//g',
		rec(200,'a')
	)
);

tag('Medium',
	rec(200,'b')
);

tag('AnotherAuthorTitle',
	prefix('. ',
		rec(200,'c')
	)
);

tag('ReminderTitle',
	join_with(' : ',
		prefix(' = ',
			rec(200,'d')
		),
		rec(200,'e')
	)
);

tag('StatementResponsibility',
	join_with(" ; ",
		rec(200,'f'),
		rec(200,'g')
	)
);

tag('NumberPart',
	rec(200,'h'),
	rec(200,'v')
);

tag('NamePart',
	rec(200,'i')
);

tag('EditionStatement',
	join_with(', ',
		rec(205,'a'),
		join_with(' = ',
			rec(205,'b'),
			rec(205,'d')
		)
	)
);

tag('StatementScale',
	rec(206,'a')
);

tag('SerialsNumberingDateVol',
	rec(207,'a')
);

tag('SerialsNumberingSource',
	rec(207,'z')
);

tag('fond',
	rec('209','a')
);

tag('PlacePublication',
	rec('210','a')
);

tag('NamePublisher',
	rec(210,'c'),
	surround(' (', ')', rec(210,'b') )
);

tag('DatePublication',
	rec('210','d')
);

tag('PlaceManufacture',
	rec(210,'e')
);

tag('NameManufacturer',
	rec('210','g'),
	surround(' (', ') ', rec(210,'f') )
);

tag('DateManufacture',
	rec(210,'h')
);

tag('ExtentItem',
	rec(215,'a')
);

tag('OtherPhysicalDetails',
	rec(215,'c')
);

tag('Dimensions',
	rec(215,'d')
);

tag('AccompanyingMaterial',
	rec(215,'e')
);

tag('SeriesStatement',
	join_with(" = ", 
		rec('225','a'),
		join_with(" : ", 
			rec('225','d'),
			join_with(" / ", 
				rec('225','e'),
				rec('225','f'),
			)
		)
	)
);

tag('VolumeNumber',
	join_with(', ',
		rec(225,'h'),
		rec(225,'i'),
		rec(225,'v')
	)
);

tag('SeriesISSN',
	rec(225,'x')
);

tag('ComputerFileCharacteristics',
	rec(230,'a')
);

tag('GeneralNote',
	rec('300')
);

tag('InternalBibliographiesNote',
	rec('320')
);

tag('FrequencyNote',
	first(
		rec('326')
	)
);

tag('ContentsNote',
	rec('327')
);

tag('Summary',
	rec('330')
);

tag('SystemDetailsNote',
	rec('337')
);

tag('IssuedWith',
	join_with(': ', rec(423,'z'),
		join_with(' / ', rec(423,'a'),
			join_with(' ', rec(423,'c'),
				rec(423,'b')
			)
		)
	)
);

#display('parts',
#	lookup(
#		prefix( 'dio-jzav:', rec(900) )
#	)
#);
#
#search('parts',
#	lookup(
#		prefix( 'id-dio-jzav:', rec(900) )
#	)
#);
#
#display('partsEF',
#	lookup(
#		prefix( 'naslov-efzg:001', rec('001') )
#	)
#);
#
#tag('partsID',
#	rec('463','1')
#);
#
#tag('piece',
#	lookup(
#		prefix( 'naslov-efzg:',
#			first(
#				rec(463,1)
#			)
#		)
#	)
#);
#
#tag('pieceSubtitle',
#	lookup(
#		prefix( 'podnaslov-efzg:',
#			first(
#				rec(463,1)
#			)
#		)
#	)
#);

tag('pieceNum',
	rec('463','v')
);

tag('pieceAnalitic',
	join_with(' / ',
		rec('464','a'),
		join_with(' ',
			rec(464,'g'),
			rec(464,'f'),
		)
	)
);

tag('UniformTitle',
	rec(500,'a'),
	surround(' [', ']', rec(500,'b') )
);

tag('VariantAccessTitle',
	rec(532)
);

tag('SubjectForm',
	rec(608)
);

tag('UncontrolledTerms',
	rec(610)
);

tag('UDC',
	rec(675)
);

tag('OtherCassification',
	rec(686)
);

tag('PersonalNamePrim',
	join_with(', ',
		rec(700,'a'),
		rec(700,'b'),
	)
);

tag('PersonalNameAdded',
	join_with(', ',
		rec(701,'a'),
		rec(701,'b'),
	),
	join_with(', ',
		rec(702,'a'),
		rec(702,'b')
	)
);

search('names',
	rec(700),
	rec(701),
	rec(702),
);

tag('CorporateNamePrim',
	join_with(', ',
		rec(710,'a'),
		rec(710,'b'),
	)
);

tag('CorporateNameAdded',
	join_with(', ',
		rec(711,'a'),
		rec(711,'b'),
	)
);

tag('CatalogingSource',
	rec(801)
);

tag('ElectronicLocation',
	rec(856,'u')
);

tag('level',
	rec(909)
);

tag('id',
	rec(900)
);

#display('set',
#	lookup(
#		prefix( 'set-jzav:',
#			rec(946,1),
#		)
#	),
#	lookup(
#		prefix( 'set-efzg:',
#			rec(461,1),
#		)
#	)
#);

search('set',
	rec(946,1),
	rec(461,1),
);

#tag('set2',
#	lookup(
#		prefix( 'set-jzav:',
#			lookup( 'parent-id:',
#				rec(946,1)
#			)
#		)
#	)
#);

tag('ShelvingControlNumber',
	rec(990)
);

tag('InternalItemNumber',
	rec(991,'b'),
	rec1(991)
);


