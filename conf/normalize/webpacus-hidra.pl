search_display('TitleProper',
	join_with(' / ',
		join_with(' = ',
			join_with(' ',
				join_with(' ',
					rec('200','a'),
					rec('200','c')
				),
				rec('200','b'),
			),
			rec('200','d'),
		),
		rec('200','f'),
	)
);
	
# regex('s/\s*[=:\/;]\s*//g',

display('Edition',
	rec('205','a')
);

display('Impressum',
	join_with(', ',
		join_with(' : ',
			rec('210','a'),
			rec('210','c'),
		),
		rec('210','d'),
	)
);

display('Series',
	rec('225','a'),
);


display('SeeAlso',
	lookup(
		sub { [ rec('200','a') . rec('200','c'),  rec('451','a') ] },
		'hidra','bib',
		sub { rec('900') },
		sub { rec('451','1') }
	)
);

display('Fond',
	lookup(
		sub { rec('200','a') },
		'hidra','bib',
		sub { rec('900') },
		sub { rec('946','1') }
	)
);	

display('Direktive',
	lookup(
		sub { rec('200','a') . ', ' . rec('856','u') },
		'hidra','dir',
		sub { rec('900') },
		sub { rec('859','1') }
	)
);

display('Set',
	lookup(
		sub { rec('200','a') },
		'hidra','bib',
		sub { rec('900') },
		sub { rec('946','1') },
	)
);

display('GeneralNote',
	rec('300','a')
);

display('URL',
	rec('856','u')
);

