
if ( rec('200','a') ) {

marc('035','6',
	join_with('',
		uc( config() ),
		' A',
		rec('000')
	)
);

marc_indicators('245', 1, 0);

marc('245','a',
	join_with(' / ',
		join_with(' : ',
			join_with(' = ',
				rec('225','a'),
				rec('225','d')
			),
			rec('225','e')
		),
		join_with(' ; ',
			rec('225','f'),
			rec('225','g')
		),
	),
);

marc('999','a',
	rec('200','i1')
);

}
