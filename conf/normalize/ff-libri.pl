
# Ova datoteka sadr�i pravila za generiranje MARC21 ISO 2709 izlazne
# datoteke. Ulazni format je CDS/ISIS izvorni format, sa zapisima u
# UNIMARC formatu, sa vi�estrukim odstupanjima od standarda.


## lookup rezultat za sabrana djela (probna procedura za filozofiju od koje se odustalo)
# my $sabrana;


## sve skupa se konvertira tek ako postoji polje 200^a
if ( rec('200','a') ) {

### LEADER

## LDR 05 - n - new
marc_leader('05','n');

## LDR 06 - a - language material 
marc_leader('06','a');

## LDR 07 - m - Monograph/item
marc_leader('07','m');

# ovo je bila proba za filozofiju
# if ( config() =~ m/fffi/ ) { 
#	$sabrana = 
#		lookup(
#			sub{ rec('A') },
#			'fffi','sabrana',
#			sub { rec('B') },
#			sub { rec('000') }
#		);
# } 
#
# if ( ! $sabrana ) {
#	marc_leader('07','m');
# } else {
#	marc_leader('07','c');
# }

## LDR 17 - Encoding level ; 7 - minimal level, u - unknown
marc_leader('17','u');

## LDR 18 - i - isbd 
marc_leader('18','i');

## LDR 19 - Multipart resource record level ; # - Not specified or not applicable, a - Set, b - Part with independent title, c - Part with dependent title 

#if ( ( ! $sabrana ) && ( rec('230') || rec('231') || rec('232') || rec('233') ) ) { 
#	marc_leader('19','a');
#}

if ( ( rec('230') || rec('231') || rec('232') || rec('233') ) ) { 
	marc_leader('19','a');
}

### 008 - All materials
## dodati sve moguce slucajeve za datum, popuniti ono sto nedostaje
## provjereno: fi

if ( rec('994','c') =~ m/\d{8}/ ) {
	marc_fixed('008','00',
		regex('s/^\d\d//',
			rec('994','c')
		),
	);
} elsif ( rec('994','c') =~ m/\d{6}/ ) {
	marc_fixed('008','00',
		regex('s/^\d\d//',
			rec('994','c')
		),
	);
	marc_fixed('008','04',
		'01'
	);
} elsif ( rec('994','c') =~ m/\d{4}/ ) {
	marc_fixed('008', 00,
		regex('s/^\d\d//',
			rec('994','c')
		),
	);
	marc_fixed('008','02',
		'0101'
	);
} else {
	marc_fixed('008','00',
		'010101'
	);
}

#warn( dump(
# 	regex('s/^\d\d//',
#		rec('994','c')
#	),
#)
#);

## 008 06 - Type of date/Publication status

## 008 07-10 - Date 1
## 008 11-14 - Date 2 
## 210d kroz modify postaje 210c

if ( rec('210','c') ) {
	my $d1 = '';
	my $d2 = '';
	if ( rec('210','c') =~ m/(\d{4})-/ ) {
		marc_fixed('008','06','m');
		if ( rec('210','c') =~ m/(\d{4})-/ ) {
			$d1 = $1;
			$d2 = '9999';
		}
		if ( rec('210','c') =~ m/-(\d{4})/ ) {
			$d2 = $1;
		}
	} else {
		marc_fixed('008','06','s');
		if ( rec('210','c') =~ m/(\d{4})/ ) {
			$d1 = $1;
		}
		if ( rec('210','c') =~ m/(\d{2})(--|__)/ ) {
			$d1 = $1.'uu';
		} 
		if ( rec('210','c') =~ m/(\d{3})(-|_)/ ) {
			$d1 = $1.'u';
		}
	}
		
# 	if ( rec('210','c') =~ m/(\?)/ ) {
# 		marc_fixed('008','06','q');
# 		if ( rec('210','c') =~ m/(\d{2})(--|__)/ ) {
# 			$d1 = $1.'uu';
# 		} 
# 		if ( rec('210','c') =~ m/(\d{3})(-|_)/ ) {
# 			$d1 = $1.'u';
# 		}
# 		if ( rec('210','c') =~ m/(\d{4})/ ) {
# 			$d1 = $1;
# 		}
# 	} elsif ( rec('210','c') =~ m/(\d{4})-/ ) {
#		if ( $sabrana ) { 
#			marc_fixed('008','06','i');
#		} else {
#			marc_fixed('008','06','m');
#		}
# 		marc_fixed('008','06','m')
# 		if ( rec('210','c') =~ m/(\d{4})-/ ) {
# 			$d1 = $1;
# 			$d2 = '9999';
# 		}
# 		if ( rec('210','c') =~ m/-(\d{4})/ ) {
# 			$d2 = $1;
# 		}
# 	} else {
# 		marc_fixed('008','06','s');
# 		if ( rec('210','c') =~ m/(\d{4})/ ) {
# 			$d1 = $1;
# 		}
# 	}

	marc_fixed('008','07',$d1);		# 07-10 - Date 1
	marc_fixed('008','11',$d2);		# 07-10 - Date 1

} else {
	marc_fixed('008','06','n');		# 06 - n = unknown date
	marc_fixed('008','07','uuuu');
}



## 008 15-17 - Place of publication, production, or execution - ako nema 102, popunjava se s |
marc_fixed('008','15','|||');

#marc_fixed('008','15', 
#	lc ( rec('102') ) 
#);

#my $zemlja = 
#	lookup(
#		sub { rec('B') },
#		'kodovi','zemlje',
#		sub { rec('A') },
#		sub { 
#			regex('s/[\s;:]/g',
#				rec('210','a') 
#			)
#		},
#	);


## 008 35-37 - Language
marc_fixed('008','35',
	# first( lc(rec('101')) )		
	lc( frec('101'))		
);
					
## 008 38 - Modified record
marc_fixed('008','38','|');		

## 008 39 - Cataloging source - d (other)
marc_fixed('008','39','d');		

### 008 - Books - raspraviti upotrebu ovih polja
## 008 18-21 - Illustrations
if ( rec('215','c') && rec('215','c') =~ m/ilustr/ ) {
	marc_fixed('008','18','a')
}

## 008 22 - Target audience
marc_fixed('008','22','|');

## 008 23 - Form of item
marc_fixed('008','23','|');

## 008 24-27 - Nature of contents
marc_fixed('008','24','||||');

## 008 28 - Government publication
marc_fixed('008','28','|');

## 008 29 - Conference publication
marc_fixed('008','29','|');

## 008 30 - Festschrift
marc_fixed('008','30','|');

## 008 31 - Index
marc_fixed('008','31','|');

## 008 32 - Undefined
## 008 33 - Literary form
marc_fixed('008','33','|');

## 008 34 - Biography
marc_fixed('008','34','|');

### 020
## postoji modify za polje 10 -> drugi ISBN po�inje prefixom "ISBN" koji se mi�e (pr. u sfb)
#if ( frec('10') ne ( frec('290') ) ) {
	if ( rec('10','a') !~ /pogre/ ) {
		marc('020','a', 
			# isbn_13(
				regex('s/\s\s/ /g',
				# regex('s/\(\d\)\(/$1 \(//g',
					rec('10','a')
				)
			# )
		); 
	}
	if ( rec('10','a') =~ /pogre/ ) {
		marc('020','z',
			regex('s/\s\s/ /g',
				rec('10','a')
			)
		);
	}
	marc('020','z',
		rec('10','z')
	);
#}

#warn( 
#	dump(
#		frec('10')
#	),
#);

### 035$
## marc 035a - System Number 
## polje mo�e sadr�avati slova i razmake
## mogu� problem u pretra�ivanju ako ima zagrade, kako bi trebalo po standardu

marc('035','a',
join_with('',
	# config('input normalize path'),
	# config('name'),
	'HR-ZaFF ',
	config(),
	'L',
	# config('input name'),
	# id(),
	# rec('994','a'),
	rec('000')
	)
);

### 040
## za sve je isti

marc('040','a',
	'HR-ZaFF'
);
marc('040','b',
	'hrv'
);
marc('040','c',
	'HR-ZaFF'
);
marc('040','e',
	'ppiak'
);

### 041 - indikatori
## pretpostavke o indikatorima
## 1. Ako postoji polje 300 i ono sadr�i /Prijevod/ ili /Izv. stv. nasl./, i1 = 1
## (Da li je napomena uvijek pisana?)
## 2. Podatak o prevodiocu u 200g. Problem: razli�iti oblici rije�i, razli�iti jezici.
## 3. Ako u UDK oznaci postoji "=", bez zagrada, onda je to prijevod
## (ako postoje zagrade pr. 94(=411.16) onda su to pomo�ne oznake)
## pr. 821.111-2=163.42 je prijevod engleske drame na HR. 
## to ima samo za knji�evna djela, eseje.... 
## ovo treba kasnije ispravljati

#if ( rec('675') =~ m/\d=\d/ ) ?

if ( 
	grep( m/prijevod/i, rec('300') ) 
	|| grep( m/nasl.*izv/i, rec('300') ) 
#	( frec('300') =~ m/Prijevod/ )
#	|| ( rec('300') =~ m/izvorni stvarni naslov/i )
#	|| ( rec('300') =~ m/naslov izvornika/i )
#	grep( m/[prijevod|stvarni\snaslov]/i, rec('300') ) 
) {
	marc_indicators('041', 1, ' ');
	marc_repeatable_subfield('041','a',
		map { lc($_) } rec('101')
	);
} elsif ( 
	count( rec('101') ) > 1 
) {
	marc_indicators('041', 0, ' ');
	marc_repeatable_subfield('041','a',
		map { lc($_) } rec('101')
	);
}

### 041
## ponovljivo polje (101) konvertira se u ponovljivo potpolje (041a)
## koristi se kad ima vi�e od jednog jezika, ili kad se radi o prijevodu
## nadopuniti

### 044
## koristi se kad ima vi�e zemalja izdavanja

### 080
## 675 podpolja b, c, d ... - �to s njima? - provjeriti za svaku bazu
## fi: abcd
## sk: ima \r\n na kraju

marc('080','a',
	rec('675','a')
);

marc('080','a',
	rec('675','b')
);

marc('080','a',
	rec('675','c')
);

marc('080','a',
	rec('675','d')
);


### 084 - other classification (R)
## NSK ga koristi za stru�nu oznaku NSK iz polja 681
## NSK konvertira polje 686 u 084, u skladu s LOC konv. tablicom
## polje 686 koristi FFPS. jo� netko?

marc('084','2',
	'APA CC'
);
marc('084','a',
	rec('686')
);

### 100 
## I1 = 0 ako je pseudonim (ima 700a, nema 700b)

if ( rec('700','a') && ! rec('700','b') ) {
	marc_indicators('100', 0, ' ');
} else {
	marc_indicators('100', 1, ' ');
}

marc('100','a',
	join_with(', ',
		rec('700', 'a'),
		rec('700', 'b')
	)
);

### formalna odrednica - iz 503 u 110, i1=0, i2=1

### 111 i 110
## provjeriti za svaku bazu
## konverzija u 110 ili 111 ovisno o postojanju/nepostojanju nekih polja - provjeriti ispise naslova iz svake zbirke - mogu�e su gre�ke.
## popraviti interpunkciju

# ovo vrijedi za FFSFB
if ( rec('710','d') || rec('710','e') || rec('710','f') ) {

marc_indicators('111', 2, ' ');

marc_compose('111',
	'a', suffix(
		rec('710','b') ? '.' :
			'',
			rec('710','a'),
	),
	'e', rec('710','b'),
	'n', prefix(
		rec('710','d') ? '(' :
			'',
			rec('710','d')
	),
	'd', prefix(
		( ! rec('710','d') ) ? '(' :
		( ! rec('710','e') ) ? '(' :
			'; ',
			rec('710','f'),
	),
	'c', prefix(
		( rec('710','e') && ( rec('710','d') || rec('710','f') ) ) ? '; ' :
		( ! rec('710','d') && ! rec('710','f') )  ? '(' :
			'',
			rec('710','e'),
	),
	'+',')',
);
} else {
	marc_indicators('110', 2, ' ');
	marc('110','a',
		rec('710','a')
	);
	marc('110','b',
		rec('710','b')
	);
	if ( rec('710','c') ) {
		marc('110','c',
			surround('(', ')', rec('710','c'))
		);
	}
}

### 245 indikatori
## i1 = 0 za anonimne publikacije, i1 = 1 ako postoji 700 ili 710
## i2 = pretpostavlja se na temelju �lana na po�etku naslova i jezika
## treba provjeriti

my $titleind1;

if ( rec('700') || rec('710') ) {
	$titleind1 = 1;
} else {
	$titleind1 = 0;
}
	
marc_indicators('245', $titleind1, 0);

if ( rec('200','a') =~ m/^Einen / ) {
	marc_indicators('245', $titleind1, 6);
} 
if ( rec('200','a') =~ m/^Eine / ) {
	marc_indicators('245', $titleind1, 5);
} 
if ( rec('200','a') =~ m/^(Die|Das|Der|Ein|Les|Los|The) / ) {
	marc_indicators('245', $titleind1, 4);
} 
if ( rec('200','a') =~ m/^(Um|Un|An|La|Le|Lo|Il) / ) { 
	marc_indicators('245', $titleind1, 3);
} 
if ( ( rec('101') =~ m/ENG/ ) && ( rec('200','a') =~ m/^A / ) ) { 
	marc_indicators('245', $titleind1, 2);
}
if ( rec('200','a') =~ m/^L / ) { 
	marc_indicators('245', $titleind1, 2);
} 
if ( rec('200','a') =~ m/^L'/ ) { 
	marc_indicators('245', $titleind1, 2);
} 


### 245
## postoji modify
## da li treba makivati razmake u inicijalima?

#_debug(3);

marc_compose('245',
	'a', suffix(
		( ! ( rec('200','d') || rec('200','e') || rec('200','k') ) ) && ( rec('200','f') ) ? ' /' :  
		( rec('200','d') ) ? ' =' :
		( rec('200','e') ) ? ' :' :
		( rec('200','k') ) ? ' ;' :
			'.', 
			rec('200','a'),
	),
	'b', suffix(
		( rec('200','d') && rec('200','f') ) ? ' /' : 
		( rec('200','d') && rec('200','c') ) ? '.'  : 
			'',
			rec('200','d'),
	),
	'b', suffix(
		( rec('200','e') && rec('200','f') ) ? ' /' : 
		( rec('200','e') && rec('200','c') ) ? '.'  : 
			'',
			rec('200','e'),
	),
	'b', suffix(
		( rec('200','k') && rec('200','f') ) ? ' /' : 
		( rec('200','k') && rec('200','c') ) ? '.'  : 
			'',
			rec('200','k'),
	),
	'c', suffix(
		( rec('200','f') && rec('200','c') ) ? '. ' :
			'',
			join_with(' ; ',
				regex('s/(\S\.)\s(\S\.\s)/$1$2/g',
					rec('200','f'),
				),
				regex('s/(\S\.)\s(\S\.\s)/$1$2/g',
					rec('200','g')
				)
			)
	),
	## append to last subfield
	'+', suffix('.',
		join_with(' / ',
			rec('200','c'),
			rec('200','x')
		)
	),
);

#_debug(0);

### 246
## i1=1 ukoliko pravilo nala�e napomenu, ali napomenu necemo pisati ponovo u 500
## i1=3 ako pravlo na nala�e napomenu
## vidi na wiki
## i2 - pogledati za svaku bazu sto su ti naslovi, pa onda oderditi indikatoda oderditi indikatoree
marc_indicators('246', 3, ' ');
marc('246','a',
	rec('532')
);

### 250
## zapisima koji nemaju potpolje, dodaje se ^a u modify - provjeriti za svaku bazu
marc_compose('250',
	'a',suffix(
		rec('205','f') ? ' / ' : 
			'',
			rec('205','a')
	),
	'b', rec('205','f'),
);

### 260
## ponovljiva potpolja u originalnom redosljedu - priprema u modify
marc_original_order('260', '210');


### 300
## urediti interpunkcije za sve kombinacije
marc('300','a',
	suffix(
		rec('215','c') ? ' : ' :
		rec('215','d') ? ' ; ' :
		rec('215','e') ? ' + ' :
			'',
			rec('215','a')
	)
);
marc('300','b',
	suffix(
		( rec('215','c') && rec('215','d') ) ? ' ; ' :
		( rec('215','c') && rec('215','e') ) ? ' + ' :
			'',
			rec('215','c')
	)
);
marc('300','c',
	suffix(
		rec('215','e') ? ' + ' :
			'',
			rec('215','d')
	)
);
marc('300','e',
	rec('215','e')
);

### 362 - broj�ani podaci za nakladni�ku cjelinu - to mi nemamo, polje se koristi samo za �asopise
## i1 = 0 - formalizirani oblik navoda
## i1 = 1 - neformalizirani
## i2 nema

# if ( ! rec('225','z')  ) {

# ako je naslov nakladni�ke cjeline slo�en, podatak se zapisuje u 490, s pripadaju�im 830

#if (  frec('225','a') =~ m/[\/=:]/  ) {
#
##_debug(3);
#
#### 490
### 490: Subfield _p is not allowed. 
#
#marc_indicators('490','0',' ');
#
#marc_template(
#	from => 225, to => 490,
#	subfields_rename => [
#		'a' => 'a',
#	 	'x' => 'x',
#		'v' => 'v',
#		'h' => 'n',
#		'i' => 'p',
#		'w' => 'v',
#	],
#	isis_template => [
#		'a',
#		'a ;|v',
#		'a,|x ;|v',
#		'a,|x ;|w',
#		'a,|x.|h ;|w',
#		'a.|h,|i ;|v',
#		'a ;|v.|i',
#		'a.|i ;|w',
#		'a,|x',
#		# greske:
#		'i ;|v',
#		'i'
#	],
#);

#marc_template(
#	from => 225, to => 490,
#	  from => "{ z => 1 }",
#	    to => "{ a => 1 }",
#
#	marc_template => [
#		'a',
#	],
#);

# marc_indicators('830',' ','0');
# i2 ovisi �lanu - popraviti 
#
# marc('830','a',
#	regex('s/(^.*)\s\/\s(.*)/$1 ($2)/g',
#		rec('225','a')
#	)
# );


#} else {

### 440 - indikatori
## 2.ind. prema jeziku i �lanu, uskladiti s 245

marc_indicators('440', ' ', '0');

if ( rec('225','a') =~ m/^Einen / ) {
		marc_indicators('440', ' ', 6);
} 

if ( rec('225','a') =~ m/^Eine / ) {
		marc_indicators('440', ' ', 5);
} 

if ( rec('225','a') =~ m/^(Die|Das|Der|Ein|Les|The) / ) {
		marc_indicators('440', ' ', 4);
} 

if ( rec('225','a') =~ m/^(Um|Un|An|La|Le|Il) / ) { 
		marc_indicators('440', ' ', 3);
} 

if ( rec('225','a') =~ m/^(A|L') / ) { 
		marc_indicators('440', ' ', 2);
} 

## 440v nije ponovljivo

marc_template(
	from => 225, to => 440,
	subfields_rename => [
		'a' => 'a',
	 	'x' => 'x',
		'v' => 'v',
		'h' => 'n',
		'i' => 'p',
		'w' => 'v',
	],
	isis_template => [
		'a',
		'a,|x',
		'a,|x ;|v',
		'a,|x.|i',
		'a,|x.|i ;|w',
		'a,|x.|h ;|w',
		'a,|x.|h,|i ;|w',
		'a,|x ;|v.|h ;|w',
		'a,|x ;|v.|h,|i ;|w',
		'a ;|v',
		'a ;|v.|i',
		'a ;|v.|h,|i',
		'a ;|w',
		'a ;|v.|i ;|w',
		'a ;|v.|h,|i ;|w',
		'a ;|v.|h ;|w',
		'a.|h,|i',
		'a.|h,|i ;|w',
		'a.|i',
		'a.|i ;|w',
		# gre�ke:
		'a ;|v ;|w',
		'a.|h',
		'a.|h ;|w',
		'a.|h ;|v',
		'h,|i',
		'h ;|v',
		'i ;|w',
		'v',	
		'w',
	],
);

#}

#marc_template(
#	from => 225, to => 490,
#	subfields_rename => [
#		'a' => 'a',
#	 	'x' => 'x',
#		'v' => 'v',
#		'h' => 'n',
#		'i' => 'p',
#		'w' => 'v',
#	],
#	isis_template => [
#		'a.|h,|i ;|w',
#		'a.|h.|w',
#		'a.|i ;|w',
#		'a ;|v.|i',
#		'a ;|v.|i ;|w',
#		'a ;|v.|h,|i ;|w',
#		'a ;|v.|h ;|w',
#		'a ;|v ;|w',
#		#mozda greska:
#		'a ;|v,|x ;|w',
#		'a ;|v,|x.|p',
#		'a,|x ;|v',
#		'a,|x.|p ;|w',
#	],
#	  from => "{ a => 1, i => 1, w => 1, \"x\" => 1 }",
#	    to => "{ a => 1, p => 1, v => 1, \"x\" => 1 }",
#
#	marc_template => [
#		'a',
#		'a ;|v',
#		'a,|x',
#		'a.|n',		# mozda greska 
#		'a.|n,|p',
#		'a.|p',
#		'a ;|v.|n',
#		'a ;|v.|n,|p',	# mozda greska
#		'n,|p',
#		'n ;|v',	# mozda greska
#		'p ;|v',
#		'v',
#		'x',
#		'x ;|v'		# greska
#	],
#);


## marc_compose za 440 radi samo kad polje ni potpolja nisu ponovljiva. ne zapisuje dobro drugo pojavljivanje ^v, tj. ^v iza ^p (iz 225^w)

#marc_compose('440',
#	'a', suffix(
#		( rec('225','v') ) ? ' ;' :
#		( rec('225','w') ) ? ' ; ' :
#		( rec('225','x') ) ? ',' :
#		( rec('225','h') || rec('225','i') ) ? '.' :
#			'.',
#			frec('225','a')
#	),
#	'x',suffix(
#		( rec('225','x') && ( rec('225','h') || rec('225','i') ) ) ? '.' :
#			'',
#			frec('225','x')
#	),
#	'v',suffix(
#		( rec('225','v') && ( rec('225','h') || rec('225','i') ) ) ? '.' :
#		( rec('225','v') && rec('225','w') ) ? ' ; ' :
#			'',
#			frec('225','v')
#	),
#	'n',suffix(
#		( rec('225','h') && rec('225','i') ) ? ',' :
#		( rec('225','h') && rec('225','w') ) ? ' ; ' :
#			'',
#			frec('225','h')
#	),
#	'p',suffix(
#		( rec('225','i') && rec('225','w') ) ? ' ; ' :
#		( rec('225','i') && rec('225','x') ) ? ',' :
#			'',
#			frec('225','i')
#	),
## 225w treba ici u 440v, na kraj
#	'+', join_with(' ; ',
#		rec('225','w'),
#	)
#);

#marc_original_order('440','225');


#if (
#	count( lookup(
#		sub { 1 },
#		'ffps','libri',
#		sub { rec('225','a') }
#	) ) > 1
#) {


#_debug(0);


### 500

marc('500','a',
	rec('300')
);

marc('500','a',
	rec('305')
);

marc('500','a',
	rec('307')
);

marc('500','a',
	rec('314')
);

### 502 - bilje�ka o disertacijama
## odgovaraju�eg polja nema u originalnim podacima. mo�da se mo�e pretpostaviti?

### 504

marc('504','a',
	rec('320')
);

### 505
## FFPS 327 - sadrzaj
## NSK konvertira 327 (NP) u 501 (R)
## u LOC konv. tablici stoji 327 (NP) u 505 (NR)
## standard i validacije dopu�taju 501 (R) i 505 (R)
## za svaku bazu posebno provjeriti sta je u poljima 327 i 330
if ( config() =~ m/ffps|fftu/ ) {
	marc_indicators('505', '0', ' ');
	marc('505','a',
		regex('s/\s*[\\r\\n]+\s*\**\s*/ ; /g',
			rec('327')
		)	
);
}

if ( config() =~ m/ffan/ ) {
	marc_indicators('505', '0', ' ');
	marc('505','a',
		regex('s/[<>]//g',
			rec('330')
		)	
);
}

### 520
## NSK konvertira 330 (P) u 520 (P) = LOC konv. tablica
if ( config() =~ m/ffps/ ) { 
	marc('520','a',
		regex('s/[\\r\\n]+/. /g',
			rec('330')
		)	
	);
}

### 526 - STUDY PROGRAM INFORMATION NOTE
## u nekim bazame je to u 996 - provjeriti za svaku bazu sta je u 996?
marc_indicators('526', 8, ' ');
marc('526','a',
	rec('996')
);

### 538 - zahtjevi sustava
marc('538','a',
	rec('337','a')
);

### 653 - Index Term-Uncontrolled
## da li ponovljivo polje ili potpolje, pogledati u Koha tags
marc_indicators('653', ' ', ' ');
marc('653','a',
	rec('610')
);

### 655 - Index Term-Genre/Form 
marc_indicators('655', ' ', 4);
marc('655','a',
	rec('608')
);

#_debug(2);


### 700
## ako je u originalu ponovljivo polje treba bioti i konvertirano u ponovljivo !!
## ako ima samo podpolje a onda je pseudonim - nakon konverzije treba provjeriti
if ( rec('701','a') =~ m/, / ) {
	marc_indicators('700', 1, ' ');
} else {
	marc_indicators('700', 0, ' ');
}

marc('700','a',
	rec('701', 'a'),
);

marc('700','a',
	join_with(', ',
		rec('701', 'c'),
		rec('701', 'd')
	),
);
marc('700','a',
	join_with(', ',
		rec('701', 'e'),
		rec('701', 'f')
	),
);
marc('700','a',
	join_with(', ',
		rec('701', 'g'),
		rec('701', 'h')
	),
);
marc('700','a',
	rec('702','a'),
);
marc('700','a',
	join_with(', ',
		rec('702','c'),
		rec('702','d')
	)
);
marc('700','a',
	join_with(', ',
		rec('702','e'),
		rec('702','f')
	)
);
marc('700','a',
	join_with(', ',
		rec('702','g'),
		rec('702','h')
	)
);

#_debug(0);

## eventualno nadopuniti 710 i 711 - provjetiti da li u podacima postoji u ISIS-u


### 740 - Added Entry - Uncontrolled Related/Analytical Title 
## raspraviti
## nadopuniti prvi indikator prema �lanovima
marc_indicators('740', '0', ' ');

# if ( ! rec('464') ) {
# marc('740','a',
# 	rec('200','c')
# );

marc('740','a',
	rec('200','k')
);

# } else {

#marc('740','a',
#	rec('464','a')
#);

# };

### 760 w ---------------------------------------------
## generiranje zapisa o nakladni�koj cjelini i povezivanje podre�enog zapisa s nadre�enim

#if (
#	rec('225','a')	# da li je nakladni�ka cjelina?
#) {

#my $series_key =
#join_with('',
#	rec('225','a'),
#	rec('210','a'),
#	rec('210','b'),
#);
#
#my $series = get( $series_key );
#
#if ($series) {
#	warn "nije novi";
#} else {
#
#$series = join_with('',
#	uc( config() ),
#	' LS',
#	rec('000')
#);
#
#set( $series_key => $series );
#
#} # $series
#} # nakladni�ka cjelina
#
#my $series_key = join_with('',
#	rec('225','a'),
#	rec('210','a'),
#	rec('210','b'),
#);
#
#if ($series_key) {
#
#	marc_indicators('760', 0, ' ');
#	
#	marc('760','t',
#		rec('225','a')
#	);
#	marc('760','w',
#		get( $series_key )
#	);
#}

# ## testing
#
# my $dup_key = join_with('',
#	rec('200','a'),
#);
#
#if ($dup_key) {
#	marc('995','a',
#		get( $dup_key )
#	);
#}
#
#
# marc_indicators('776', 0, ' ');
# 
# marc('776','a',
#	rec('452','1')
# );
#
# -------------------------------------------------------------------

### 774 - Constituent Item Entry - to bi trebali koristiti ali ne koristimo jer NSK to koristi za ne�to drugo
### 787 - Nonspecific Relationship Entry 

#marc_indicators('787','0',' ');
#
#marc_compose('787',
#	'g', rec('230','v'),
#	't', rec('230','a'),
#	'b', rec('240','a'),
#	'd', rec('250','a'),
#	'd', rec('250','d'),
#	'h', rec('260','a'),
#	'k', rec('270','a'),
#	'n', join_with('. - ',
#		rec('280'),
#	),
#	'x', join_with(', ',
#		rec('290'),
#	)
#);
#marc_compose('787',
#	'g', rec('231','v'),
#	't', rec('231','a'),
#	'b', rec('241','a'),
#	'd', rec('251','a'),
#	'd', rec('251','d'),
#	'h', rec('261','a'),
#	'k', rec('271','a'),
#	'n', join_with('. - ',
#		rec('281'),
#	),
#	'x', join_with(', ',
#		rec('291'),
#	)
#);
#marc_compose('787',
#	'g', rec('232','v'),
#	't', rec('232','a'),
#	'b', rec('242','a'),
#	'd', rec('252','a'),
#	'd', rec('252','d'),
#	'h', rec('262','a'),
#	'k', rec('272','a'),
#	'n', join_with('. - ',
#		rec('282'),
#	),
#	'x', join_with(', ',
#		rec('292'),
#	)
#);
#marc_compose('787',
#	'g', rec('233','v'),
#	't', rec('233','a'),
#	'b', rec('243','a'),
#	'd', rec('253','a'),
#	'h', rec('263','a'),
#	'k', rec('273','a'),
#	'n', join_with('. - ',
#		rec('283'),
#	),
#	'x', join_with(', ',
#		rec('293'),
#	)
#);


### 852 - ne koristimo, koristimo  942 i 952

### 876 - item information - basic bibliographic unit (R)
## da li ovdje zapisati stare inventarne brojeve?
#marc('876','a',
#	rec('991','t'),
#);

### 886 - former marc
# inicijale treba prebaciti u neko lokalno polje, a ovo polje nam ne treba
#
# marc_indicators('886', 2, ' ');
# marc('886','2',
#	'ffmarc'
# );
# marc('886','a',
#	'994'
# );
# marc('886','b',
#	join_with(''.
#		'##^a',
#		join_with('',
#			prefix('^a',
#				rec('994','a'),
#			),
#			prefix('^b',
#				rec('994','b')
#			)
#		)
#	)
#);

### KOHA items

marc('942','b',
	'LIB'
);

if ( grep( m/Posebni otisak/, rec('300') ) ) {
	marc('942','c',
		'SEP'
	);
} else {
	marc('942','c',
		'KNJ'
	);
}

## 990 u sk ima \r\n na kraju

marc('942','d',
	join_with(' | ',
		rec('990')
	)
);	

my $novasig1 =
	lookup(
		sub { rec('C') },
		'ffiz','mapirano',
		sub { rec('B') },
		sub { rec('990') }
	);
my $novasig2 = 
	lookup(
		sub { rec('C') . ' ' . rec('D') },
		'ffiz','starasig',
		sub { rec('A') },
		sub { rec('000') },
	);

marc('942','h',
	$novasig1
);

marc('942','i',
	$novasig2
);

if ( config() =~ m/ffar/ ) {
	my $novasig1 =
		lookup(
			sub { rec('E') },
			'ffar','mapirano',
			sub { rec('A') },
			sub { rec('000') }
		);
	if ( $novasig1 ) {
		marc('942','h',
			$novasig1
		);
	} else {
		marc('942','h',
			'ARH'
		);
	}


}

if ( config() =~ m/ffsf|ffpo|fffi|ffar|ffsk/ ) {
#	my $sig1 = 
#		lookup(
#			sub { rec('C') },
#			'fffi','signature',
#			sub { rec('B') },
#			sub { frec('990') },
#		);
	if ( rec('700','a') ) {
#		marc('942','h',
#			$sig1,
#		);
		if ( rec('200','a') =~ m/^(The|Die|Das|Der|Ein|Les) / ) {
			marc('942','i',
				join_with(' ',
					regex('s/(^.{3}).*/$1/',
						uc ( rec('700','a') )
					),
					regex('s/^.{4}(.{1}).*/$1/',
						lc ( rec('200','a') )
					)
				)
			);
		} elsif ( rec('200','a') =~ m/^(Um|Un|An|La|Le|Il) / ) {
			marc('942','i',
				join_with(' ',
					regex('s/(^.{3}).*/$1/',
						uc ( rec('700','a') )
					),
					regex('s/^.{3}(.{1}).*/$1/',
						lc ( rec('200','a') )
					)
				)
			);
		} elsif ( rec('200','a') =~ m/^Eine / ) {
			marc('942','i',
				join_with(' ',
					regex('s/(^.{3}).*/$1/',
						uc ( rec('700','a') )
					),
					regex('s/^.{5}(.{1}).*/$1/',
						lc ( rec('200','a') )
					)
				)
			);
		} elsif ( rec('200','a') =~ m/^Einen / ) {
			marc('942','i',
				join_with(' ',
					regex('s/(^.{3}).*/$1/',
						uc ( rec('700','a') )
					),
					regex('s/^.{6}(.{1}).*/$1/',
						lc ( rec('200','a') )
					)
				)
			);
		} elsif ( rec('200','a') =~ m/^(A|L) / ) {
			marc('942','i',
				join_with(' ',
					regex('s/(^.{3}).*/$1/',
						uc ( rec('700','a') )
					),
					regex('s/^.{2}(.{1}).*/$1/',
						lc ( rec('200','a') )
					)
				)
			);
		} else {
			marc('942','i',
				join_with(' ',
					regex('s/(^.{3}).*/$1/',
						uc ( rec('700','a') )
					),
					regex('s/(^.{1}).*/$1/',
						lc ( rec('200','a') )
					)
				)
			);
		}
	} elsif ( rec('710','a') ) {
#		marc('942','h',
#			$sig1,
#		);
		if ( rec('200','a') =~ m/^(The|Die|Das|Der|Ein|Les) / ) {
			marc('942','i',
				join_with(' ',
					regex('s/(^.{3}).*/$1/',
						uc ( rec('710','a') )
					),
					regex('s/^.{4}(.{1}).*/$1/',
						lc ( rec('200','a') )
					)
				)
			);
		} elsif ( rec('200','a') =~ m/^(Um|Un|An|La|Le|Il) / ) {
			marc('942','i',
				join_with(' ',
					regex('s/(^.{3}).*/$1/',
						uc ( rec('710','a') )
					),
					regex('s/^.{3}(.{1}).*/$1/',
						lc ( rec('200','a') )
					)
				)
			);
		} elsif ( rec('200','a') =~ m/^Eine / ) {
			marc('942','i',
				join_with(' ',
					regex('s/(^.{3}).*/$1/',
						uc ( rec('710','a') )
					),
					regex('s/^.{5}(.{1}).*/$1/',
						lc ( rec('200','a') )
					)
				)
			);
		} elsif ( rec('200','a') =~ m/^Einen / ) {
			marc('942','i',
				join_with(' ',
					regex('s/(^.{3}).*/$1/',
						uc ( rec('710','a') )
					),
					regex('s/^.{6}(.{1}).*/$1/',
						lc ( rec('200','a') )
					)
				)
			);
		} elsif ( rec('200','a') =~ m/^(A|L) / ) {
			marc('942','i',
				join_with(' ',
					regex('s/(^.{3}).*/$1/',
						uc ( rec('710','a') )
					),
					regex('s/^.{2}(.{1}).*/$1/',
						lc ( rec('200','a') )
					)
				)
			);
		} else {
			marc('942','i',
				join_with(' ',
					regex('s/(^.{3}).*/$1/',
						uc ( rec('710','a') )
					),
					regex('s/(^.{1}).*/$1/',
						lc ( rec('200','a') )
					)
				)
			);
		}
	} elsif ( rec('200','a') =~ m/^(The|Die|Das|Der|Ein|Les) / ) {
#		marc('942','h',
#			$sig1,
#		);
		marc('942','i',
			regex('s/^.{4}(.{3}).*/$1/',
				uc ( rec('200','a') )
			),
		);
			
	} else {
#		marc('942','h',
#			$sig1,
#		);
		marc('942','i',
			regex('s/(^\w{3}).*/$1/',
				uc ( rec('200','a') )
			)
		)
	}
} # if config() za oznaku primjerka

if ( ( rec('230') || rec('231') || rec('232') || rec('233') ) ) { 
	warn "skupni zapis - nema primjeraka";
} else {
	marc('952','8',
		rec('991','8')
	);
	marc('952','a',
		rec('991','a')
	);
	marc('952','b',
		rec('991','b')
	);
	marc('952','c',
		rec('991','c'),
	);
	marc('952','i',
		rec('991','i')
	);
	marc('952','y',
		rec('991','y')
	);
}

### LOKALNA POLJA
## 1. da li je zapis pregledan ili nije i datum kad je pregledan
##    provjeriti za svaku bazu koje polje koristi
##    SFM:	999^aP
##    AN:	999 Pregledan
## raspraviti



} # polje 200a
# } sig
