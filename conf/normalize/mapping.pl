if ( rec('200','a') ) {

# Leader
#
# raspraviti 17,18

# u nekim knji�nicama koristi se 999a i 999b

if ( rec('999') && first(rec('999')) =~ m/pregledan/i) {
	marc_leader('05','c');
} else {
	marc_leader('05','n');
}

	marc_leader('06','a');
	marc_leader('07','m');
	marc_leader('18','i');

if ( rec('230') ) {
	marc_leader('19','r');
}


# 001 polje dodaje Voyager

# polje 035 je u NSK ponovljivo. za�to ???

# privemeno koristimo 0356 umjesto 0359, zato �to marclint 9 prijavljuje kao gre�ku

marc('035','6',
join_with('',
#		config('input normalize path'),
#		config('name'),
#		config('input name'),
		config(),
#		id(),
#		rec('994','a'),
		rec('000')
	)
);

# u marc 035a treba dodati System Number - 
# NSK zapisuje Voyagerov ID (001) - kako ???

# u FFSF bazama postoji nekoliko zaredanih ISBN, odvojenih s '. - '

marc('020','a',
	rec('10')
);

# u polje 040a mo�da jo� dodati prefiks HR
# NSK ima HR NSB (mo�da u novijim zapisima ima NSK?)

marc('040','a',
	'FFZG'
);

marc('040','b',
	'hrv'
);

if (rec('300') && first(rec('300')) =~ m/prijevod/i) {
	marc_indicators('041', 1, ' ');
} else {
	marc_indicators('041', 0, ' ');
};

marc_repeatable_subfield('041','a',
	rec('101')
);

# marc_indicators('245', 0, 0);

if ( rec('700') ) {
	marc_indicators('245', 1, 0);
} else {
	marc_indicators('245', 0, 0);
};

#_debug(3);

marc_compose('245',
	'a', suffix(
		rec('200','b') ? '. '	:
		rec('200','k') ? ' ; '	:
		rec('200','d') ? ' = '	:
		rec('200','e') ? ' : '	:
		rec('200','f') ? ' / '	:
				 '.',
			rec('200','a')
	),
	'h', rec('200','b'),
	'b', 
		join_with(' ; ',
			rec('200','k'),
		),
	'b', rec('200','d'), 
	'b', 
		join_with(' : ',
			rec('200','e'),
		),
	'c', 
		join_with(' ; ',
			rec('200','f'),
		),
# FIXME append to last subfield
	'+', prefix('. ', rec('200','c') ),
);


#_debug(0);


# marc('245','a',
# 	rec('200','a')
# );
# 
# marc('245','b',
# 	join_with(' : ',
# 		rec('200','d'),
# 		rec('200','e')
# 	)
# );
# 
# marc('245','c',
# 	join_with(' ; ',
# 		rec('200','f'),
# 		rec('200','g')
# 	)
# );

marc('250','a',
	rec('205','a')
);

# FIXME ponovljiva potpolja u originalnom redosljedu
# 
marc_original_order('260', '210');

# marc('260','a',
# 	rec('210','a'),
# );
#
# marc('260','b',
# 	rec('210','c'),
# );

# marc('260','c',
# 	rec('210','d'),
# );

# marc_compose('260',
#	'a', split_rec_on('210','a', qr/\s*;\s*/, 1),
#	'b', first(rec('210','c')),
#	'a', split_rec_on('210','a', qr/\s*;\s*/, 2),
#	'c', first(rec('210','d')),
# );

marc('300','a',
	rec('215','a')
);

marc('300','b',
	rec('215','c')
);

marc('300','c',
	rec('215','d')
);

marc('300','e',
	rec('215','e')
);


marc_indicators('490', 1, ' ');
marc('490','a',
	join_with(' : ',
		rec('225','a'),
		join_with(" =  ",
			rec('225','d'),
			join_with(" / ",
				rec('225','e'),
				rec('225','f')
			)
		)
	)
);

marc('490','v',
#	join2_with(225, 'v', ' , ', 225,'w');
	join_with(', ',
		rec('225','v'),
		rec('225','w')
	)
);

marc('500','a',
	rec('300')
);

marc('500','a',
	rec('314')
);

marc('504','a',
	rec('320')
);

# napomene uz konverziju polja sa sadr�ajem:
# NSK konvertira 327 (NP) u 501 (R)
# u LOC konv. tablici stoji 327 (NP) u 505 (NR)
# standard i validacije dopu�taju 501 (R) i 505 (R)
# FFPS koristi 327 (P)
# FFIZ koristi 327


marc_indicators('505', '0', ' ');
marc('505','a',
	regex('s/\s*[\\r\\n]+\s*\**\s*/ ; /g',
		rec('327')
	)
);

# napomene uz konverziju polja 330:
# NSK konvertira 330 (P) u 520 (P) = LOC konv. tablica
# FFIZ koristi 330 (P) - klju�ne rije�i, dijelovi sadr�aja, sa�eci
# FFFO koristi 330 (P) - klju�ne rije�i, dijelovi sadr�aja 
# FFPS koristi 330 (NP) - sa�etak

marc('520','a',
	regex('s/[\\r\\n]+/. /g',
		rec('330')
	)
);

# mo�da i polje 520y - jezik napomene ako je uvijek isti jezik

marc_indicators('655', ' ', 4);
marc('655','a',
	rec('608')
);

marc_indicators('653', ' ', ' ');
marc('653','a',
	rec('610')
);

# 675 podpolja c, d ... - �to s njima ???

marc('080','a',
	rec('675','a')
);

# 084 je other classification (R)
# NSK ga koristi za stru�nu oznaku NSK iz polja 681
# NSK definira i prelazak polja 686 u 084, u skladu s LOC konv. tablicom
#
# polje 686 koristi FFPS

marc('084','a',
	rec('686')
);

marc_indicators('100', 1, ' ');
marc('100','a',
	join_with(', ',
		rec('700', 'a'),
		rec('700', 'b')
	)
);

#_debug(2);

marc_indicators('700', 1, ' ');

if ( rec('701') ) { 
marc('700','4', 
	'aut'
); 
}

marc('700','a',
	rec('701', 'a'),
);

marc('700','a',
	join_with(', ',
		rec('701', 'c'),
		rec('701', 'd')
	)
);

marc('700','a',
	join_with(', ',
		rec('701', 'e'),
		rec('701', 'f')
	)
);

marc('700','a',
	join_with(', ',
		rec('701', 'g'),
		rec('701', 'h')
	)
);


marc('700','a',
	rec('702','a'),
);


# ako je u originalu ponovljivo polje treba biti i konvertirano!!


marc('700','a',
	join_with(', ',
		rec('702','c'),
		rec('702','d')
	)
);

marc('700','a',
	join_with(', ',
		rec('702','e'),
		rec('702','f')
	)
);

marc('700','a',
	join_with(', ',
		rec('702','g'),
		rec('702','h')
	)
);

#_debug(0);


if ( rec('710','f') ) {

marc_indicators('111', 2, ' ');
marc('111','a',
	rec('710','a')
);

marc('111','c',
	rec('710','e')
);

marc('111','d',
	rec('710','f')
);

marc('111','e',
	rec('710','b')
);

marc('111','n',
	rec('710','d')
);


} else {
	
marc_indicators('110', 2, ' ');
marc('110','a',
	rec('710','a')
);

marc('110','c',
	rec('710','c')
);

};


marc_indicators('740', 0, ' ');

if ( ! rec('464') ) {
marc('740','a',
	rec('200','c')
);

marc('740','a',
	rec('200','k')
);

} else {
marc('740','a',
	rec('464','a')
);
};



## vi�erazinci 

if ( rec('230') ) {

	marc_duplicate();
 	
	marc_remove('245');
	marc_compose('245',
	'a', suffix(
		rec('200','b') ? '. '	:
		rec('200','k') ? ' ; '	:
		rec('200','d') ? ' = '	:
		rec('200','e') ? ' : '	:
		rec('200','f') ? ' / '	:
				 '.',
			rec('230','a')
	),
	'b', 
		join_with(' ; ',
			rec('230','k'),
		),
	'b', rec('230','d'), 
	'b', 
		join_with(' : ',
			rec('230','e'),
		),
	'c', 
		join_with(' ; ',
			rec('230','f'),
		),
	);

	marc_remove('260');
	marc('260','c',
		rec('250')
	);

	marc_remove('300');
	marc('300','a',
		rec('260')
	);

	marc_remove('490');
	marc('490','a',
		rec('270')
	);

	marc_remove('500');
	marc('500','a',
		rec('280')
	);

	marc_remove('020');
	marc('020','a',
		rec('290')
	);

	marc_indicators('774', 0, ' ');
	marc('774','6',
		join_with('',
			config(),
			rec('000')
		)
	);

	marc('774','g',
		rec('230','v')
	);
 
};

}
