sub csv { to('csv',@_) }

if ( rec('200','a') ) {

## 022

if ( ! rec('225','a') ) {
	csv('A',
		rec('11')
	);
}

csv('B',
	join_with('',
		uc( config() ),
		' P',
		rec('000')
	)
);

csv('C',
	rec('101')
);


csv('D',
	rec('675','a')
);

csv('E',
	join_with(', ',
		rec('700', 'a'),
		rec('700', 'b')
	)
);

csv('F',
	rec('710','a')
);

csv('G',
	rec('710','c')
);

csv('H',
	lookup(
	 	sub { rec('530','a') },
 		'nsk','bbaza',
 		sub { rec('011','a') },
 		sub { rec('11') },
 	) 
);

csv('I',
	rec('200','a')
);

csv('J',
	rec('200','d')
);

csv('K',
	rec('200','e')
);


csv('L',
	rec('532')
);


csv('M',
	join_with(' / ',
		rec('205','a'),
		rec('205','f')
	)
);

csv('N',
	rec('210','a')
);

csv('O',
	rec('210','c')
);

csv('P',
	rec('210','d')
);

csv('R',
	rec('215','d')
);

csv('S',
	rec('326')
);

csv('T',
	rec('207','a')
);

csv('U',
	rec('300')
);

csv('V',
	rec('610')
);

csv('W',
	join_with(', ',
		rec('702','a'),
		rec('702','b'),
	),
);


csv('Z',
	join_with(' ',
		rec('990')
	)
);

csv('X',
	rec('991','a')
);

csv('Y',
	join_with(' ; ', 
		rec('992')
	)
);
}
