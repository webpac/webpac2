tag('label',
	rec('035','6')
);

tag('ISSN',
	rec('022','a')
);

#tag('Jezik',
#	rec('041','a')
#);

tag('Zemlja',
	rec('044','a')
);

tag('UDK',
	rec('080','a')
);

#tag('APA CC',
#	rec('084','a')
#);

tag('Autori',
	join_with(' ; ',
		rec('100','a'),
		join_with(' ; ',
			rec('700','a')
		)
	)
);

tag('Naslov',
	join_with('',
		rec('245','a'),
		join_with('',
			rec('245','b'),
			rec('245','c')
		)
	)
);

tag('Ucestalost',
	rec('310','a')
);

tag('Brojcani podaci',
	rec('362','a')
);

#tag('Nakladnicka cjelina',
#	rec('440','a')
#);

#tag('Izdanje',
#	rec('250','a')
#);

#tag('Impresum',
#	join_with('',
#		rec('260','a'),
#		join_with('',
#			rec('260','b'),
#			rec('260','c')
#		)
#	)
#);


tag('Mjesto izdavanja',
	regex('s/[,;:] $//',
		rec('260','a')
	)
);

tag('Izdavac',
	regex('s/[,;:] $//',
		rec('260','b')
	)
);

tag('Godina',
	rec('260','c')
);

#tag('Materijalni opis',
#	join_with('',
#		rec('300','a'),
#		join_with('',
#			rec('300','b'),
#			rec('300','c')
#		)
#	)
#);

tag('Dimenzije',
	rec('300','c')
);

#tag('Napomena',
#	rec('505','a')
#);

tag('Predmetnice',
	rec('653','a')
);

tag('Urednici',
	rec('700','a')
);

tag('Ustanove',
	rec('710','a')
);

#tag('Signatura',
#	rec('852','a')
#);
#
#tag('Inv.Br',
#	rec('876','a')
#);
#
tag('Fond',
	rec('992','a')
);
