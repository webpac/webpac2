
search_display('ISBN',
	isbn_13( 
		rec('10') 
	)
);

search_display('ISSN',
	rec('11')
);


search_display('Language',
	rec('101','a')
);

search_display('TitleProper',
	join_with(' / ',
		join_with('',
			rec('200','a'),
			rec('200','c'),
		),
		rec('200','f'),
	)
);
	


# regex('s/\s*[=:\/;]\s*//g',

sorted('TitleProper',
	rec('200','a'),
);


#display('PripadajuciOznaka',
#	lookup(
#		sub { [ rec('200','a') . rec('200','c'),  rec(451,'a') ] },
#		'hidra','bib',
#		sub { rec('900') },
#		sub { rec('451','1') }
#	)
#);
#
#display('Fond',
#	lookup(
#		sub { rec('200','a') },
#		'hidra','bib',
#		sub { rec('900') },
#		sub { rec('946','1') }
#	)
#);	


search('PlacePublication',
	rec('210','a')
);

search('NamePublisher',
	rec('210','c'),
);

search_display('DatePublication',
	rec('210','d')
);

display('Impresum',
	rec('210'),
);

search_display('Series',
	rec('225','a')
);

search_display('GeneralNote',
	rec('300')
);

