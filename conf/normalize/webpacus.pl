search_display('ISBN',
	isbn_13(
		rec('020','a')
	)
);

search_display('ISBN_incorrect',
	rec('010','z')
);

search_display('ISSN',
	rec('022','a')
);


search_display('Language',
	rec('041','a')
);

search_display('TitleProper',
	join_with('',
		rec('245','a'),
		join_with(' / ',
			rec('245','b'),
			rec('245','c')
		)
	)
);

# regex('s/\s*[=:\/;]\s*//g',

sorted('TitleProper',
	rec('245','a'),
);

search_display('ReminderTitle',
	join_with(' : ',
		prefix(' = ',
			rec('245','d')
		),
		rec('245','e')
	)
);

display('Pripadajuci',
	lookup(
		sub { rec('245','a') },
		'webpacus','foobar',
		sub { rec('774','w') },
		sub { rec('035','6') }
	)
);


search('PlacePublication',
	rec('260','a')
);

search('NamePublisher',
	rec('260','b'),
);

search_display('DatePublication',
	rec('260','c')
);

display('Impresum',
	rec('260'),
);

search_display('Dimensions',
	rec('300','c')
);

search_display('GeneralNote',
	rec('500')
);

