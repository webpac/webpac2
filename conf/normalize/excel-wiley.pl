marc_leader('05','n');
marc_leader('06','a');
marc_leader('07','s');
marc_leader('18','i');

marc('022','a',
	lookup(
		sub { rec('B') },
		'wiley','full',
		sub { 
			regex('s/\s*(\.|:|\-|\(|\)|and|&)\s*//g',
				lc( rec('A') )
			);
		},
	)
);

marc('022','y',
	lookup(
		sub { rec('C') },
		'wiley','full',
		sub { 
			regex('s/\s*(\.|:|\-|\(|\)|and|&)\s*//g',
				lc( rec('A') )
			);
		},
	)
);

marc_indicators('245', 1, 0);

marc('245','a',
	rec('A')
);

marc('245','h',
	'[ Elektroni�ka gra�a ]'
);

marc('500','a',
	lookup(
		sub { 'Full Text dostupan od: ' . rec('V') },
		'wiley','full',
		sub { 
			regex('s/\s*(\.|:|\-|\(|\)|and|&)\s*//g',
				lc( rec('A') )
			);
		},
	)
);

marc('856','u',
	lookup(
		sub { rec('D') },
		'wiley','full',
		sub { 
			regex('s/\s*(\.|:|\-|\(|\)|and|&)\s*//g',
				lc( rec('A') )
			);
		},
	)
);

