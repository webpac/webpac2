### Leader

marc_leader('05','n');
marc_leader('06','a');
#if ( rec('990') ) {
#	marc_leader('07','a');
#} else {
	marc_leader('07','b');
#};
marc_leader('18','i');

### 007
marc_fixed('007',00,'ta');

### 008

### 035
# privemeno koristimo 0356 umjesto 0359, zato �to marclint 9 prijavljuje kao gre�ku

marc('035','6',
join_with('',
		config('input normalize path'),
#		config('name'),
#		config('input name'),
#		config(),
#		id(),
#		rec('994','a'),
#		rec('000')
#	)
#);


### 020

marc('020','a',
	rec('10')
);

### 040

marc('040','a',
	'HR FFZG'
);

marc('040','b',
	'hrv'
);

marc('040','e',
	'HR PPIAK'
);

### 245 indikatori

marc_indicators('245', 0, 0);

#_debug(3);

marc_compose('245',
	'a', suffix(
		rec('203','b') ? '. '	:
		rec('203','k') ? ' ; '	:
		rec('203','d') ? ' = '	:
		rec('203','e') ? ' : '	:
		rec('203','f') ? ' / '	:
				 '.',
			join_with('. ',
				rec('203','a')
			)
	),
	'h', rec('203','b'),
	'b', 
	join_with(' ; ',
		rec('203','k'),
	),
	'b', 
	join_with(' = ',
		rec('203','d'), 
	),
	'b', 
	join_with(' : ',
		rec('203','e'),
	),
	'c', 
	join_with(' ; ',
		rec('203','f'),
	),
	'+', prefix('. ', rec('203','c') ),
);

marc_original_order('260', '210');

marc('520','a',
	regex('s/[\\r\\n]+/. /g',
		rec('330')
	)
);

marc_indicators('653', ' ', ' ');
marc('653','a',
	rec('610')
);

# 675 podpolja c, d ... - �to s njima ???

marc('080','a',
	rec('675','a')
);

marc_indicators('100', 1, ' ');
marc('100','a',
	join_with(', ',
		rec('700', 'a'),
		rec('700', 'b')
	)
);

marc_indicators('700', 1, ' ');

if ( rec('701') ) { 
marc('700','4', 
	'aut'
); 
}

marc('700','a',
	rec('701', 'a'),
);

marc('700','a',
	join_with(', ',
		rec('701', 'c'),
		rec('701', 'd')
	)
);

marc('700','a',
	join_with(', ',
		rec('701', 'e'),
		rec('701', 'f')
	)
);

marc('700','a',
	join_with(', ',
		rec('701', 'g'),
		rec('701', 'h')
	)
);


marc('700','a',
	rec('702','a'),
);


# ako je u originalu ponovljivo polje treba biti i konvertirano!!


marc('700','a',
	join_with(', ',
		rec('702','c'),
		rec('702','d')
	)
);

marc('700','a',
	join_with(', ',
		rec('702','e'),
		rec('702','f')
	)
);

marc('700','a',
	join_with(', ',
		rec('702','g'),
		rec('702','h')
	)
);

#_debug(0);


if ( rec('710','f') ) {

marc_indicators('111', 2, ' ');
marc('111','a',
	rec('710','a')
);

marc('111','c',
	rec('710','e')
);

marc('111','d',
	rec('710','f')
);

marc('111','e',
	rec('710','b')
);

marc('111','n',
	rec('710','d')
);


} else {
	
marc_indicators('110', 2, ' ');
marc('110','a',
	rec('710','a')
);

marc('110','c',
	rec('710','c')
);

};

marc_indicators('773', 0, ' ');

marc('773','g',
	rec('200','x'),
);

marc('773','t',
	join_with(' : ',
		rec('200','a'),
		rec('200','e'),
	),
);

marc('773','w',
	lookup(
		sub { 'ffkk-peri-' . rec('000') },
		'ffkk','peri',
		sub { rec('11') }
	) ||
	lookup(
		sub { 'ffkk-peri-' . rec('000') },
		'ffkk','peri',
		sub { first(rec(200,'a')) . ' ' . first(rec('200','e')) },
	) ||

	lookup(
		sub { "ffkk-peri-" . rec('000') },
		'ffkk','peri',
		sub { rec(200,'a') },
#		sub { rec(900,'x') },
	)

);


