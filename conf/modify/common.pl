# this is pseudo hash/yaml format for regex mappings

# FFSF - nekoliko zaredanih ISBN, odvojenih s '. - '

10
  '*'
    'regex:^' => '^a'
    'regex:\(' => ' ('
    'regex:  ' => ' '
    ' | ' => '^a'
    ' ; ' => '^a'
    '. - ' => '^a'
    'ISBN ' => ''
    'ISBN ' => ''

101
  '*'
    'regex:^' => '^a'
    ' ; ' => '^a'

200
  '*'
    '[A]'   => 'A'
    '[An]'   => 'An'
    '[The]' => 'The'
    '[Der]' => 'Der'
    '[Le]'  => 'Le'
    '<' => ''
    '>' => ''
  '^a'
    ' = ' => '^d'
    ' : ' => '^e'
    ' ; ' => '^k'
    '^g' => '^f'
  '^d'
    '^e' => ' : '
    '^k' => ' ; '
    '^e' => ' : '
  '^k'
    '^d' => ' = '
    '^k' => ' ; '
    '^e' => ' : '
  '^e'
    '^d' => ' = '
    '^k' => ' ; '
    '^g' => '^f'
    '^e' => ' : '
    '^e' => ' : '
  '^f'
    '^f' => ' ; '
    '^d' => ' = '
  '*'
    '^g' => ' ; '

205
  '*'
    'regex:^' => '^a'
  '^a'
    ' / ' => '^f'

210
  '*'
    '^b' => '^a'
    '^g' => '^c'
    ' ; ' => '^a'
    ' : ' => '^c'
  '*'
    '^c' => ' : ^b'
    '^d' => '^c'
  '^a'
    '^a' => ' ; ^x'
    '^c' => ', ^c'
  '^x'
    '^a' => ' ; ^x'
    '^a' => ' ; ^x'
    '^a' => ' ; ^x'
    '^a' => ' ; ^x'
  '^b'
    '^a' => ' ; ^x'
    '^a' => ' ; ^x'
    '^a' => ' ; ^x'
    '^a' => ' ; ^x'
    '^c' => ', ^c'
  '*'
    '^x' => '^a'
    '^y' => '^a'
  '^c'
    '. (' => '. ^e('
  '^e'
    ' : ' => ' : ^f'
  '*'
    'regex:$' => '.'

225
  '^a'
    '^e' => ' : '
    '^f' => ' / '
    '^d' => ' = '
    ' ; ' => '^v' 
    ', ISSN ' => '^x'
  '^v'		
    '^e' => ' : '
  '^i'
    '^f' => ' / '
    '^e' => ' : '
  '^v' 
    '^f' => ' / '
    '^d' => ' = '
  '*'
    '^e' => '^a'
610
  '*'
    '_ _' => ' -- '

701
  '*'
    '^b' => ', '
702
  '*'
    '^b' => ', '

991
  'regex:^' => '^8SLF^aFFZG^bFFZG^cSE^islf'
  'regex:$' => '^yKNJ'

220
  'regex:^' => '^8SLF^aFFZG^bFFZG^cSE^islf'
  'regex:$' => '^yKNJ'

221
  'regex:^' => '^8SLF^aFFZG^bFFZG^cSE^islf'
  'regex:$' => '^yKNJ'

222
  'regex:^' => '^8SLF^aFFZG^bFFZG^cSE^islf'
  'regex:$' => '^yKNJ'

223
  'regex:^' => '^8SLF^aFFZG^bFFZG^cSE^islf'
  'regex:$' => '^yKNJ'
