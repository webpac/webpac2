search( 'author',	
	join_with(' ; ',
		rec('100','a'),
		rec('700','a')
	)
);
search( 'title', 
	join_with(' ',
		rec('245','a'),
		rec('245','b'),
		rec('245','c'),
	)
);
search( 'isbn',   	rec('020','a') );
search( 'impressum',   	
	join_with(' ',
		rec('260','a'),
		rec('260','b'),
		rec('260','c'), 
	)
);
if ( my $year = rec('008') ) {
	search( 'year',
		substr( $year,7,4 )
	) if length($year) > 13;
}


search( 'keywords',
	join_with(' ; ',
		rec('653','a')
	)
);
search( 'biblionumber',
	rec('999','c')
);
