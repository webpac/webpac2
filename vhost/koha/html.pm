package koha::html;

use strict;
use warnings;

sub url {
	my ($self,$url) = @_;
	qq{<a href="$url">$url</a>};
}

sub title {
	my ($self,$title,$data) = @_;
	my $biblionumber = $data->{biblionumber} || die; # title only without biblionumber

	my $responsibility = $1 if $title =~ s{(\s+/\s+.+)$}{};

	qq|<a target="koha.ffzg.hr" href="http://koha.ffzg.hr/cgi-bin/koha/opac-detail.pl?biblionumber=$biblionumber">$title</a>$responsibility|;
}

1;
