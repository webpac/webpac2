#!/usr/bin/perl

use warnings;
use strict;

use JSON;
use DBI;

print "Content-type: text/javascript\n\r\n\r";

my $dbh = DBI->connect('dbi:Pg:dbname=dipl','dpavlin','', { RaiseError => 1, AutoCommit => 0 });

my $data = $dbh->selectall_arrayref(q{
	select sum(ttc),ca,count(ca) from citirani group by ca order by count asc ;
});
print "var data = ",to_json(
#	 [ map { $_->[0] } @$data ] 
	$data
),";\n\r";
