search('title', rec('A') );
search('issn',  	
	join_with(' ; ',
		rec('B'),
		rec('C')
	)
);
search('publisher', rec('D') );

my $vol;
if ( rec('F') =~ m/Y/ ) {
	$vol = ', Vol. 1'
} else {
	$vol = '';
}

search('coverage',
	suffix( $vol, rec('E') )
);

search( 'database',	
	config('input description')
);

