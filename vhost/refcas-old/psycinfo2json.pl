tag('title', uc ( rec('A') ) );
tag('issn',  	
	join_with(' ; ',
		rec('B'),
		rec('C')
	)
);
tag('publisher', rec('D') );

my $vol;
if ( rec('F') =~ m/Y/ ) {
	$vol = ', Vol. 1'
} else {
	$vol = '';
}

tag('coverage',
	suffix( $vol, rec('E') )
);

tag( 'database',	
	config('input description')
);

