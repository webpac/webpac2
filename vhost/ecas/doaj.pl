search( 'title', 	rec('A') );
search( 'title-alternative', 	rec('B') );
search( 'issn',  	
	join_with(' ; ',
		rec('F'),
		rec('G')
	)
);
search( 'publisher', 	rec('D') );
search( 'language', 	rec('E') );
search( 'urlf',    	rec('C') );
search( 'language',	rec('E') );
search( 'country',	rec('M') );
search( 'keyword',	rec('H') );
search( 'subject',	rec('L') );
search( 'coverage',
	join_with(' - ',
		rec('I'),
		rec('J')
	)
);
search( 'collection',	
	config('input description')
)
