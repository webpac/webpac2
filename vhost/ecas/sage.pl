search( 'title', 	rec('B') );
search( 'issn',
	join_with(' ; ',
	 	rec('C'),
		rec('D')
	)
);
search( 'coverage',
        join_with(' - ',
                rec('J'),
                rec('K')
        )
);
search( 'url',    	rec('E') );
search( 'urlp',    	rec('E') );
search( 'subject',	
	join_with(' - ',
		rec('F'),
		rec('G')
	)
);
search( 'collection',	'SAGE Humanities and Social Sciences' );	
