my $issn_key =
        rec('F');

my $issn = get( $issn_key );

if ( $issn ) {
        # warn "nije novi";
} else {

$issn = rec('F');

set( $issn_key => $issn );


tag( 'title', 	rec('D') );
tag( 'abbtitle', 	rec('E') );
tag( 'issn',
	join_with(' ; ',
	  	$issn,
		rec('G') 
	)
);
tag( 'publisher', 	'Cambridge' );
tag( 'url',    	'http://journals.cambridge.org/' . lc ( rec('A') ) );
tag( 'urlp',    	'http://journals.cambridge.org/' . lc ( rec('A') ) );
tag( 'collection',	
	config('input description')
)

}
