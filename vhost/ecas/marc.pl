search( 'title', 	
	regex('s/&#237;/í/g',
		rec('245','a') 
	)
);
search( 'issn',   	
	join_with(' ; ',
		rec('022','a'),
		rec('022','y')
	)
);
search( 'publisher',   	
	regex('s/,$//',
		rec('260','b') 
	)
);
search( 'url',		rec('856','u') );	
search( 'urlpe',	rec('856','u') );	
search( 'collection',	
	config('input description')
)

