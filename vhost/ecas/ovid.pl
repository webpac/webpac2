if ( rec('Journal Subset') !~ m/PsycARTICLES/ ) {

search( 'title', 	rec('Source') );
search( 'issn',   	rec('ISSN') );
search( 'url',    	rec('Link') );
search( 'urlp',    	rec('Link') );
search( 'collection', 	
	join_with('. ',
		'Ovid Full Text',
		rec('Journal Subset'),
	)
);

}
