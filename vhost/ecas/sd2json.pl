tag( 'title', 	rec('C') );
tag( 'issn',  	rec('D') );
tag( 'publisher', rec('E') );
tag( 'coverage',
	join_with(' to ',
		rec('H'),
		rec('K'),
	)
);
tag( 'titlechange',	rec('R') );
tag( 'remarks',	rec('S') );
tag( 'url',    	rec('T') );
tag( 'urlp',    	rec('T') );
tag( 'collection',	
	config('input description')
);
