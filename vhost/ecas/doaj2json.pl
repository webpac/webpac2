tag( 'title', 	rec('A') );
tag( 'title-alternative', rec('B') );
tag( 'issn',	rec('F') );
tag( 'issn_e',	rec('G') );
tag( 'publisher', 	rec('D') );
tag( 'urlf',    	rec('C') );
tag( 'language',	rec('E') );
tag( 'country',		rec('M') );
tag( 'keyword',		rec('H') );
tag( 'subject',		rec('L') );
tag( 'coverage',
	join_with('-',
		rec('I'),
		rec('J')
	)
);
tag( 'collection',	
	config('input description')
)
