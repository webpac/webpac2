tag( 'title', 	
	regex('s/&#237;/í/g',
		rec('245','a') 
	)
);
tag( 'issn',	rec('022','y') );
tag( 'issn_e',	rec('022','a') );
		
tag( 'publisher',   	
	regex('s/,$//',
		rec('260','b') 
	)
);
tag( 'url',		rec('856','u') );	
tag( 'urlp',		rec('856','u') );	
tag( 'subject',		rec('650','a') );
tag( 'collection',	
	config('input description')
)

