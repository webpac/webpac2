tag( 'title', 	rec('A') );
tag( 'collection_standard', rec('B') );
tag( 'collection_expanded', rec('C') );
tag( 'issn',	rec('D') );
tag( 'issn_e', rec('E') );
tag( 'coverage', 
	join_with('',
		rec('F'),
		rec('H')
	)
);
tag( 'url', rec('G') );
tag( 'general_info', rec('N') );
tag( 'price_pe_usd', rec('O') );
tag( 'price_e_usd', rec('P') );
tag( 'price_p_usd', rec('Q') );
tag( 'postage', rec('R') );

tag( 'collection',	
	config('input description')
)
