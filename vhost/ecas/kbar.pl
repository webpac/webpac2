search( 'title', 	rec('A') );
search( 'issn',
	join_with(' ; ',
	 	rec('B'),
		rec('C')
	)
);
search( 'coverage', 
	join_with(' - ',
		rec('D'),
		rec('G')
	)
);
search( 'publisher',	rec('P') );
search( 'url',    	rec('J') );
search( 'urlp',    	rec('J') );
search( 'subject',	rec('S') );

if ( rec('R') && config('input description') =~ m/JSTOR/ ) {
	search( 'collection',
		join_with(' ',
			'JSTOR', rec('R')
		)
	);
} elsif  ( rec('R') ) {
	search( 'collection', rec('R') );
} else {
	search( 'collection',
		config('input description')
	)
}
