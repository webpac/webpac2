search( 'title', 	rec('A') );
search( 'issn',  	
	join_with(' ; ',
		rec('C'),
		rec('D')
	)
);
search( 'publisher',	rec('E') );
search( 'url',    	rec('M') );
search( 'urlp',    	rec('M') );
search( 'collection',	
	config('input description')
);
