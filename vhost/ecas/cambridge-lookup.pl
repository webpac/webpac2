my $issn_key =
        rec('F');

my $issn = get( $issn_key );

if ( $issn ) {
        # warn "nije novi";
} else {

$issn = rec('F');

set( $issn_key => $issn );


search( 'title', 	rec('D') );
search( 'abbtitle', 	rec('E') );
search( 'issn',
	join_with(' ; ',
	  	$issn,
		rec('G') 
	)
);
search( 'publisher', 	'Cambridge' );
search( 'url',    	'http://journals.cambridge.org/' . lc ( rec('A') ) );
search( 'urlp',    	'http://journals.cambridge.org/' . lc ( rec('A') ) );
search( 'collection',	
	config('input description')
)

}
