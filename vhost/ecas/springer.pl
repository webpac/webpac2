
search( 'title', 	rec('B') );
search( 'issn',
	join_with(' ; ',
		rec('G'),
		rec('H'),
	),
);
search( 'publisher', 	rec('F') );
search( 'url',    	rec('E') );
search( 'urlp',    	rec('E') );
search( 'keyword',	rec('I') );
search( 'collection',	
	config('input description')
);
