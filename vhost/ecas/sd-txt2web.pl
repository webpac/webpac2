search( 'title', 	rec('C') );
search( 'issn',  	rec('D'),
);
search( 'publisher', 	rec('E') );

search( 'coverage',
	join_with(' to ',
		rec('H'),
		rec('K'),
	)
);

search( 'titlechange',	rec('R') );
search( 'remarks',	rec('S') );
search( 'url',    	rec('T') );
search( 'urlp',    	rec('T') );
search( 'collection',	
	config('input description')
);

