search( 'title', 	rec('B') );
search( 'issn',
	join_with(' ; ',
	 	rec('C'),
		rec('D')
	)
);
#search( 'coverage',	rec('C') );
search( 'publisher',	'Cambridge University Press' );
search( 'url',    	rec('K') );
search( 'urlp',    	rec('K') );
#search( 'subject',	rec('J') );
search( 'collection',	
	config('input description')
);

