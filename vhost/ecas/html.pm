package ecas::html;

use strict;
use warnings;

sub url {
	my ($self,$url) = @_;
	qq{ <a target="ecas" href="$url">Pristup iz ustanove</a> };
}

sub urlp {
	my ($self,$urlp) = @_;
	my $proxy = $urlp;
	$proxy =~ s{http://}{http://proxy.knjiznice.ffzg.hr/proxy/nph-proxy.cgi/en/00/http/};
	qq{ <a target="ecas" href="$proxy">Pristup od kuće</a> };
}

sub urlpe {
	my ($self,$urlpe) = @_;
	my $proxy = $urlpe;
	$proxy =~ s{http://}{http://proxy.knjiznice.ffzg.hr/proxy/nph-proxy2.cgi/en/00/http/};
	qq{ <a target="ecas" href="$proxy">Pristup od kuće</a> };
}


sub urlf {
	my ($self,$urlf) = @_;
	qq{ <a target="ecas" href="$urlf">Slobodni pristup</a> };
}

sub urlebsco {
	my ($self,$urlebsco) = @_;
	my $shib = $urlebsco . '&authtype=shib';
	qq{ <a target="ecas" href="$shib">Pristup od kuće</a> };
}

sub urlebscopa {
	my ($self,$urlebscopa) = @_;
	qq{ <a target="ecas" href="$urlebscopa">Pristup od kuće</a> };
}

#sub publisher {
#	my ($self,$publisher) = @_;
#	qq{<a href="?search=$publisher">$publisher</a>};
#}

1;
