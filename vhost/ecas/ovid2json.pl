tag( 'title', 	rec('Source') );
tag( 'issn',   	rec('ISSN') );
tag( 'url',    	rec('Link to the Ovid Full Text or citation') );
tag( 'urlp',   	rec('Link to the Ovid Full Text or citation') );
tag( 'collection', 	
	join_with('. ',
		rec('Database'),
		rec('Journal Subset'),
	)
);
