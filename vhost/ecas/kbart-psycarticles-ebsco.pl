search( 'title', 	rec('A') );
search( 'issn',
	join_with(' ; ',
	 	rec('B'),
		rec('C')
	)
);
search( 'coverage', 
	join_with(' - ',
		rec('D'),
		rec('G')
	)
);
search( 'publisher',	rec('P') );
search( 'url',    	
	join_with('',
		'http://search.ebscohost.com/direct.asp?db=pdh&jid=',
		rec('L'),
		'&scope=site'
	) 
);
search( 'urlebscopa',
	join_with('',
		'http://search.ebscohost.com/direct.asp?db=pdh&jid=',
		rec('L'),
#		'&scope=site&authtype=shib'
		'&custid=s4311509&authtype=cpid'
	) );

search( 'subject',	rec('S') );

if ( rec('R') && config('input description') =~ m/JSTOR/ ) {
	search( 'collection',
		join_with(' ',
			'JSTOR', rec('R')
		)
	);
} elsif  ( rec('R') ) {
	search( 'collection', rec('R') );
} else {
	search( 'collection',
		config('input description')
	)
}
