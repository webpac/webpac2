search( 'title', 	rec('A') );
search( 'issn',
	join_with(' ; ',
	 	rec('B'),
		rec('C')
	)
);
search( 'coverage', rec('Q') );
search( 'publisher',	rec('P') );
search( 'url',    	rec('J') );
search( 'urlp',    	rec('J') );
search( 'subject',	rec('S') );
search( 'collection',	rec('R') );	
