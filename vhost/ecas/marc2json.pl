tag( 'title', 	
	regex('s/&#237;/í/g',
		rec('245','a') 
	)
);
tag( 'issn',	rec('022','a') );
tag( 'issn_e',	rec('022','y') );
tag( 'publisher',   	
	regex('s/,$//',
		rec('260','b') 
	)
);
tag( 'url',		rec('856','u') );	
tag( 'urlpe',		rec('856','u') );	
tag( 'coverage',	rec('856','3') );
tag( 'collection',	
	config('input description')
)

