search( 'title', 	rec('A') );
search( 'issn',  	
	join_with(' ; ',
		rec('B'),
		rec('C')
	)
);
search( 'url',    	rec('R') );
search( 'urlp',		rec('R') );
search( 'coverage',
	join_with(' - ',
		rec('D'),
		rec('G')
	)
);
search( 'collection',	
	config('input description')
)
